package com.careyq.third;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author CareyQ
 * @since 2021/2/27 14:27
 */
@Component
@SuppressWarnings(value = { "unchecked", "rawtypes" })
public class RedisService {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 缓存基本对象
     *
     * @param key      key
     * @param value    value
     * @param timeout  过期时间
     * @param timeUnit 时间单位, 天:TimeUnit.DAYS 小时:TimeUnit.HOURS 分钟:TimeUnit.MINUTES
     *                 秒:TimeUnit.SECONDS 毫秒:TimeUnit.MILLISECONDS
     */
    public <T> void setEx(final String key, final T value, final Integer timeout, final TimeUnit timeUnit) {
        redisTemplate.opsForValue().set(key, value, timeout, timeUnit);
    }

    /**
     * 获取指定 key 的值 对象
     *
     * @param key key
     * @return 值 对象
     */
    public <T> T getEx(final String key) {
        ValueOperations<String, T> operation = redisTemplate.opsForValue();
        return operation.get(key);
    }

    /**
     * 删除 key
     */
    public void delete(String key) {
        redisTemplate.delete(key);
    }

    /**
     * 查找匹配的key
     *
     * @param pattern key
     * @return 结果
     */
    public Set<String> keys(String pattern) {
        return redisTemplate.keys(pattern);
    }

    /**
     * 批量删除key
     *
     * @param keys keys
     */
    public void delete(Collection<String> keys) {
        redisTemplate.delete(keys);
    }
}

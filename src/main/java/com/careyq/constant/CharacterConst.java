package com.careyq.constant;

/**
 * @author CareyQ
 * @since 2021/2/27 15:39
 */
public class CharacterConst {
    /**
     * 英文符号
     */
    public static final String SYMBOL_COLON = ":";
    public static final String POINT = ".";
    public static final String AND = "&";
    public final static String STAR = "*";

    /**
     * 数字
     */
    public static final String ZERO = "0";
    public static final String ONE = "1";
    public static final String TWO = "2";
}

package com.careyq.constant;

/**
 * @author CareyQ
 * @since 2021/2/27 15:38
 */
public class BaseRedisConst {
    public static final String IP_SOURCE = "IP_SOURCE";
    public static final String LOGIN_TOKEN_KEY = "LOGIN_TOKEN_KEY";
    /**
     * 登录的UUID
     */
    public final static String LOGIN_UUID_KEY = "LOGIN_UUID_KEY";
    /**
     * Redis分隔符
     */
    public final static String SEGMENTATION = ":";

    public static final String ROUTER = "ROUTER";
    public static final String SYSTEM_CONFIG = "SYSTEM_CONFIG";
    public static final String WEB_CONFIG = "WEB_CONFIG";
    public static final String ALL = "ALL";


}

package com.careyq.constant;

/**
 * @author CareyQ
 * @since 2021/3/18 10:16
 */
public class ErrorCodeConst {
    /**
     * 查询操作默认异常码
     */
    public static final String QUERY_DEFAULT_ERROR = "00100";
    public static final String SYSTEM_CONFIG_IS_NOT_EXIST = "00101";
    public static final String PLEASE_CONFIGURE_PASSWORD = "00102";
    public static final String PLEASE_CONFIGURE_BLOG_COUNT = "00103";
    public static final String PLEASE_CONFIGURE_TAG_COUNT = "00104";
    public static final String PLEASE_CONFIGURE_LINK_COUNT = "00105";
    public static final String PLEASE_CONFIGURE_SYSTEM_PARAMS = "00106";
    public static final String PLEASE_SET_QI_NIU = "00107";
    public static final String PLEASE_SET_LOCAL = "00108";
    public static final String PLEASE_SET_MINIO = "00109";
    public static final String SYSTEM_CONFIG_NOT_EXIST = "00110";
}

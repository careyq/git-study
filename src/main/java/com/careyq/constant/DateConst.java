package com.careyq.constant;

/**
 * @author CareyQ
 * @since 2021/3/30 12:32
 */
public class DateConst {
    public static final String SECOND_AGO = " 秒前";
    public static final String MINUTE_AGO = " 分钟前";
    public static final String HOUR_AGO = " 小时前";
    public static final String DAY_AGO = " 天前";
    public static final String MONTH_AGO = " 月前";
    public static final String YEAR_AGO = " 年前";
    public static final String YESTERDAY = "昨天";

    public static final long ONE_MINUTE = 60000L;
    public static final long ONE_HOUR = 3600000L;
    public static final long ONE_DAY = 86400000L;
    public static final long ONE_WEEK = 604800000L;

}

package com.careyq.constant;

/**
 * 通用常量
 *
 * @author CareyQ
 * @since 2021/2/28 23:02
 */
public class Constants {
    /**
     * 令牌
     */
    public static final String TOKEN = "token";
    /**
     * GBK 字符集
     */
    public static final String GBK = "GBK";
    /**
     * 令牌前缀
     */
    public static final String TOKEN_PREFIX = "Bearer ";
    /**
     * 令牌前缀
     */
    public static final String LOGIN_USER_KEY = "login_user_key";
    /**
     * 登录用户 redis key
     */
    public static final String LOGIN_TOKEN_KEY = "login_tokens:";
}

package com.careyq.constant;

/**
 * @author CareyQ
 * @since 2021/3/2 22:46
 */
public class UserConst {
    /** 菜单类型（目录） */
    public static final String TYPE_DIR = "M";

    /** 菜单类型（菜单） */
    public static final String TYPE_MENU = "C";
}

package com.careyq.constant;

/**
 * SQL 字段常量
 *
 * @author CareyQ
 * @since 2021/2/27 21:42
 */
public class SqlConst {
    public final static String LIMIT_ONE = "LIMIT 1";
    public static final String USER_NAME = "USER_NAME";
    public final static String CREATE_TIME = "create_time";
    public static final String UPDATE_TIME = "update_time";
    public static final String UPDATE_BY = "update_by";
    public static final String CREATE_BY = "create_by";
    public static final String NAME = "name";

    public static final String EMAIL = "email";

    /**
     * Admin
     */
    public static final String PASS_WORD = "pass_word";
    public static final String ROLE_UID = "role_uid";
    public static final String NICK_NAME = "nick_name";


    /**
     * blog 表
     */
    public static final String UID = "uid";
    public static final String TITLE = "title";
    public static final String SUMMARY = "summary";
    public static final String CONTENT = "content";
    public static final String STATUS = "status";
    public static final String ORIGINAL = "original";
    public static final String AUTHOR = "author";
    public static final String ADMIN_UID = "admin_uid";
    public static final String SOURCE = "source";
    public static final String CLICK_COUNT = "click_count";
    public static final String CATEGORY_UID = "category_uid";
    public static final String TAG_UID = "tag_uid";
    public static final String IMG_UID = "img_uid";
    public static final String COMMENT = "comment";
    public static final String IS_DELETE = "is_delete";
    public static final String LINK_ID = "link_id";


    /**
     * 字典表相关
     */
    public static final String DICT_TYPE = "dict_type";
    public static final String DICT_TYPE_UID = "dict_type_uid";
    public static final String DICT_NAME = "dict_name";
    public static final String DICT_LABEL = "dict_label";

    /**
     * 分类表
     */
    public static final String CATEGORY_NAME = "category_name";
    public static final String IS_SHOW = "is_show";
    public static final String LINK = "link";

    /**
     * 标签表
     */
    public static final String TAG_NAME = "tag_name";
    /**
     * 图片分类表
     */
    public static final String FILE_UID = "file_uid";
    /**
     * 待办事项表
     */
    public static final String DONE = "done";

    public static final String SOURCE_POST = "post";
    public static final String SOURCE_ABOUT = "about";
    public static final String SOURCE_GUESTBOOK = "guestbook";
    public static final String BLOG_UID = "blog_uid";
    public static final String PARENT_UID = "parent_uid";

    /**
     * 分页相关
     */
    public final static String TOTAL = "total";
    public final static String TOTAL_PAGE = "totalPage";
    public final static String CURRENT_PAGE = "currentPage";
    public final static String BLOG_LIST = "blogList";
    public final static String PAGE_SIZE = "pageSize";
}

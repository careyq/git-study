package com.careyq.constant;

/**
 * 菜单类型
 *
 * @author CareyQ
 * @since 2021/3/14 15:23
 */
public class MenuTypeConst {
    public static final String MENU = "M";
    public static final String DIRECTORY = "D";
    public static final String BUTTON = "B";
    public static final String MAIN = "Main";
}

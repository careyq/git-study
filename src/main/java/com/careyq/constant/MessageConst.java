package com.careyq.constant;

/**
 * @author CareyQ
 * @since 2021/3/10 18:46
 */
public class MessageConst {
    public static final String UPDATE_SUCCESS ="更新成功";
    public static final String UPDATE_FAIL = "更新失败";
    public static final String INSERT_SUCCESS = "添加成功";
    public static final String INSERT_FAIL = "添加失败";
    public static final String DATA_EXIST = "数据已存在";
    public static final String DELETE_SUCCESS = "删除成功";
    public static final String DELETE_FAIL = "删除失败";
    public static final String PARAM_INCORRECT = "传入参数有误！";
    public static final String BLOG_UNDER_THIS_SORT = "该分类下还有博客！";
    public static final String FILE_IS_EMPTY = "上传的文件为空！";
    public static final String PICTURE_IS_NOT_EXIST = "请求的图片不存在！";
    public static final String FILE_LOCAL_UPLOAD_EXCEPTION = "文件上传本地存储出错，请检查配置！";
    public static final String FILE_COS_UPLOAD_EXCEPTION = "文件上传腾讯云 COS 出错，请检查配置！";
    public static final String FILE_MUST_BE_SELECT = "文件必须选择一个上传区域";
    public static final String MUST_BE_OPEN_LOCAL = "存储方式: 本地存储启用，必须配置本地存储信息";
    public static final String MUST_BE_OPEN_COS = "存储方式: 腾讯云 COS 存储启用，必须配置 COS 信息";
    public static final String SYSTEM_CONFIG_IS_NOT_EXIST = "系统配置不存在，请检查数据库";
    public static final String WEB_CONFIG_IS_NOT_EXIST = "网站配置不存在，请检查数据库";
    public static final String QUERY_DEFAULT_ERROR = "查询操作失败";
    public static final String FILE_IS_NOT_EXIST = "文章标题图已被删除，请重新设置！";
    public static final String INSERT_POST_PICTURE_FAIL = "添加文章图片失败！";
    public static final String INSERT_POST_PICTURE_SUCCESS = "添加文章图片成功！";
    public static final String THE_PICTURE_CATEGORY_PIC_EXIST = "该图片分类下还有图片存在";
    public static final String OPERATION_FAIL = "操作失败";
    public static final String OPERATION_SUCCESS = "操作成功";
    public static final String POST_NOT_EXIST = "文章不存在!";
}

package com.careyq.constant;

/**
 * @author CareyQ
 * @since 2021/2/25 18:14
 */
public class CommonConst {
    public static final String SOURCE = "source";
    public static final String ADMIN = "admin";
    public static final String WEB = "web";
    public static final String PIC_CATEGORY_UID = "picCategoryUid";

    public static final String BLOG_COUNT = "blogCount";
    public static final String CATEGORY_COUNT = "categoryCount";
    public static final String TAG_COUNT = "tagCount";
    public static final String COMMENT_COUNT = "commentCount";
    public static final String LINK_COUNT = "linkCount";
}

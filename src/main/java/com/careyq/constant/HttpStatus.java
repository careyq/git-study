package com.careyq.constant;

/**
 * @author CareyQ
 * @since 2021/2/25 18:44
 */
public class HttpStatus {
    /**
     * 操作成功
     */
    public static final int SUCCESS = 200;
    /**
     * 未授权
     */
    public static final int UNAUTHORIZED = 401;
    /**
     * 系统内部错误
     */
    public static final int ERROR = 500;
}

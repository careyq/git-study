package com.careyq.constant;

/**
 * @author CareyQ
 * @since 2021/3/30 12:39
 */
public class NumberConst {
    public static final long L45 = 45L;
    public static final long L24 = 24L;
    public static final long L48 = 48L;
    public static final long L30 = 30L;
    public static final long L12 = 12L;
    public static final long L4 = 4L;
}

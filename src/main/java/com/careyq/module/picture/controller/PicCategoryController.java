package com.careyq.module.picture.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.module.picture.service.PicCategoryService;
import com.careyq.module.picture.vo.PicCategoryVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author CareyQ
 * @since 2021/3/16 15:55
 */
@RestController
@RequestMapping("/picture/category")
public class PicCategoryController {

    @Autowired
    private PicCategoryService picCategoryService;

    @PostMapping("/list")
    public ResponseDTO list(@RequestBody PicCategoryVO picCategoryVO) {
        return ResponseDTO.success(picCategoryService.getPageList(picCategoryVO));
    }

    @PostMapping("/add")
    public ResponseDTO addPicCategory(@RequestBody PicCategoryVO picCategoryVO) {
        return picCategoryService.addPicCategory(picCategoryVO);
    }

    @PostMapping("/edit")
    public ResponseDTO editPicCategory(@RequestBody PicCategoryVO picCategoryVO) {
        return picCategoryService.editPicCategory(picCategoryVO);
    }

    @PostMapping("/delete")
    public ResponseDTO delete(@RequestBody String uid) {
        return picCategoryService.delete(uid);
    }
}

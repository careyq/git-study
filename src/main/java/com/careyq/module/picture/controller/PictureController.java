package com.careyq.module.picture.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.module.picture.service.PictureService;
import com.careyq.module.picture.vo.PictureVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author CareyQ
 * @since 2021/3/16 22:32
 */
@RestController
@RequestMapping("/picture/manager")
public class PictureController {

    @Autowired
    private PictureService pictureService;

    @PostMapping("/getList")
    public ResponseDTO getList(@RequestBody PictureVO pictureVO) {
        return ResponseDTO.success(pictureService.getPageList(pictureVO));
    }

    @PostMapping("/upload")
    public synchronized ResponseDTO upload(HttpServletRequest request, @RequestParam(value = "file") List<MultipartFile> fileList) {
        return pictureService.batchUploadPic(request, fileList);
    }

    @PostMapping("/cropper")
    public ResponseDTO edit(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
        System.out.println(file.getName() + file.getSize());
        //return pictureService.cropperPicture(request, file);
        return null;
    }

    @PostMapping("/delete")
    public ResponseDTO delete(@RequestParam("source") String storage, @RequestParam("pictures") List<String> picUids) {
        return pictureService.deletePictures(storage, picUids);
    }

    @PostMapping("/upload/post")
    public ResponseDTO addPostPicture(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
        return pictureService.addPostPicture(request, file);
    }

    @GetMapping("/getPicture/{uid}")
    public ResponseDTO get(@PathVariable String uid) {
        return pictureService.getPicture(uid);
    }

    @PostMapping("/logo")
    public ResponseDTO uploadLogo(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
        return pictureService.uploadLogo(request, file);
    }

    @GetMapping("/getLogo")
    public ResponseDTO getLogo() {
        return pictureService.getLogo();
    }

    @PostMapping("/avatar")
    public ResponseDTO uploadAvatar(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
        return pictureService.uploadAvatar(request, file);
    }

    @GetMapping("/getAvatar")
    public ResponseDTO getAvatar() {
        return pictureService.getAvatar();
    }

    @PostMapping("/wechat")
    public ResponseDTO uploadWechat(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
        return pictureService.uploadWechat(request, file);
    }

    @GetMapping("/getWechat")
    public ResponseDTO getWechat() {
        return pictureService.getWechat();
    }
}

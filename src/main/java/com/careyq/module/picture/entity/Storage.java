package com.careyq.module.picture.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 文件存储类，用户区分 web 和 admin 文件
 *
 * @author CareyQ
 * @since 2021/3/17 16:22
 */
@Data
@TableName("t_file_storage")
public class Storage {
    /**
     * 存储名
     */
    @TableId
    private String storageName;
    /**
     * 存储地址
     */
    private String url;
}

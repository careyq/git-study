package com.careyq.module.picture.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.careyq.common.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 图片分类表
 *
 * @author CareyQ
 * @since 2021/3/16 14:45
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_file_category")
public class PicCategory extends BaseEntity<PicCategory> {
    /**
     * 图片分类名称
     */
    private String categoryName;
    /**
     * 封面图 uid
     */
    private String fileUid;
    /**
     * 封面图地址
     */
    @TableField(exist = false)
    private String fileUrl;
}

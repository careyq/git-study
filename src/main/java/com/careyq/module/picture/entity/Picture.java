package com.careyq.module.picture.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.careyq.common.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author CareyQ
 * @since 2021/3/16 21:50
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_file")
@ToString
public class Picture extends BaseEntity<Picture> {
    /**
     * 图片名
     */
    private String name;
    /**
     * 图片分类 uid
     */
    private String categoryUid;
    /**
     * 旧名字
     */
    private String oldName;
    /**
     * 图片大小
     */
    private Long size;
    /**
     * 图片扩展名
     */
    private String extension;
    /**
     * 图片路径
     */
    private String url;
    /**
     * Gitee SHA
     */
    private String giteeSha;
}

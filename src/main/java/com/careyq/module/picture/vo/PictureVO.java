package com.careyq.module.picture.vo;

import com.careyq.common.domain.vo.BaseVO;
import com.careyq.module.picture.entity.Picture;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author CareyQ
 * @since 2021/3/16 21:50
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class PictureVO extends BaseVO<PictureVO> {
    private String name;
    private String categoryUid;
}

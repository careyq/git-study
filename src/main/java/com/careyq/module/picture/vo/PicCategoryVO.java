package com.careyq.module.picture.vo;

import com.careyq.common.domain.vo.BaseVO;
import com.careyq.module.picture.entity.PicCategory;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author CareyQ
 * @since 2021/3/16 15:14
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class PicCategoryVO extends BaseVO<PicCategory> {
    private String categoryName;
    private String fileUid;
    private String fileUrl;
}

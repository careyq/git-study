package com.careyq.module.picture.service;

import com.careyq.module.picture.entity.Picture;
import com.careyq.module.picture.entity.Storage;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * 本地文件系统服务
 *
 * @author CareyQ
 * @since 2021/3/17 15:12
 */
public interface LocalFileService {
    /**
     * 本地存储上传文件
     *
     * @param file         要上传的文件
     * @param absoluteDir  存储的绝对文件目录
     * @param absolutePath 存储的绝对文件路径
     * @return 操作结果
     */
    boolean uploadFile(File file, String absoluteDir, String absolutePath);

    /**
     * 文件修改
     *
     * @param file    修改好的文件
     * @param storage 存储
     * @param picture 修改对应的已有文件
     * @return 修改后的文件名
     * @throws IOException 异常
     */
    String modifyFile(MultipartFile file, Storage storage, Picture picture) throws IOException;

    /**
     * 文件删除
     *
     * @param picture 删除的文件
     * @param storage 存储
     * @return 操作结果
     */
    boolean deleteFile(Picture picture, Storage storage);
}

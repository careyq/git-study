package com.careyq.module.picture.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.impl.SuperServiceImpl;
import com.careyq.constant.MessageConst;
import com.careyq.constant.SqlConst;
import com.careyq.module.picture.entity.PicCategory;
import com.careyq.module.picture.entity.Picture;
import com.careyq.module.picture.mapper.PicCategoryMapper;
import com.careyq.module.picture.service.PicCategoryService;
import com.careyq.module.picture.service.PictureService;
import com.careyq.module.picture.vo.PicCategoryVO;
import com.careyq.module.system.service.SystemConfigService;
import com.careyq.util.SecurityUtils;
import com.careyq.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author CareyQ
 * @since 2021/3/16 15:47
 */
@Service
@Slf4j
public class PicCategoryServiceImpl extends SuperServiceImpl<PicCategoryMapper, PicCategory> implements PicCategoryService {

    @Autowired
    private PicCategoryService picCategoryService;
    @Autowired
    private PicCategoryMapper picCategoryMapper;
    @Autowired
    private PictureService pictureService;
    @Autowired
    private SystemConfigService systemConfigService;

    @Override
    public IPage<PicCategory> getPageList(PicCategoryVO picCategoryVO) {
        Page<PicCategory> page = new Page<>();
        page.setSize(picCategoryVO.getPageSize());
        page.setCurrent(picCategoryVO.getCurrentPage());
        Page<PicCategory> pageList = picCategoryService.page(page, new QueryWrapper<>());
        List<PicCategory> picCategoryList = pageList.getRecords();

        picCategoryList.forEach(item -> {
            if (StringUtils.isNotEmpty(item.getFileUid())) {
                QueryWrapper<Picture> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq(SqlConst.UID, item.getFileUid());

                try {
                    String localDomain = systemConfigService.getSystemConfig().getLocalDomain();
                    item.setFileUrl(localDomain + pictureService.getOne(queryWrapper).getUrl());
                }catch (NullPointerException e) {
                    log.error(MessageConst.FILE_IS_NOT_EXIST + "==> {}", item.getCategoryName());
                }
            }
        });
        pageList.setRecords(picCategoryList);

        return pageList;
    }

    @Override
    public ResponseDTO addPicCategory(PicCategoryVO picCategoryVO) {
        String categoryName = picCategoryVO.getCategoryName();
        QueryWrapper<PicCategory> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.CATEGORY_NAME, categoryName);
        int count = picCategoryService.count(queryWrapper);

        if (count >= 1) {
            return ResponseDTO.error(MessageConst.DATA_EXIST);
        }

        PicCategory picCategory = new PicCategory();
        picCategory.setCategoryName(categoryName);
        picCategory.setFileUid(picCategoryVO.getFileUid());
        picCategory.insert();
        return ResponseDTO.success(MessageConst.INSERT_SUCCESS);
    }

    @Override
    public ResponseDTO editPicCategory(PicCategoryVO picCategoryVO) {
        PicCategory picCategory = picCategoryService.getById(picCategoryVO.getUid());
        picCategory.setFileUid(picCategoryVO.getFileUid());
        picCategory.setCategoryName(picCategoryVO.getCategoryName());
        picCategoryMapper.updateById(picCategory);
        return ResponseDTO.success(MessageConst.UPDATE_SUCCESS);
    }

    @Override
    public ResponseDTO delete(String uid) {
        QueryWrapper<Picture> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.CATEGORY_UID, uid);
        int count = pictureService.count(queryWrapper);

        if (count > 0) {
            return ResponseDTO.error(MessageConst.THE_PICTURE_CATEGORY_PIC_EXIST);
        }

        if (picCategoryService.removeById(uid)) {
            return ResponseDTO.success(MessageConst.DELETE_SUCCESS);
        }

        return ResponseDTO.error(MessageConst.DELETE_FAIL);
    }
}

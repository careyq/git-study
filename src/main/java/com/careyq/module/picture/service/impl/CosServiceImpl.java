package com.careyq.module.picture.service.impl;

import cn.hutool.core.thread.ThreadFactoryBuilder;
import com.careyq.module.picture.service.CosService;
import com.careyq.module.system.entity.SystemConfig;
import com.careyq.module.system.service.SystemConfigService;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.model.*;
import com.qcloud.cos.region.Region;
import com.qcloud.cos.transfer.TransferManager;
import com.qcloud.cos.transfer.TransferManagerConfiguration;
import com.qcloud.cos.transfer.Upload;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.concurrent.*;

/**
 * @author CareyQ
 * @since 2021/3/23 11:42
 */
@Service
@Slf4j
public class CosServiceImpl implements CosService {
    @Autowired
    private SystemConfigService systemConfigService;


    @Override
    public void getCosObjects(String bucket) {
        ListObjectsRequest listObjectsRequest = new ListObjectsRequest();
        listObjectsRequest.setBucketName(bucket);
        ObjectListing objectListing = generateCosClient().listObjects(listObjectsRequest);
        List<COSObjectSummary> objectSummaries = objectListing.getObjectSummaries();
        for (COSObjectSummary objectSummary : objectSummaries) {
            // 文件的路径key
            String key = objectSummary.getKey();
            // 文件的etag
            String etag = objectSummary.getETag();
            // 文件的长度
            long fileSize = objectSummary.getSize();
            // 文件的存储类型
            String storageClasses = objectSummary.getStorageClass();
        }
    }

    @Override
    @SuppressWarnings("all")
    public boolean uploadFile(File file, String key) {
        SystemConfig systemConfig = systemConfigService.getSystemConfig();
        String localPath = systemConfig.getLocalPath();

        String cosBucket = systemConfig.getCosBucket();
        TransferManager transferManager = generateTransferManager();
        try {

            PutObjectRequest putObjectRequest = new PutObjectRequest(cosBucket, key, file);

            Upload upload = transferManager.upload(putObjectRequest);
            UploadResult uploadResult = upload.waitForUploadResult();

            return true;
        } catch (InterruptedException e) {
            log.error("文件上传 COS 出错, 等待上传结束: {}", e.getMessage());
        } finally {
            closeTransferManager(transferManager);
        }

        return false;
    }

    private TransferManager generateTransferManager() {
        ThreadFactory threadFactory = new ThreadFactoryBuilder().setNamePrefix("thread-cos-runner-%d").build();
        ExecutorService threadPool = new ThreadPoolExecutor(16, 16, 200L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), threadFactory);
        TransferManager transferManager = new TransferManager(generateCosClient(), threadPool);
        TransferManagerConfiguration transferManagerConfiguration = new TransferManagerConfiguration();
        transferManagerConfiguration.setMultipartUploadThreshold(10 * 1024 * 1024);
        transferManagerConfiguration.setMinimumUploadPartSize(10 * 1024 * 1024);
        transferManager.setConfiguration(transferManagerConfiguration);
        return transferManager;
    }

    private void closeTransferManager(TransferManager transferManager) {
        transferManager.shutdownNow();
    }

    /**
     * 获取存储桶列表
     *
     * @return 存储桶列表
     */
    private List<Bucket> getBuckets() {
        return generateCosClient().listBuckets();
    }

    /**
     * 获取 COSClient
     *
     * @return COSClient
     */
    private COSClient generateCosClient() {
        SystemConfig systemConfig = systemConfigService.getSystemConfig();
        String secretId = systemConfig.getCosSecretId();
        String secretKey = systemConfig.getCosSecretKey();
        String cosRegion = systemConfig.getCosRegion();

        BasicCOSCredentials credentials = new BasicCOSCredentials(secretId, secretKey);
        Region region = new Region(cosRegion);
        ClientConfig clientConfig = new ClientConfig(region);
        return new COSClient(credentials, clientConfig);
    }
}

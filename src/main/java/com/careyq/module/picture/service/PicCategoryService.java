package com.careyq.module.picture.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.SuperService;
import com.careyq.module.picture.entity.PicCategory;
import com.careyq.module.picture.vo.PicCategoryVO;

/**
 * @author CareyQ
 * @since 2021/3/16 15:46
 */
public interface PicCategoryService extends SuperService<PicCategory> {
    /**
     * 获取图片分类列表
     *
     * @param picCategoryVO 图片分类视图层
     * @return 列表
     */
    IPage<PicCategory> getPageList(PicCategoryVO picCategoryVO);

    /**
     * 添加图片分类
     *
     * @param picCategoryVO 图片分类视图层
     * @return 操作结果消息
     */
    ResponseDTO addPicCategory(PicCategoryVO picCategoryVO);

    /**
     * 修改图片分类
     *
     * @param picCategoryVO 图片分类视图层
     * @return 操作结果消息
     */
    ResponseDTO editPicCategory(PicCategoryVO picCategoryVO);

    /**
     * 删除分类
     *
     * @param uid id
     * @return 结果
     */
    ResponseDTO delete(String uid);
}

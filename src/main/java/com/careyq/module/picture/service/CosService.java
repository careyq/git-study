package com.careyq.module.picture.service;

import java.io.File;

/**
 * @author CareyQ
 * @since 2021/3/23 11:41
 */
public interface CosService {
    /**
     * 获取对象列表
     *
     * @param bucket 存储桶
     */
    void getCosObjects(String bucket);

    /**
     * 上传对象
     *
     * @param file 文件
     * @param key  对象键 文件的相对路径
     * @return 操作结果
     */
    boolean uploadFile(File file, String key);
}

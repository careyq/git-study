package com.careyq.module.picture.service.impl;

import com.careyq.common.service.impl.SuperServiceImpl;
import com.careyq.module.picture.entity.Storage;
import com.careyq.module.picture.mapper.StorageMapper;
import com.careyq.module.picture.service.StorageService;
import org.springframework.stereotype.Service;

/**
 * @author CareyQ
 * @since 2021/3/17 16:26
 */
@Service
public class StorageServiceImpl extends SuperServiceImpl<StorageMapper, Storage> implements StorageService {
}

package com.careyq.module.picture.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.SuperService;
import com.careyq.module.picture.entity.Picture;
import com.careyq.module.picture.vo.PictureVO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author CareyQ
 * @since 2021/3/16 21:58
 */
public interface PictureService extends SuperService<Picture> {
    /**
     * 获取图片列表
     *
     * @param pictureVO 图片视图层
     * @return 列表
     */
    IPage<Picture> getPageList(PictureVO pictureVO);

    /**
     * 批量上传图片
     *
     * @param request  request
     * @param fileList 文件列表
     * @return 操作结果消息
     */
    ResponseDTO batchUploadPic(HttpServletRequest request, List<MultipartFile> fileList);

    /**
     * 修改图片
     *
     * @param request request
     * @param file    文件
     * @return 操作结果消息
     */
    ResponseDTO cropperPicture(HttpServletRequest request, MultipartFile file);

    /**
     * 根据图片 id 列表删除
     *
     * @param storage 存储类型 web or admin
     * @param picUids id 列表
     * @return 操作结果消息
     */
    ResponseDTO deletePictures(String storage, List<String> picUids);

    /**
     * 根据 uid 获取图片
     *
     * @param uid uid
     * @return 图片
     */
    ResponseDTO getPicture(String uid);

    /**
     * 文章图片的添加
     *
     * @param request 请求
     * @param file    文件
     * @return 操作结果消息，携带 Picture
     */
    ResponseDTO addPostPicture(HttpServletRequest request, MultipartFile file);

    /**
     * Logo 上传
     *
     * @param request 请求
     * @param file    文件
     * @return 操作结果
     */
    ResponseDTO uploadLogo(HttpServletRequest request, MultipartFile file);

    /**
     * 获取 Logo
     *
     * @return 操作结果
     */
    ResponseDTO getLogo();

    /**
     * Avatar 上传
     *
     * @param request 请求
     * @param file    文件
     * @return 结果
     */
    ResponseDTO uploadAvatar(HttpServletRequest request, MultipartFile file);

    /**
     * 获取 Avatar
     *
     * @return 操作结果
     */
    ResponseDTO getAvatar();

    /**
     * Wechat 上传
     *
     * @param request 请求
     * @param file    文件
     * @return 结果
     */
    ResponseDTO uploadWechat(HttpServletRequest request, MultipartFile file);

    /**
     * 获取 Wechat
     *
     * @return 操作结果
     */
    ResponseDTO getWechat();
}

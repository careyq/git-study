package com.careyq.module.picture.service;

import com.careyq.common.mapper.SuperMapper;
import com.careyq.common.service.SuperService;
import com.careyq.module.picture.entity.Storage;

/**
 * @author CareyQ
 * @since 2021/3/17 16:25
 */
public interface StorageService extends SuperService<Storage> {
}

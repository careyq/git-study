package com.careyq.module.picture.service.impl;

import cn.hutool.core.lang.UUID;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.impl.SuperServiceImpl;
import com.careyq.constant.CharacterConst;
import com.careyq.constant.CommonConst;
import com.careyq.constant.MessageConst;
import com.careyq.constant.SqlConst;
import com.careyq.module.admin.service.AdminService;
import com.careyq.module.picture.entity.Picture;
import com.careyq.module.picture.entity.Storage;
import com.careyq.module.picture.mapper.PictureMapper;
import com.careyq.module.picture.service.CosService;
import com.careyq.module.picture.service.LocalFileService;
import com.careyq.module.picture.service.PictureService;
import com.careyq.module.picture.service.StorageService;
import com.careyq.module.picture.vo.PictureVO;
import com.careyq.module.system.entity.SystemConfig;
import com.careyq.module.system.service.SystemConfigService;
import com.careyq.util.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author CareyQ
 * @since 2021/3/16 21:59
 */
@Service
@Slf4j
public class PictureServiceImpl extends SuperServiceImpl<PictureMapper, Picture> implements PictureService {
    @Autowired
    private PictureService pictureService;
    @Autowired
    private StorageService storageService;
    @Autowired
    private LocalFileService localFileService;
    @Autowired
    private SystemConfigService systemConfigService;
    @Autowired
    private CosService cosService;
    @Autowired
    private AdminService adminService;

    @Override
    public IPage<Picture> getPageList(PictureVO pictureVO) {
        QueryWrapper<Picture> queryWrapper = new QueryWrapper<>();
        Page<Picture> page = new Page<>();
        page.setCurrent(pictureVO.getCurrentPage());
        page.setSize(pictureVO.getPageSize());
        queryWrapper.eq(SqlConst.CATEGORY_UID, pictureVO.getCategoryUid());
        queryWrapper.orderByDesc(SqlConst.CREATE_TIME);
        IPage<Picture> pageList = pictureService.page(page, queryWrapper);
        SystemConfig systemConfig = systemConfigService.getSystemConfig();

        List<Picture> pictureList = pageList.getRecords();

        for (Picture picture : pictureList) {
            if (CharacterConst.ZERO.equals(systemConfig.getPicShow())) {
                picture.setUrl(systemConfig.getLocalDomain() + picture.getUrl());
            } else if (CharacterConst.ONE.equals(systemConfig.getPicShow())) {
                picture.setUrl(systemConfig.getCosDomain() + picture.getUrl());
            }
            pageList.setRecords(pictureList);
        }
        return pageList;
    }

    @Override
    public ResponseDTO batchUploadPic(HttpServletRequest request, List<MultipartFile> fileList) {
        if (fileList.isEmpty()) {
            return ResponseDTO.error(MessageConst.FILE_IS_EMPTY);
        }

        String picCategoryUid = request.getParameter(CommonConst.PIC_CATEGORY_UID);
        String pictureAbsoluteDir = getPictureAbsoluteDir(request);
        SystemConfig systemConfig = systemConfigService.getSystemConfig();
        List<Picture> pictureList = new ArrayList<>();
        for (MultipartFile file : fileList) {
            String oldName = file.getOriginalFilename();
            long size = file.getSize();
            assert oldName != null;
            String extension = oldName.substring(oldName.lastIndexOf("."));
            String newName = System.currentTimeMillis() + extension;

            // 图片相对文件路径 /xx/xx/xx/xx.jpg
            String pictureStoragePath = getPictureStoragePath(request, newName);
            // 图片绝对文件路径 E:/xx/xx/xx/xx.jpg
            String pictureAbsolutePath = getPictureAbsolutePath(request, newName);

            File dest = multipartFileToFile(file, extension);
            // 是否启用了本地存储
            if (CharacterConst.ONE.equals(systemConfig.getUploadLocal())) {
                if (!localFileService.uploadFile(dest, pictureAbsoluteDir, pictureAbsolutePath)) {
                    return ResponseDTO.error(MessageConst.FILE_LOCAL_UPLOAD_EXCEPTION);
                }
            }

            // 是否启用了腾讯云存储
            if (CharacterConst.ONE.equals(systemConfig.getUploadCos())) {
                if (!cosService.uploadFile(dest, pictureStoragePath)) {
                    return ResponseDTO.error(MessageConst.FILE_COS_UPLOAD_EXCEPTION);
                }
            }

            Picture picture = new Picture();
            picture.setUrl(pictureStoragePath);
            picture.setSize(size);
            picture.setExtension(extension);
            picture.setName(newName);
            picture.setOldName(oldName);
            picture.setCategoryUid(picCategoryUid);
            picture.insert();
            pictureList.add(picture);
        }

        return ResponseDTO.success(MessageConst.INSERT_SUCCESS, pictureList);
    }

    @Override
    public ResponseDTO cropperPicture(HttpServletRequest request, MultipartFile file) {

        List<MultipartFile> fileList = new ArrayList<>();
        fileList.add(file);
        ResponseDTO responseDTO = batchUploadPic(request, fileList);
        Picture result = parseResponseDTO(responseDTO);

        if (result != null) {
            String uid = request.getParameter(SqlConst.UID);
            QueryWrapper<Picture> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq(SqlConst.UID, uid);
            Picture picture = pictureService.getOne(queryWrapper);
            picture.setName(result.getName());
            picture.setUrl(result.getUrl());
            picture.updateById();
            return ResponseDTO.success(MessageConst.UPDATE_SUCCESS);
        }

        return ResponseDTO.error(MessageConst.UPDATE_FAIL);
    }

    @Override
    public ResponseDTO deletePictures(String storageId, List<String> picUids) {
        for (String picUid : picUids) {
            Picture picture = pictureService.getById(picUid);
            picture.deleteById();
        }
        return ResponseDTO.success(MessageConst.DELETE_SUCCESS);
    }

    @Override
    public ResponseDTO getPicture(String uid) {
        SystemConfig systemConfig = systemConfigService.getSystemConfig();
        Picture picture = pictureService.getById(uid);
        if (picture == null) {
            return ResponseDTO.error(MessageConst.PICTURE_IS_NOT_EXIST);
        }

        if (CharacterConst.ZERO.equals(systemConfig.getPicShow())) {
            picture.setUrl(systemConfig.getLocalDomain() + picture.getUrl());
        } else if (CharacterConst.ONE.equals(systemConfig.getPicShow())) {
            picture.setUrl(systemConfig.getCosDomain() + picture.getUrl());
        }

        return ResponseDTO.success(picture);
    }

    @Override
    public ResponseDTO addPostPicture(HttpServletRequest request, MultipartFile file) {
        List<MultipartFile> fileList = new ArrayList<>();
        fileList.add(file);
        ResponseDTO responseDTO = batchUploadPic(request, fileList);
        Picture result = parseResponseDTO(responseDTO);

        if (result != null) {
            SystemConfig systemConfig = systemConfigService.getSystemConfig();
            if (CharacterConst.ZERO.equals(systemConfig.getPicShow())) {
                result.setUrl(systemConfig.getLocalDomain() + result.getUrl());
            } else if (CharacterConst.ONE.equals(systemConfig.getPicShow())) {
                result.setUrl(systemConfig.getCosDomain() + result.getUrl());
            }
            return ResponseDTO.success(MessageConst.INSERT_POST_PICTURE_SUCCESS, result);
        }
        return ResponseDTO.error(MessageConst.INSERT_POST_PICTURE_FAIL);
    }

    @Override
    public ResponseDTO uploadLogo(HttpServletRequest request, MultipartFile file) {
        return uploadImage(request, file, "logo.jpg", "jpg");
    }

    @Override
    public ResponseDTO getLogo() {
        return getImage("logo.jpg");
    }

    @Override
    public ResponseDTO uploadAvatar(HttpServletRequest request, MultipartFile file) {
        return uploadImage(request, file, "avatar.jpg", "jpg");
    }

    @Override
    public ResponseDTO getAvatar() {
        return getImage("avatar.jpg");
    }

    @Override
    public ResponseDTO uploadWechat(HttpServletRequest request, MultipartFile file) {
        return uploadImage(request, file, "wechat.jpg", "jpg");
    }

    @Override
    public ResponseDTO getWechat() {
        return getImage("wechat.jpg");
    }

    private ResponseDTO uploadImage(HttpServletRequest request, MultipartFile file, String fileName, String extension) {
        String pictureAbsoluteDir = getPictureAbsoluteDir(request);
        String pictureAbsolutePath = getPictureAbsolutePath(request, fileName);
        File dest = multipartFileToFile(file, extension);
        if (!localFileService.uploadFile(dest, pictureAbsoluteDir, pictureAbsolutePath)) {
            return ResponseDTO.error(MessageConst.INSERT_FAIL);
        }

        // 图片相对文件路径 /xx/xx/xx/xx.jpg
        String pictureStoragePath = getPictureStoragePath(request, fileName);

        QueryWrapper<Picture> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.NAME, fileName);
        Picture oldLogo = pictureService.getOne(queryWrapper);
        if ("wechat.jpg".equals(fileName)) {
            adminService.setWechat(pictureStoragePath);
        } else {
            adminService.setAvatar(pictureStoragePath);
        }

        if (oldLogo == null) {
            Picture picture = new Picture();
            picture.setUrl(pictureStoragePath);
            picture.setExtension(extension);
            picture.setName(fileName);
            picture.setOldName(fileName + "_old.jpg");
            picture.insert();
            return ResponseDTO.success(MessageConst.INSERT_SUCCESS);
        }

        oldLogo.setUrl(pictureStoragePath);
        oldLogo.updateById();

        return ResponseDTO.success(MessageConst.UPDATE_SUCCESS);
    }

    private ResponseDTO getImage(String fileName) {
        String localDomain = systemConfigService.getSystemConfig().getLocalDomain();
        QueryWrapper<Picture> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.NAME, fileName);
        Picture picture = pictureService.getOne(queryWrapper);
        if (picture == null) {
            return ResponseDTO.error("获取 " + fileName + " 失败！");
        }
        return ResponseDTO.success("获取 " + fileName + " 成功！", localDomain + picture.getUrl());
    }

    private Picture parseResponseDTO(ResponseDTO responseDTO) {

        Object data = responseDTO.get("data");
        if (data instanceof ArrayList) {
            return (Picture) ((ArrayList<?>) data).get(0);
        }
        return null;
    }

    private File multipartFileToFile(MultipartFile file, String extension) {
        File dest = null;
        try {
            dest = File.createTempFile(UUID.randomUUID().toString(), extension);
            FileUtils.copyInputStreamToFile(file.getInputStream(), dest);
        } catch (IOException e) {
            log.error("修改文件异常, MultipartFile 复制流出错: {}", e.getMessage());
        }
        return dest;
    }

    /**
     * 获取图片相对目录位置 例如 /xx/xx/xx/xx
     *
     * @param request 请求
     * @return 目录位置
     */
    private String getPictureStorageDir(HttpServletRequest request) {
        String storageName = request.getParameter(CommonConst.SOURCE);
        Storage storage = storageService.getById(storageName);

        return storage.getUrl() + "/" +
                DateUtils.getYear() + "/" +
                DateUtils.getMonth() + "/" +
                DateUtils.getDay() + "/";
    }

    /**
     * 获取图片绝对目录位置 例如 E:/xx/xx/xx/xx
     *
     * @param request 请求
     * @return 目录位置
     */
    private String getPictureAbsoluteDir(HttpServletRequest request) {
        SystemConfig systemConfig = systemConfigService.getSystemConfig();
        return systemConfig.getLocalPath() + getPictureStorageDir(request);
    }

    /**
     * 获取图片相对文件路径 例如 /xx/xx/xx/xx.jpg
     *
     * @param request  请求
     * @param fileName 文件名
     * @return 文件路径
     */
    private String getPictureStoragePath(HttpServletRequest request, String fileName) {
        return getPictureStorageDir(request) + fileName;
    }

    /**
     * 获取图片绝对文件路径 例如 E:/xx/xx/xx/xx.jpg
     *
     * @param request  请求
     * @param fileName 文件名
     * @return 文件路径
     */
    private String getPictureAbsolutePath(HttpServletRequest request, String fileName) {
        return getPictureAbsoluteDir(request) + fileName;
    }
}

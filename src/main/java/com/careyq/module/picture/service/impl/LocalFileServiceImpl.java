package com.careyq.module.picture.service.impl;

import com.careyq.constant.CharacterConst;
import com.careyq.module.picture.entity.Picture;
import com.careyq.module.picture.entity.Storage;
import com.careyq.module.picture.service.LocalFileService;
import com.careyq.module.system.entity.SystemConfig;
import com.careyq.module.system.service.SystemConfigService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

/**
 * 本地文件服务实现类
 *
 * @author CareyQ
 * @since 2021/3/17 15:15
 */
@Service
@Slf4j
public class LocalFileServiceImpl implements LocalFileService {
    @Autowired
    private SystemConfigService systemConfigService;

    @Override
    @SuppressWarnings("all")
    public boolean uploadFile(File file, String absoluteDir, String absolutePath) {
        SystemConfig systemConfig = systemConfigService.getSystemConfig();
        File fileDir = new File(absoluteDir);
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
        File localFile = new File(absolutePath);
        try {
            FileUtils.copyFile(file, localFile);
            return true;
        } catch (IOException e) {
            log.error("文件上传本地存储出错, CopyFile: {}", e.getMessage());
        }

        return false;
    }

    @Override
    @SuppressWarnings("all")
    public String modifyFile(MultipartFile file, Storage storage, Picture picture) throws IOException {
        String name = picture.getName();

        if (deleteFile(picture, storage)) {
            String storageDir = getStorageDir(picture, storage);
            String newName = System.currentTimeMillis() + CharacterConst.POINT + picture.getExtension();
            String storagePath = storageDir + newName;

            File storageFile = new File(storagePath);
            storageFile.createNewFile();
            file.transferTo(storageFile);

            return newName;
        }
        return null;
    }

    @Override
    public boolean deleteFile(Picture picture, Storage storage) {
        String storageDir = getStorageDir(picture, storage);
        String name = storageDir + picture.getName();
        File file = new File(name);

        return file.delete();
    }

    public String getStorageDir(Picture picture, Storage storage) {
        SystemConfig systemConfig = systemConfigService.getSystemConfig();
        String localPath = systemConfig.getLocalPath();

        Date createTime = picture.getCreateTime();
        String extension = picture.getExtension();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(createTime);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        return localPath +
                storage.getUrl() + "/" +
                extension + "/" +
                year + "/" +
                month + "/" +
                day + "/";
    }
}

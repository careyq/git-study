package com.careyq.module.picture.mapper;

import com.careyq.common.mapper.SuperMapper;
import com.careyq.module.picture.entity.PicCategory;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author CareyQ
 * @since 2021/3/16 15:13
 */
@Mapper
public interface PicCategoryMapper extends SuperMapper<PicCategory> {
}

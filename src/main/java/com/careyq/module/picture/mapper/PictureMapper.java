package com.careyq.module.picture.mapper;

import com.careyq.common.mapper.SuperMapper;
import com.careyq.module.picture.entity.Picture;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author CareyQ
 * @since 2021/3/16 21:56
 */
@Mapper
public interface PictureMapper extends SuperMapper<Picture> {
}

package com.careyq.module.picture.mapper;

import com.careyq.common.mapper.SuperMapper;
import com.careyq.module.picture.entity.Storage;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author CareyQ
 * @since 2021/3/17 16:25
 */
@Mapper
public interface StorageMapper extends SuperMapper<Storage> {
}

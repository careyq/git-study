package com.careyq.module.blog.vo;

import com.careyq.common.domain.vo.BaseVO;
import com.careyq.module.blog.entity.Comment;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author CareyQ
 * @since 2021/4/3 21:02
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ToString
public class CommentVO extends BaseVO<Comment> {
    private String uid;
    private String nickName;
    private String email;
    private String url;
    private String content;
    private String blogUid;
    private String status;
    private String parentUid;
    private String source;
}

package com.careyq.module.blog.vo;

import com.careyq.common.domain.vo.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author CareyQ
 * @since 2021/3/8 10:17
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CategoryVO extends BaseVO<CategoryVO> {
    /**
     * 分类名
     */
    private String categoryName;
}

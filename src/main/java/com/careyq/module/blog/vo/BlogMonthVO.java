package com.careyq.module.blog.vo;

import lombok.Data;
import lombok.ToString;

/**
 * @author CareyQ
 * @since 2021/3/24 23:58
 */
@Data
@ToString
public class BlogMonthVO {
    private String month;
    private String total;
}

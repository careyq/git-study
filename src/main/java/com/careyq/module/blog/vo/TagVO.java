package com.careyq.module.blog.vo;

import com.careyq.common.domain.vo.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author CareyQ
 * @since 2021/3/8 11:27
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TagVO extends BaseVO<TagVO> {
    /**
     * 标签名
     */
    private String name;
}

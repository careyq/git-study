package com.careyq.module.blog.vo;

import com.careyq.common.domain.vo.BaseVO;
import com.careyq.module.blog.entity.Comment;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author CareyQ
 * @since 2021/4/3 21:02
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ToString
public class LinkVO extends BaseVO<Comment> {
    private String uid;
    private String title;
    private String url;
    private String logo;
    private String email;
    private String status;
}

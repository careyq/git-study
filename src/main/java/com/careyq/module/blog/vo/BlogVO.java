package com.careyq.module.blog.vo;

import com.careyq.common.domain.vo.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * BlogVO
 *
 * @author CareyQ
 * @since 2021/3/7 10:29
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ToString
public class BlogVO extends BaseVO<BlogVO> {
    private String title;
    private String content;
    private String categoryUid;
    private String tagUid;
    private String imgUid;
    private String status;
    private String comment;
    private String isDelete;
    private String original;
    private String author;
    private String source;
}

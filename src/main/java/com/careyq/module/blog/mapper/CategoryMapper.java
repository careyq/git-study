package com.careyq.module.blog.mapper;

import com.careyq.common.mapper.SuperMapper;
import com.careyq.module.blog.entity.Category;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author CareyQ
 * @since 2021/3/8 10:29
 */
@Mapper
public interface CategoryMapper extends SuperMapper<Category> {
}

package com.careyq.module.blog.mapper;

import com.careyq.common.mapper.SuperMapper;
import com.careyq.module.blog.entity.Tag;
import org.apache.ibatis.annotations.Mapper;

/**
 * TagMapper
 *
 * @author CareyQ
 * @since 2021/3/8 11:25
 */
@Mapper
public interface TagMapper extends SuperMapper<Tag> {
}

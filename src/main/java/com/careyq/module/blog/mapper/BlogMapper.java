package com.careyq.module.blog.mapper;

import com.careyq.common.mapper.SuperMapper;
import com.careyq.module.blog.entity.Blog;
import com.careyq.module.blog.vo.BlogMonthVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * 博客表 Mapper 接口
 *
 * @author CareyQ
 * @since 2021/3/7 11:02
 */
@Mapper
public interface BlogMapper extends SuperMapper<Blog> {

    /**
     * 根据月份获取博客数量
     * @return 结果
     */
    @Select("SELECT DATE_FORMAT(create_time, '%Y-%m') month, COUNT(title) total FROM t_blog WHERE is_delete = 0 AND create_time BETWEEN DATE_SUB(NOW(), INTERVAL 6 MONTH) AND NOW() GROUP BY month")
    List<BlogMonthVO> getBlogCountByMonth();
}

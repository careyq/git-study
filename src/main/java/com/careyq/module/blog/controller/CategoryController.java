package com.careyq.module.blog.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.module.blog.service.CategoryService;
import com.careyq.module.blog.vo.CategoryVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author CareyQ
 * @since 2021/3/8 10:39
 */
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @PostMapping("/getList")
    public ResponseDTO getList(@RequestBody CategoryVO categoryVO) {
        return ResponseDTO.success(categoryService.getPageList(categoryVO));
    }

    @PostMapping("/add")
    public ResponseDTO add(@RequestBody CategoryVO categoryVO) {
        return categoryService.add(categoryVO);
    }

    @PostMapping("/edit")
    public ResponseDTO edit(@RequestBody CategoryVO categoryVO) {
        return categoryService.edit(categoryVO);
    }

    @GetMapping("/getCategory/{uid}")
    public ResponseDTO getCategory(@PathVariable String uid) {
        return ResponseDTO.success(categoryService.getCategory(uid));
    }

    @PostMapping("/deleteBatch")
    public ResponseDTO deleteBatch(@RequestBody List<CategoryVO> categoryVoList) {
        return categoryService.deleteBatch(categoryVoList);
    }

    @GetMapping("/getCategories")
    public ResponseDTO getCategories() {
        return ResponseDTO.success(categoryService.getCategories());
    }
}

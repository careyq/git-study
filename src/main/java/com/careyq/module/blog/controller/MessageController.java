package com.careyq.module.blog.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.module.blog.entity.Comment;
import com.careyq.module.blog.service.MessageService;
import com.careyq.module.blog.vo.CommentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author CareyQ
 * @since 2021/3/8 10:39
 */
@RestController
@RequestMapping("/message")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @PostMapping("/getComment")
    public ResponseDTO getCategory(@RequestBody CommentVO commentVO) {
        return ResponseDTO.success(messageService.getPage(commentVO));
    }

    @PostMapping("/passComment")
    public ResponseDTO passComment(@RequestBody String uid) {
        return messageService.passComment(uid);
    }

    @PostMapping("/deleteComment")
    public ResponseDTO delete(@RequestBody List<Comment> comments) {
        return messageService.delete(comments);
    }
}

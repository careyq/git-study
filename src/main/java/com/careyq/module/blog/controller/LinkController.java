package com.careyq.module.blog.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.constant.MessageConst;
import com.careyq.module.blog.entity.Link;
import com.careyq.module.blog.service.LinkService;
import com.careyq.module.blog.vo.LinkVO;
import com.careyq.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author CareyQ
 * @since 2021/4/6 23:03
 */
@RestController
@RequestMapping("/link")
public class LinkController {
    @Autowired
    private LinkService linkService;

    @PostMapping("/getLinks")
    public ResponseDTO getLinks(@RequestBody LinkVO linkVO) {
        return ResponseDTO.success(linkService.getPageList(linkVO));
    }

    @PostMapping("/passLink")
    public ResponseDTO passLink(@RequestBody LinkVO linkVO) {
        return linkService.passLink(linkVO);
    }

    @GetMapping("/get/{uid}")
    public ResponseDTO getLink(@PathVariable String uid) {
        if (StringUtils.isEmpty(uid)) {
            return ResponseDTO.error(MessageConst.QUERY_DEFAULT_ERROR);
        }
        return ResponseDTO.success(linkService.getById(uid));
    }

    @PostMapping("/addLink")
    public ResponseDTO addLink(@RequestBody LinkVO linkVO) {
        return linkService.addLinkFromAdmin(linkVO);
    }

    @PostMapping("/editLink")
    public ResponseDTO editLink(@RequestBody LinkVO linkVO) {
        return linkService.editLink(linkVO);
    }

    @PostMapping("/delete")
    public ResponseDTO delete(@RequestBody List<Link> links) {
        return linkService.deleteLinks(links);
    }
}

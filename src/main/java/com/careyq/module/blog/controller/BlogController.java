package com.careyq.module.blog.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.module.blog.service.BlogService;
import com.careyq.module.blog.vo.BlogVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 博客表 controller
 *
 * @author CareyQ
 * @since 2021/3/7 11:40
 */
@RestController
@RequestMapping("/blog")
@Slf4j
public class BlogController {
    @Autowired
    private BlogService blogService;

    @PostMapping("/getList")
    public ResponseDTO getList(@RequestBody BlogVO blogVO) {
        return ResponseDTO.success(blogService.getPageList(blogVO));
    }

    @PostMapping("/addPost")
    public ResponseDTO add(@RequestBody BlogVO blogVO) {
        return blogService.addBlog(blogVO);
    }

    @PostMapping("/edit")
    public ResponseDTO edit(@RequestBody BlogVO blogVO) {
        return blogService.editBlog(blogVO);
    }

    @PostMapping("/delete")
    public ResponseDTO delete(@RequestBody BlogVO blogVO) {
        return blogService.deleteBlog(blogVO);
    }

    @PostMapping("/deleteBatch")
    public ResponseDTO deleteBatch(@RequestBody List<BlogVO> blogVoList) {
        return blogService.deleteBatchBlog(blogVoList);
    }

    @GetMapping("/getPost/{uid}")
    public ResponseDTO getPost(@PathVariable String uid) {
        return ResponseDTO.success(blogService.getBlog(uid));
    }
}

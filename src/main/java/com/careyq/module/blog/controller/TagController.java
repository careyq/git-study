package com.careyq.module.blog.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.module.blog.service.TagService;
import com.careyq.module.blog.vo.TagVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 标签 Controller
 *
 * @author CareyQ
 * @since 2021/3/8 12:53
 */
@RestController
@RequestMapping("/tag")
public class TagController {

    @Autowired
    private TagService tagService;

    @PostMapping("/getList")
    public ResponseDTO getList(@RequestBody TagVO tagVO) {
        return ResponseDTO.success(tagService.getPageList(tagVO));
    }

    @PostMapping("/add")
    public ResponseDTO add(@RequestBody String tag) {
        return tagService.add(tag);
    }

    @PostMapping("/edit")
    public ResponseDTO edit(@RequestBody TagVO tagVO) {
        return tagService.edit(tagVO);
    }

    @GetMapping("/getTag/{uid}")
    public ResponseDTO getCategory(@PathVariable String uid) {
        return ResponseDTO.success(tagService.getTag(uid));
    }

    @PostMapping("/deleteBatch")
    public ResponseDTO deleteBatch(@RequestBody List<TagVO> tagVoList) {
        return tagService.deleteBatch(tagVoList);
    }
}

package com.careyq.module.blog.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.careyq.common.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 分类表
 *
 * @author CareyQ
 * @since 2021/3/8 10:21
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_category")
public class Category extends BaseEntity<Category> {
    private static final long serialVersionUID = 1L;

    /**
     * 分类名
     */
    private String categoryName;
    /**
     * 点击数
     */
    private Integer clickCount;
    /**
     * 该标签下的文章总数
     */
    @TableField(exist = false)
    private String postCount;
}

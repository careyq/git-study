package com.careyq.module.blog.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * @author CareyQ
 * @since 2021/4/3 20:57
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_comment")
@ToString
public class Comment extends Model<Comment> {
    @TableId(value = "uid", type = IdType.ASSIGN_UUID)
    private String uid;
    private String nickName;
    private String email;
    private String url;
    private String avatarUrl;
    private String content;
    private String blogUid;
    private String status;
    private String parentUid;
    private String source;
    private Boolean isAdmin;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date passTime;

    @TableField(exist = false)
    private String blogName;
    @TableField(exist = false)
    private String blogLinkId;
    @TableField(exist = false)
    private String parentNickName;
    @TableField(exist = false)
    private List<Comment> replyList;
}

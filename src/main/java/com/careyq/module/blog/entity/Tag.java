package com.careyq.module.blog.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.careyq.common.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 标签表
 *
 * @author CareyQ
 * @since 2021/3/8 11:23
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_tag")
public class Tag extends BaseEntity<Tag> {
    private static final long serialVersionUID = 1L;

    /**
     * 分类名
     */
    private String tagName;
    /**
     * 点击数
     */
    private Integer clickCount;
    /**
     * 拥有该标签的文章总数
     */
    @TableField(exist = false)
    private String postCount;
}

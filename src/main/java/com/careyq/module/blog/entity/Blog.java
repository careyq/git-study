package com.careyq.module.blog.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.careyq.common.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 博客表
 *
 * @author CareyQ
 * @since 2021/3/7 10:40
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_blog")
public class Blog extends BaseEntity<Blog> {
    private static final long serialVersionUID = 1L;
    /**
     * 博客标题
     */
    private String title;
    /**
     * 博客内容
     */
    private String content;
    /**
     * 博客分类UID
     */
    private String categoryUid;
    /**
     * 标签uid
     * updateStrategy = FieldStrategy.IGNORED ：表示更新时候忽略非空判断
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String tagUid;
    /**
     * 标题图片UID
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String imgUid;
    /**
     * 是否发布(0:否， 1:是)
     */
    private String status;
    /**
     * 是否开启评论(0:否， 1:是)
     */
    private String comment;
    /**
     * 是否删除(0:否， 1:是)
     */
    private String isDelete;
    /**
     * 是否原创
     */
    private String original;
    /**
     * 如果原创，作者为管理员名
     */
    private String author;
    /**
     * 文章出处
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String source;
    /**
     * 博客点击数
     */
    private Integer clickCount;
    /**
     * 管理员UID
     */
    private String adminUid;
    /**
     * 链接 ID
     */
    private String linkId;

    /* 以下字段不包含在表中 */
    /**
     * 博客分类名
     */
    @TableField(exist = false)
    private String category;
    @TableField(exist = false)
    private String imgUrl;
    @TableField(exist = false)
    private String relativeDate;
    @TableField(exist = false)
    private List<String> tags;
}

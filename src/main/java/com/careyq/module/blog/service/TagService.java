package com.careyq.module.blog.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.SuperService;
import com.careyq.module.blog.entity.Tag;
import com.careyq.module.blog.vo.TagVO;

import java.util.List;
import java.util.Map;

/**
 * 标签表 服务类
 *
 * @author CareyQ
 * @since 2021/3/8 11:26
 */
public interface TagService extends SuperService<Tag> {
    /**
     * 获取标签列表
     *
     * @param tagVO 标签视图层
     * @return 标签列表
     */
    IPage<Tag> getPageList(TagVO tagVO);

    /**
     * 添加标签 遍历标签 ID 数组，若没有则添加
     *
     * @param tagId 标签数组
     * @return 更新后的标签
     */
    String addTagNotFound(String[] tagId);

    /**
     * 根据 ID 查询
     *
     * @param uid ID
     * @return 标签
     */
    Tag getTag(String uid);

    /**
     * 添加
     *
     * @param name 名称
     * @return 操作结果消息
     */
    ResponseDTO add(String name);

    /**
     * 添加 Tag
     *
     * @param name 名称
     * @return id
     */
    String addTag(String name);

    /**
     * 修改
     *
     * @param tagVO 标签视图层
     * @return 操作结果消息
     */
    ResponseDTO edit(TagVO tagVO);

    /**
     * 批量删除
     *
     * @param tagVoList 列表
     * @return 操作结果消息
     */
    ResponseDTO deleteBatch(List<TagVO> tagVoList);

    /**
     * 获取标签总数
     *
     * @return 总数
     */
    Integer getTagCount();

    /**
     * 获取标签和文章总数
     *
     * @return 总数
     */
    Map<String, Object> getTagForPostCount();

    /**
     * 获取所有标签
     *
     * @return 标签列表
     */
    List<Tag> getAllTag();

    /**
     * 根据标签名获取标签
     *
     * @param tagName 标签名
     * @return 分类
     */
    Tag getTagByName(String tagName);

    Tag getTagByNameOrUid(String str);
}

package com.careyq.module.blog.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.SuperService;
import com.careyq.module.blog.entity.Category;
import com.careyq.module.blog.vo.CategoryVO;

import java.util.List;
import java.util.Map;

/**
 * 分类表 服务类
 *
 * @author CareyQ
 * @since 2021/3/8 10:25
 */
public interface CategoryService extends SuperService<Category> {
    /**
     * 获取分类列表
     *
     * @param categoryVO 分类视图层
     * @return IPage
     */
    IPage<Category> getPageList(CategoryVO categoryVO);

    /**
     * 根据分类 ID 查询
     *
     * @param uid 分类 ID
     * @return 分类
     */
    Category getCategory(String uid);

    /**
     * 添加分类
     *
     * @param categoryVO 视图
     * @return 操作结果消息
     */
    ResponseDTO add(CategoryVO categoryVO);

    /**
     * 修改分类
     *
     * @param categoryVO 分类视图层
     * @return 操作结果消息
     */
    ResponseDTO edit(CategoryVO categoryVO);

    /**
     * 批量删除分类
     *
     * @param categoryVoList 分类列表
     * @return 操作结果消息
     */
    ResponseDTO deleteBatch(List<CategoryVO> categoryVoList);

    /**
     * 获取分类总数
     *
     * @return 总数
     */
    Integer getCategoryCount();

    /**
     * 获取分类和文章总数
     *
     * @return 总数
     */
    Map<String, Object> getCategoryForPostCount();

    /**
     * 获取所有分类
     *
     * @return 分类
     */
    List<Category> getCategories();

    /**
     * 根据分类名获取分类
     *
     * @param categoryName 分类名
     * @return 分类
     */
    Category getCategoryByName(String categoryName);
}

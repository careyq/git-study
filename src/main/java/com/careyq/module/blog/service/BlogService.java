package com.careyq.module.blog.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.SuperService;
import com.careyq.module.blog.entity.Blog;
import com.careyq.module.blog.vo.BlogVO;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 博客表 服务类
 *
 * @author CareyQ
 * @since 2021/3/7 10:39
 */
public interface BlogService extends SuperService<Blog> {

    /**
     * 获取博客列表
     *
     * @param blogVO 博客视图层
     * @return IPage
     */
    IPage<Blog> getPageList(BlogVO blogVO);

    /**
     * 增加博客
     *
     * @param blogVO 博客视图层
     * @return 操作结果消息
     */
    ResponseDTO addBlog(BlogVO blogVO);

    /**
     * 修改博客
     *
     * @param blogVO 博客视图层
     * @return 操作结果消息
     */
    ResponseDTO editBlog(BlogVO blogVO);

    /**
     * 根据关键字和指定字段查询总数
     *
     * @param columnName 字段名
     * @param keyword    关键字
     * @return 总数
     */
    Integer getPostCount(String columnName, String keyword);

    /**
     * 删除博客
     *
     * @param blogVO 博客视图层
     * @return 操作结果消息
     */
    ResponseDTO deleteBlog(BlogVO blogVO);

    /**
     * 批量删除博客
     *
     * @param blogVoList 博客列表
     * @return 操作结果消息
     */
    ResponseDTO deleteBatchBlog(List<BlogVO> blogVoList);

    /**
     * 根据 ID 查询
     *
     * @param uid ID
     * @return 博客
     */
    Blog getBlog(String uid);

    /**
     * 通过月份获取博客数量
     *
     * @return 结果
     */
    Map<String, Object> getBlogCountByMonth();

    /**
     * 获取博客归档日期
     *
     * @return 日期 set
     */
    Map<String, Map<String, List<Blog>>> getBlogTimeList();

    Map<String, Object> getBlogByKeyword(String keywords, Long currentPage, Long pageSize);
}

package com.careyq.module.blog.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.impl.SuperServiceImpl;
import com.careyq.constant.CharacterConst;
import com.careyq.constant.MessageConst;
import com.careyq.constant.SqlConst;
import com.careyq.module.blog.entity.Link;
import com.careyq.module.blog.mapper.LinkMapper;
import com.careyq.module.blog.service.LinkService;
import com.careyq.module.blog.vo.LinkVO;
import com.careyq.util.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author CareyQ
 * @since 2021/4/4 0:25
 */
@Service
public class LinkServiceImpl extends SuperServiceImpl<LinkMapper, Link> implements LinkService {

    @Autowired
    private LinkService linkService;

    @Override
    public IPage<Link> getPageList(LinkVO linkVO) {

        QueryWrapper<Link> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(linkVO.getStatus())) {
            queryWrapper.eq(SqlConst.STATUS, linkVO.getStatus());
        }
        queryWrapper.orderByDesc(SqlConst.CREATE_TIME, SqlConst.STATUS);

        IPage<Link> pageList = new Page<>();
        pageList.setSize(linkVO.getPageSize());
        pageList.setCurrent(linkVO.getCurrentPage());

        return linkService.page(pageList, queryWrapper);
    }

    @Override
    public ResponseDTO editLink(LinkVO linkVO) {
        Link link = linkService.getById(linkVO.getUid());
        if (link == null) {
            return ResponseDTO.error(MessageConst.UPDATE_FAIL);
        }

        BeanUtils.copyProperties(linkVO, link);
        if (link.updateById()) {
            return ResponseDTO.success(MessageConst.UPDATE_SUCCESS);
        }
        return ResponseDTO.error(MessageConst.UPDATE_FAIL);
    }

    @Override
    public ResponseDTO passLink(LinkVO linkVO) {
        if (StringUtils.isEmpty(linkVO.getUid())) {
            return ResponseDTO.error(MessageConst.UPDATE_FAIL);
        }

        Link link = linkService.getById(linkVO.getUid());
        link.setStatus(CharacterConst.ONE);
        link.setPassTime(new Date(System.currentTimeMillis()));
        if (link.updateById()) {
            return ResponseDTO.success(MessageConst.UPDATE_SUCCESS);
        }

        return ResponseDTO.error(MessageConst.UPDATE_FAIL);
    }

    @Override
    public ResponseDTO addLinkFromWeb(LinkVO linkVO) {
        Link link = new Link();
        BeanUtils.copyProperties(linkVO, link);
        link.setStatus(CharacterConst.ZERO);
        link.setCreateTime(new Date(System.currentTimeMillis()));
        if (link.insert()) {
            return ResponseDTO.success(MessageConst.INSERT_SUCCESS);
        }
        return ResponseDTO.error(MessageConst.INSERT_FAIL);
    }

    @Override
    public ResponseDTO addLinkFromAdmin(LinkVO linkVO) {
        Link link = new Link();
        BeanUtils.copyProperties(linkVO, link);
        link.setStatus(CharacterConst.ONE);
        Date date = new Date(System.currentTimeMillis());
        link.setCreateTime(date);
        link.setPassTime(date);
        if (link.insert()) {
            return ResponseDTO.success(MessageConst.INSERT_SUCCESS);
        }
        return ResponseDTO.error(MessageConst.INSERT_FAIL);
    }

    @Override
    public ResponseDTO deleteLinks(List<Link> links) {
        if (links.isEmpty()) {
            return ResponseDTO.error(MessageConst.DELETE_FAIL);
        }

        List<String> uids = new ArrayList<>();
        links.forEach(item -> uids.add(item.getUid()));

        if (linkService.removeByIds(uids)) {
            return ResponseDTO.success(MessageConst.DELETE_SUCCESS);
        }
        return ResponseDTO.error(MessageConst.DELETE_FAIL);
    }

    @Override
    public List<Link> getAllLink() {
        QueryWrapper<Link> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.STATUS, CharacterConst.ONE);
        queryWrapper.orderByDesc(SqlConst.CREATE_TIME);
        return linkService.list(queryWrapper);
    }
}

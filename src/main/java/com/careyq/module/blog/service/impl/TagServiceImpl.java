package com.careyq.module.blog.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.impl.SuperServiceImpl;
import com.careyq.constant.CharacterConst;
import com.careyq.constant.MessageConst;
import com.careyq.constant.SqlConst;
import com.careyq.module.blog.entity.Blog;
import com.careyq.module.blog.entity.Category;
import com.careyq.module.blog.entity.Tag;
import com.careyq.module.blog.mapper.TagMapper;
import com.careyq.module.blog.service.BlogService;
import com.careyq.module.blog.vo.CategoryVO;
import com.careyq.module.blog.vo.TagVO;
import com.careyq.module.blog.service.TagService;
import com.careyq.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 标签表 服务实现类
 *
 * @author CareyQ
 * @since 2021/3/8 12:46
 */
@Service
public class TagServiceImpl extends SuperServiceImpl<TagMapper, Tag> implements TagService {

    @Autowired
    private TagService tagService;

    @Autowired
    private TagMapper tagMapper;

    @Autowired
    private BlogService blogService;

    @Override
    public IPage<Tag> getPageList(TagVO tagVO) {
        QueryWrapper<Tag> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotNull(tagVO.getKeyword()) && StringUtils.isNotEmpty(tagVO.getKeyword())) {
            queryWrapper.like(SqlConst.TAG_NAME, tagVO.getKeyword().trim());
        }
        queryWrapper.orderByDesc(SqlConst.CREATE_TIME);
        Page<Tag> page = new Page<>();
        page.setSize(tagVO.getPageSize());
        page.setCurrent(tagVO.getCurrentPage());
        page = tagService.page(page, queryWrapper);
        List<Tag> records = page.getRecords();
        for (Tag tag : records) {
            Integer postCount = blogService.getPostCount(SqlConst.TAG_UID, tag.getUid());
            tag.setPostCount(postCount.toString());
        }
        page.setRecords(records);
        return page;
    }

    @Override
    public String addTagNotFound(String[] tagId) {
        StringBuilder tags = new StringBuilder();

        for (int i = 0; i < tagId.length; i++) {
            if (isExistByUid(tagId[i]) && isExistByName(tagId[i])) {
                tags.append(tagId[i]);
            } else {
                Tag tag = new Tag();
                tag.setTagName(tagId[i]);
                tagService.save(tag);
                tags.append(tag.getUid());
            }

            if (i < tagId.length - 1) {
                tags.append(",");
            }
        }
        return tags.toString();
    }

    @Override
    public Tag getTag(String uid) {
        return tagService.getById(uid);
    }

    @Override
    public ResponseDTO add(String name) {
        if (isExistByName(name)) {
            return ResponseDTO.error(MessageConst.DATA_EXIST);
        }

        Tag tag = new Tag();
        tag.setTagName(name);

        boolean save = tagService.save(tag);
        if (save) {
            return ResponseDTO.success(MessageConst.INSERT_SUCCESS);
        }

        return ResponseDTO.error(MessageConst.INSERT_FAIL);
    }

    @Override
    public String addTag(String name) {
        Tag tag = new Tag();
        tag.setTagName(name);
        tagMapper.insert(tag);
        return tag.getUid();
    }

    @Override
    public ResponseDTO edit(TagVO tagVO) {
        Tag tag = tagService.getTag(tagVO.getUid());
        tag.setTagName(tagVO.getName());
        tag.setUpdateTime(new Date());
        boolean save = tag.updateById();

        if (save) {
            return ResponseDTO.success(MessageConst.UPDATE_SUCCESS);
        }
        return ResponseDTO.error(MessageConst.UPDATE_FAIL);
    }

    @Override
    public ResponseDTO deleteBatch(List<TagVO> tagVoList) {
        if (tagVoList.size() <= 0) {
            return ResponseDTO.error(MessageConst.PARAM_INCORRECT);
        }

        List<String> uidList = new ArrayList<>();
        tagVoList.forEach(item -> uidList.add(item.getUid()));

        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.IS_DELETE, CharacterConst.ZERO);

        for (String uid : uidList) {
            queryWrapper.like(SqlConst.TAG_UID, uid);
            int count = blogService.count(queryWrapper);

            if (count > 0) {
                return ResponseDTO.error(MessageConst.BLOG_UNDER_THIS_SORT);
            }
        }

        boolean flag = tagService.removeByIds(uidList);

        if (flag) {
            return ResponseDTO.success(MessageConst.DELETE_SUCCESS);
        }
        return ResponseDTO.error(MessageConst.DELETE_FAIL);
    }

    @Override
    public Integer getTagCount() {
        return tagService.count();
    }

    /**
     * 根据 id 判断标签是否存在
     *
     * @param uid 标签 id
     * @return true：存在 false：不存在
     */
    public boolean isExistByUid(String uid) {
        QueryWrapper<Tag> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.UID, uid).last("limit 1");
        Integer count = tagMapper.selectCount(queryWrapper);
        return count >= 1;
    }

    /**
     * 根据 name 判断标签是否存在
     *
     * @param name 标签 name
     * @return true：存在 false：不存在
     */
    public boolean isExistByName(String name) {
        QueryWrapper<Tag> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.TAG_NAME, name).last("limit 1");
        Integer count = tagMapper.selectCount(queryWrapper);
        return count >= 1;
    }

    @Override
    public Map<String, Object> getTagForPostCount() {
        TagVO tagVO = new TagVO();
        tagVO.setPageSize(1000L);
        tagVO.setCurrentPage(1L);
        IPage<Tag> pageList = getPageList(tagVO);
        List<Tag> tagList = pageList.getRecords();

        Map<String, Object> result = new LinkedHashMap<>(tagList.size());
        tagList.forEach(tag -> {
            result.put(tag.getTagName(), tag.getPostCount());
        });

        return result;
    }

    @Override
    public List<Tag> getAllTag() {
        return tagService.list();
    }

    @Override
    public Tag getTagByName(String tagName) {
        QueryWrapper<Tag> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.TAG_NAME, tagName);
        return tagService.getOne(queryWrapper);
    }

    @Override
    public Tag getTagByNameOrUid(String str) {
        Tag tag = tagService.getById(str);
        if (tag == null) {
            tag = getTagByName(str);
        }
        return tag;
    }
}

package com.careyq.module.blog.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.impl.SuperServiceImpl;
import com.careyq.constant.CharacterConst;
import com.careyq.constant.MessageConst;
import com.careyq.constant.SqlConst;
import com.careyq.module.blog.entity.Blog;
import com.careyq.module.blog.entity.Category;
import com.careyq.module.blog.entity.Tag;
import com.careyq.module.blog.mapper.BlogMapper;
import com.careyq.module.blog.service.BlogService;
import com.careyq.module.blog.service.CategoryService;
import com.careyq.module.blog.service.TagService;
import com.careyq.module.blog.vo.BlogMonthVO;
import com.careyq.module.blog.vo.BlogVO;
import com.careyq.module.picture.entity.Picture;
import com.careyq.module.picture.service.PictureService;
import com.careyq.module.system.entity.SystemConfig;
import com.careyq.module.system.service.SystemConfigService;
import com.careyq.util.DateUtils;
import com.careyq.util.SecurityUtils;
import com.careyq.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 博客表 服务实现类
 *
 * @author CareyQ
 * @since 2021/3/7 10:58
 */
@Service
@Slf4j
public class BlogServiceImpl extends SuperServiceImpl<BlogMapper, Blog> implements BlogService {

    @Autowired
    private BlogService blogService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private BlogMapper blogMapper;
    @Autowired
    private PictureService pictureService;
    @Autowired
    private SystemConfigService systemConfigService;
    @Autowired
    private TagService tagService;

    @Override
    public IPage<Blog> getPageList(BlogVO blogVO) {
        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        // 判断参数，构建搜索条件
        if (StringUtils.isNotEmptyAndNotNull(blogVO.getKeyword())) {
            queryWrapper.like(SqlConst.TITLE, blogVO.getKeyword().trim());
        }

        if (StringUtils.isNotEmpty(blogVO.getCategoryUid())) {
            queryWrapper.like(SqlConst.CATEGORY_UID, blogVO.getCategoryUid());
        }

        if (StringUtils.isNotEmpty(blogVO.getTagUid())) {
            queryWrapper.like(SqlConst.TAG_UID, blogVO.getTagUid());
        }

        if (StringUtils.isNotEmpty(blogVO.getOriginal())) {
            queryWrapper.like(SqlConst.ORIGINAL, blogVO.getOriginal());
        }

        // 分页
        Page<Blog> page = new Page<>();
        page.setCurrent(blogVO.getCurrentPage());
        page.setSize(blogVO.getPageSize());
        queryWrapper.eq(SqlConst.IS_DELETE, CharacterConst.ZERO);
        queryWrapper.orderByDesc(SqlConst.CREATE_TIME);

        IPage<Blog> pageList = blogService.page(page, queryWrapper);
        List<Blog> blogList = pageList.getRecords();

        blogList.forEach(item -> {
            if (StringUtils.isNotEmpty(item.getCategoryUid())) {
                item.setCategory(categoryService.getCategory(item.getCategoryUid()).getCategoryName());
            }
            if (StringUtils.isNotEmpty(item.getImgUid()) && StringUtils.isNotNull(item.getImgUid())) {
                item.setImgUrl(setImgUrl(item));
            }
        });

        pageList.setRecords(blogList);

        return pageList;
    }

    @Override
    public ResponseDTO addBlog(BlogVO blogVO) {
        if (isExistByName(blogVO.getTitle())) {
            return ResponseDTO.error(MessageConst.DATA_EXIST);
        }

        Blog blog = new Blog();
        BeanUtils.copyProperties(blogVO, blog);

        String tagUid = blog.getTagUid();

        if (StringUtils.isNotEmpty(tagUid)) {
            blog.setTagUid(StringUtils.join(setTags(tagUid)));
        }

        if (StringUtils.isEmpty(blogVO.getAuthor())) {
            blog.setAuthor(SecurityUtils.getUsername());
        }

        if (blogService.save(blog)) {
            return ResponseDTO.success(MessageConst.INSERT_SUCCESS);
        }
        return ResponseDTO.error(MessageConst.INSERT_FAIL);
    }

    @Override
    public ResponseDTO editBlog(BlogVO blogVO) {
        Blog blog = blogService.getById(blogVO.getUid());
        if (blog == null) {
            return ResponseDTO.error(MessageConst.UPDATE_FAIL);
        }
        BeanUtils.copyProperties(blogVO, blog);

        String tagUid = blog.getTagUid();
        if (StringUtils.isNotEmpty(tagUid)) {
            blog.setTagUid(StringUtils.join(setTags(tagUid)));
        }

        if (blog.updateById()) {
            return ResponseDTO.success(MessageConst.UPDATE_SUCCESS);
        }
        return ResponseDTO.error(MessageConst.UPDATE_FAIL);
    }

    @Override
    public ResponseDTO deleteBlog(BlogVO blogVO) {
        Blog blog = blogService.getById(blogVO.getUid());
        blog.setIsDelete(CharacterConst.ONE);
        boolean flag = blog.updateById();

        if (flag) {
            return ResponseDTO.success(MessageConst.DELETE_SUCCESS);
        }

        return ResponseDTO.error(MessageConst.DELETE_FAIL);
    }

    @Override
    public ResponseDTO deleteBatchBlog(List<BlogVO> blogVoList) {
        if (blogVoList.size() <= 0) {
            return ResponseDTO.error(MessageConst.PARAM_INCORRECT);
        }

        List<String> uidList = new ArrayList<>();
        blogVoList.forEach(item -> uidList.add(item.getUid()));

        List<Blog> blogList = blogService.listByIds(uidList);
        blogList.forEach(item -> item.setIsDelete(CharacterConst.ONE));

        boolean flag = blogService.updateBatchById(blogList);

        if (flag) {
            return ResponseDTO.success(MessageConst.DELETE_SUCCESS);
        }
        return ResponseDTO.error(MessageConst.DELETE_FAIL);
    }

    /**
     * 根据 title 判断博客是否存在
     *
     * @param title 博客名
     * @return true：存在 false：不存在
     */
    public boolean isExistByName(String title) {
        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.TITLE, title).last("limit 1");
        Integer count = blogMapper.selectCount(queryWrapper);
        return count >= 1;
    }

    @Override
    public Integer getPostCount(String columnName, String keyword) {
        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.IS_DELETE, (CharacterConst.ZERO));
        if (StringUtils.isNotNull(columnName) && SqlConst.CATEGORY_UID.equals(columnName)) {
            queryWrapper.eq(columnName, keyword);
        }

        if (StringUtils.isNotNull(columnName) && SqlConst.TAG_UID.equals(columnName)) {
            queryWrapper.like(columnName, keyword);
        }

        return blogMapper.selectCount(queryWrapper);
    }

    @Override
    public Blog getBlog(String uid) {
        return blogService.getById(uid);
    }

    @Override
    public Map<String, Object> getBlogCountByMonth() {
        List<BlogMonthVO> blogCountByMonth = blogMapper.getBlogCountByMonth();
        List<String> halfYearAllMonth = DateUtils.getHalfYearAllMonth();

        Map<String, Object> result = new LinkedHashMap<>(6);

        halfYearAllMonth.forEach(month -> {
            boolean flag = true;
            Iterator<BlogMonthVO> iterator = blogCountByMonth.iterator();
            while (iterator.hasNext()) {
                BlogMonthVO blogMonthVO = iterator.next();
                if (month.equals(blogMonthVO.getMonth())) {
                    result.put(month, blogMonthVO.getTotal());
                    iterator.remove();
                    flag = false;
                    break;
                }
            }
            if (flag) {
                result.put(month, CharacterConst.ZERO);
            }
        });

        return result;
    }

    @Override
    public Map<String, Map<String, List<Blog>>> getBlogTimeList() {
        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.IS_DELETE, CharacterConst.ZERO);
        queryWrapper.eq(SqlConst.STATUS, CharacterConst.ONE);
        queryWrapper.orderByDesc(SqlConst.CREATE_TIME);
        queryWrapper.select(Blog.class, item -> !item.getProperty().equals(SqlConst.CONTENT));
        List<Blog> list = blogService.list(queryWrapper);

        Map<String, Map<String, List<Blog>>> yearMap = new LinkedHashMap<>();

        for (Blog blog : list) {
            Date createTime = blog.getCreateTime();

            String year = new SimpleDateFormat("yyyy 年").format(createTime);
            String month = new SimpleDateFormat("MM 月").format(createTime);

            if (yearMap.get(year) == null) {
                List<Blog> blogList = new ArrayList<>();
                blogList.add(blog);
                yearMap.put(year, new LinkedHashMap<String, List<Blog>>() {{
                    put(month, blogList);
                }});
            } else if (yearMap.get(year).get(month) == null) {
                List<Blog> blogList = new ArrayList<>();
                blogList.add(blog);
                yearMap.get(year).put(month, blogList);
            } else {
                yearMap.get(year).get(month).add(blog);
            }

        }
        return yearMap;
    }

    @Override
    public Map<String, Object> getBlogByKeyword(String keywords, Long currentPage, Long pageSize) {
        final String keyword = keywords.trim();
        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        queryWrapper.and(wrapper -> wrapper.like(SqlConst.TITLE, keyword)
                .or().like(SqlConst.CONTENT, keyword));
        queryWrapper.eq(SqlConst.IS_DELETE, CharacterConst.ZERO);
        queryWrapper.eq(SqlConst.ORIGINAL, CharacterConst.ONE);
        queryWrapper.orderByDesc(SqlConst.CLICK_COUNT, SqlConst.CREATE_TIME);
        Page<Blog> page = new Page<>();
        page.setCurrent(currentPage);
        page.setSize(pageSize);

        IPage<Blog> iPage = blogService.page(page, queryWrapper);
        List<Blog> blogList = iPage.getRecords();

        blogList.forEach(item -> {
            item.setContent(setSummary(item));
            if (StringUtils.isNotEmpty(item.getCategoryUid())) {
                item.setCategory(categoryService.getCategory(item.getCategoryUid()).getCategoryName());
            }
            if (StringUtils.isNotEmpty(item.getImgUid())) {
                item.setImgUrl(setImgUrl(item));
            }
            item.setCategory(setCategory(item));
            item.setRelativeDate(DateUtils.getRelativeDate(item.getCreateTime()));
        });

        Map<String, Object> map = new HashMap<>(8);
        // 返回总记录数
        map.put(SqlConst.TOTAL, iPage.getTotal());
        // 返回总页数
        map.put(SqlConst.TOTAL_PAGE, iPage.getPages());
        // 返回当前页大小
        map.put(SqlConst.PAGE_SIZE, pageSize);
        // 返回当前页
        map.put(SqlConst.CURRENT_PAGE, iPage.getCurrent());
        // 返回数据
        map.put(SqlConst.BLOG_LIST, blogList);

        return map;
    }

    private String setSummary(Blog blog) {
        String content = blog.getContent();
        if (StringUtils.isNotEmpty(content)) {
            String regEx = "!\\[.*\\)|#+\\s|\\*+\\s|\\*+|~+|`|>+\\s|-+\\s|\\+\\s|\\[|\\].*\\)";
            Pattern pattern = Pattern.compile(regEx);
            Matcher matcher = pattern.matcher(content);
            String regStr = matcher.replaceAll("").replace('\n', ' ').replaceAll(" +", " ").trim();
            if (regStr.length() > 110) {
                regStr = regStr.substring(0, 110).trim();
            }
            return regStr + "...";
        }
        return content + "...";
    }

    private String setCategory(Blog blog) {
        String categoryUid = blog.getCategoryUid();
        Category category = categoryService.getById(categoryUid);
        return category.getCategoryName();
    }

    private String setImgUrl(Blog blog) {
        String imgUid = blog.getImgUid();
        Picture picture = pictureService.getById(imgUid);
        String url = picture.getUrl();

        SystemConfig systemConfig = systemConfigService.getSystemConfig();
        if (CharacterConst.ZERO.equals(systemConfig.getPicShow())) {
            return systemConfig.getLocalDomain() + url;
        } else {
            return systemConfig.getCosDomain() + url;
        }
    }

    private String setTags(String tagUid) {
        String[] uid = tagUid.split(",");
        String[] tempUid = new String[uid.length];

        for (int i = 0; i < uid.length; i++) {
            Tag tag = tagService.getTagByNameOrUid(uid[i]);

            if (tag == null) {
                tempUid[i] = tagService.addTag(uid[i]);
            } else {
                tempUid[i] = tag.getUid();
            }
        }
        return StringUtils.join(tempUid);
    }
}

package com.careyq.module.blog.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.impl.SuperServiceImpl;
import com.careyq.constant.CharacterConst;
import com.careyq.constant.MessageConst;
import com.careyq.constant.SqlConst;
import com.careyq.module.blog.entity.Blog;
import com.careyq.module.blog.entity.Category;
import com.careyq.module.blog.mapper.CategoryMapper;
import com.careyq.module.blog.service.BlogService;
import com.careyq.module.blog.service.CategoryService;
import com.careyq.module.blog.vo.CategoryVO;
import com.careyq.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 分类表 服务实现类
 *
 * @author CareyQ
 * @since 2021/3/8 10:27
 */
@Service
@Slf4j
public class CategoryServiceImpl extends SuperServiceImpl<CategoryMapper, Category> implements CategoryService {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private BlogService blogService;

    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public IPage<Category> getPageList(CategoryVO categoryVO) {
        QueryWrapper<Category> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotNull(categoryVO.getKeyword()) && StringUtils.isNotEmpty(categoryVO.getKeyword())) {
            queryWrapper.like(SqlConst.CATEGORY_NAME, categoryVO.getKeyword().trim());
        }
        queryWrapper.orderByDesc(SqlConst.CREATE_TIME);
        Page<Category> page = new Page<>();
        page.setSize(categoryVO.getPageSize());
        page.setCurrent(categoryVO.getCurrentPage());
        page = categoryService.page(page, queryWrapper);
        List<Category> records = page.getRecords();
        for (Category item : records) {
            Integer postCount = blogService.getPostCount(SqlConst.CATEGORY_UID, item.getUid());
            item.setPostCount(postCount.toString());
        }
        page.setRecords(records);
        return page;
    }

    @Override
    public Category getCategory(String uid) {
        return categoryService.getById(uid);
    }

    @Override
    public ResponseDTO add(CategoryVO categoryVO) {
        if (isExistByName(categoryVO.getCategoryName())) {
            return ResponseDTO.error(MessageConst.DATA_EXIST);
        }

        Category category = new Category();
        category.setCategoryName(categoryVO.getCategoryName());

        boolean save = categoryService.save(category);
        if (save) {
            return ResponseDTO.success(MessageConst.INSERT_SUCCESS);
        }

        return ResponseDTO.error(MessageConst.INSERT_FAIL);
    }

    @Override
    public ResponseDTO edit(CategoryVO categoryVO) {
        Category category = categoryService.getCategory(categoryVO.getUid());
        category.setCategoryName(categoryVO.getCategoryName());

        if (category.updateById()) {
            return ResponseDTO.success(MessageConst.UPDATE_SUCCESS);
        }
        return ResponseDTO.error(MessageConst.UPDATE_FAIL);
    }

    @Override
    public ResponseDTO deleteBatch(List<CategoryVO> categoryVoList) {
        if (categoryVoList.size() <= 0) {
            return ResponseDTO.error(MessageConst.PARAM_INCORRECT);
        }

        List<String> uidList = new ArrayList<>();

        for (CategoryVO item : categoryVoList) {
            uidList.add(item.getUid());
        }

        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.IS_DELETE, CharacterConst.ZERO);
        queryWrapper.in(SqlConst.CATEGORY_UID, uidList);
        int count = blogService.count(queryWrapper);

        if (count > 0) {
            return ResponseDTO.error(MessageConst.BLOG_UNDER_THIS_SORT);
        }

        boolean flag = categoryService.removeByIds(uidList);

        if (flag) {
            return ResponseDTO.success(MessageConst.DELETE_SUCCESS);
        }
        return ResponseDTO.error(MessageConst.DELETE_FAIL);
    }

    /**
     * 根据名字判断分类是否存在
     *
     * @param name 分类名
     * @return true：存在 false：不存在
     */
    public boolean isExistByName(String name) {
        QueryWrapper<Category> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.CATEGORY_NAME, name).last("limit 1");
        Integer count = categoryMapper.selectCount(queryWrapper);
        return count >= 1;
    }

    @Override
    public Integer getCategoryCount() {
        return categoryService.count();
    }

    @Override
    public Map<String, Object> getCategoryForPostCount() {
        CategoryVO categoryVO = new CategoryVO();
        categoryVO.setPageSize(1000L);
        categoryVO.setCurrentPage(1L);
        IPage<Category> pageList = getPageList(categoryVO);
        List<Category> categoryList = pageList.getRecords();

        Map<String, Object> result = new LinkedHashMap<>(categoryList.size());
        categoryList.forEach(category -> {
            result.put(category.getCategoryName(), category.getPostCount());
        });

        return result;
    }

    @Override
    public List<Category> getCategories() {
        return categoryService.list();
    }

    @Override
    public Category getCategoryByName(String categoryName) {
        QueryWrapper<Category> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.CATEGORY_NAME, categoryName);
        return categoryService.getOne(queryWrapper);
    }
}

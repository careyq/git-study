package com.careyq.module.blog.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.SuperService;
import com.careyq.module.blog.entity.Link;
import com.careyq.module.blog.vo.LinkVO;

import java.util.List;

/**
 * @author CareyQ
 * @since 2021/4/4 0:24
 */
public interface LinkService extends SuperService<Link> {
    /**
     * 获取友链分页
     *
     * @param linkVO 视图
     * @return 分页
     */
    IPage<Link> getPageList(LinkVO linkVO);

    /**
     * 通过审核
     *
     * @param linkVO 视图
     * @return 结果
     */
    ResponseDTO passLink(LinkVO linkVO);

    /**
     * 修改 link
     *
     * @param linkVO 视图
     * @return 结果
     */
    ResponseDTO editLink(LinkVO linkVO);

    /**
     * 提交审核
     *
     * @param linkVO 视图
     * @return 结果
     */
    ResponseDTO addLinkFromWeb(LinkVO linkVO);

    /**
     * 添加友链
     *
     * @param linkVO 视图
     * @return 结果
     */
    ResponseDTO addLinkFromAdmin(LinkVO linkVO);

    /**
     * 删除 link
     *
     * @param links link列表
     * @return 结果
     */
    ResponseDTO deleteLinks(List<Link> links);

    /**
     * 获取所有 link
     *
     * @return link 列表
     */
    List<Link> getAllLink();
}

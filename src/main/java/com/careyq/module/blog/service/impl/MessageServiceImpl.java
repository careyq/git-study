package com.careyq.module.blog.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.impl.SuperServiceImpl;
import com.careyq.constant.CharacterConst;
import com.careyq.constant.MessageConst;
import com.careyq.constant.SqlConst;
import com.careyq.module.blog.entity.Blog;
import com.careyq.module.blog.entity.Comment;
import com.careyq.module.blog.mapper.CommentMapper;
import com.careyq.module.blog.service.BlogService;
import com.careyq.module.blog.service.MessageService;
import com.careyq.module.blog.vo.CommentVO;
import com.careyq.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author CareyQ
 * @since 2021/4/4 0:25
 */
@Service
public class MessageServiceImpl extends SuperServiceImpl<CommentMapper, Comment> implements MessageService {

    @Autowired
    private MessageService messageService;

    @Autowired
    private BlogService blogService;

    @Override
    public IPage<Comment> getPage(CommentVO commentVO) {
        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(commentVO.getSource())) {
            queryWrapper.eq(SqlConst.SOURCE, commentVO.getSource());
        }

        queryWrapper.orderByDesc(SqlConst.CREATE_TIME, SqlConst.STATUS);
        IPage<Comment> page = new Page<>();
        page.setSize(commentVO.getPageSize());
        page.setCurrent(commentVO.getCurrentPage());
        IPage<Comment> pageList = messageService.page(page, queryWrapper);

        List<Comment> commentList = pageList.getRecords();
        commentList.forEach(item -> {
            if (SqlConst.SOURCE_POST.equals(item.getSource())) {
                String blogUid = item.getBlogUid();
                Blog blog = blogService.getById(blogUid);
                item.setBlogLinkId(blog.getLinkId());
                item.setBlogName(blog.getTitle());
            }

            if (StringUtils.isNotEmpty(item.getParentUid())) {
                item.setParentNickName(getParentComment(item.getParentUid()).getNickName());
            }
        });

        pageList.setRecords(commentList);

        return pageList;
    }

    @Override
    public ResponseDTO passComment(String uid) {
        Comment comment = messageService.getById(uid);
        comment.setStatus(CharacterConst.ONE);
        comment.setPassTime(new Date(System.currentTimeMillis()));
        comment.updateById();
        return ResponseDTO.success(MessageConst.UPDATE_SUCCESS);
    }

    @Override
    public ResponseDTO delete(List<Comment> comments) {
        if (comments.isEmpty()) {
            return ResponseDTO.error(MessageConst.DELETE_FAIL);
        }

        List<String> uids = new ArrayList<>();
        comments.forEach(item -> uids.add(item.getUid()));

        if (messageService.removeByIds(uids)) {
            return ResponseDTO.success(MessageConst.DELETE_SUCCESS);
        }
        return ResponseDTO.error(MessageConst.DELETE_FAIL);
    }

    private Comment getParentComment(String parentUid) {
        return messageService.getById(parentUid);
    }
}

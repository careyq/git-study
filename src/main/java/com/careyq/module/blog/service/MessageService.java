package com.careyq.module.blog.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.SuperService;
import com.careyq.module.blog.entity.Comment;
import com.careyq.module.blog.vo.CommentVO;

import java.util.List;

/**
 * @author CareyQ
 * @since 2021/4/4 0:24
 */
public interface MessageService extends SuperService<Comment> {

    /**
     * 获取评论
     *
     * @param commentVO 视图
     * @return 分页
     */
    IPage<Comment> getPage(CommentVO commentVO);

    /**
     * 通过
     *
     * @param uid 评论 id
     * @return 结果
     */
    ResponseDTO passComment(String uid);

    /**
     * 删除
     *
     * @param comments 列表
     * @return 结果
     */
    ResponseDTO delete(List<Comment> comments);
}

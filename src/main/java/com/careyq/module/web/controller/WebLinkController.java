package com.careyq.module.web.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.module.blog.service.LinkService;
import com.careyq.module.blog.vo.LinkVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author CareyQ
 * @since 2021/4/6 23:04
 */
@RestController
@RequestMapping("/web/link")
public class WebLinkController {

    @Autowired
    private LinkService linkService;

    @PostMapping("/add")
    public ResponseDTO addLink(@RequestBody LinkVO linkVO) {
        return linkService.addLinkFromWeb(linkVO);
    }

    @GetMapping("/get")
    public ResponseDTO getLinks() {
        return ResponseDTO.success(linkService.getAllLink());
    }
}

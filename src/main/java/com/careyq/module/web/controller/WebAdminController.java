package com.careyq.module.web.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.module.admin.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author CareyQ
 * @since 2021/4/8 21:24
 */
@RestController
@RequestMapping("/web/admin")
public class WebAdminController {

    @Autowired
    private AdminService adminService;

    @GetMapping("/get")
    public ResponseDTO get() {
        return ResponseDTO.success(adminService.getInfo());
    }
}

package com.careyq.module.web.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.module.blog.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author CareyQ
 * @since 2021/4/6 15:58
 */
@RestController
@RequestMapping("/web/categories")
public class WebCategoryController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping("/get")
    public ResponseDTO getCategories() {
        return ResponseDTO.success(categoryService.getCategories());
    }
}

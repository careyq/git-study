package com.careyq.module.web.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.constant.MessageConst;
import com.careyq.holder.RequestHolder;
import com.careyq.module.blog.entity.Blog;
import com.careyq.module.blog.service.BlogService;
import com.careyq.module.web.service.PostService;
import com.careyq.util.IpUtils;
import com.careyq.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.concurrent.TimeUnit;

/**
 * @author CareyQ
 * @since 2021/3/28 16:33
 */
@RestController
@RequestMapping("/web")
public class PostController {
    @Autowired
    private PostService postService;
    @Autowired
    private BlogService blogService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @GetMapping("/getPostList")
    public ResponseDTO getPostList(@RequestParam(name = "currentPage", required = false, defaultValue = "1") Long currentPage,
                                   @RequestParam(name = "pageSize", required = false, defaultValue = "5") Long pageSize) {
        return ResponseDTO.success(postService.getPostList(currentPage, pageSize));
    }

    @GetMapping("/getPost")
    public ResponseDTO getPost(@RequestParam(name = "linkId", required = false) String linkId) {
        HttpServletRequest request = RequestHolder.getRequest();
        assert request != null;
        String ip = IpUtils.getIpAddr(request);

        Blog blog = postService.getPostByUid(linkId);

        if (blog == null) {
            return ResponseDTO.error(MessageConst.POST_NOT_EXIST);
        }

        String result = stringRedisTemplate.opsForValue().get("POST_CLICK:" + ip + "#" + blog.getUid());

        if (StringUtils.isEmpty(result)) {
            Integer clickCount = blog.getClickCount() + 1;
            blog.setClickCount(clickCount);
            blog.updateById();

            stringRedisTemplate.opsForValue().set("POST_CLICK:" + ip + "#" + blog.getUid(), blog.getClickCount().toString(), 24, TimeUnit.HOURS);
        }

        return ResponseDTO.success(blog);
    }

    @GetMapping("/getArchivePost")
    public ResponseDTO getArchivePost() {
        return ResponseDTO.success(blogService.getBlogTimeList());
    }

    @GetMapping("/posts/categories/{categoryName}")
    public ResponseDTO getPostByCategory(@PathVariable String categoryName) {
        return ResponseDTO.success(postService.getPostsByCategory(categoryName));
    }

    @GetMapping("/posts/tags/{tagName}")
    public ResponseDTO getPostByTag(@PathVariable String tagName) {
        return ResponseDTO.success(postService.getPostsByTag(tagName));
    }
}

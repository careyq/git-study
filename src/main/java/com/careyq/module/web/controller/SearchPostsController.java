package com.careyq.module.web.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.module.blog.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author CareyQ
 * @since 2021/4/13 19:59
 */
@RestController
@RequestMapping("/web")
public class SearchPostsController {

    @Autowired
    private BlogService blogService;

    @GetMapping("/sqlSearch")
    public ResponseDTO sqlSearchBlog(@RequestParam() String keywords,
                                     @RequestParam(name = "currentPage", required = false, defaultValue = "1") Long currentPage,
                                     @RequestParam(name = "pageSize", required = false, defaultValue = "10") Long pageSize) {
        return ResponseDTO.success(blogService.getBlogByKeyword(keywords, currentPage, pageSize));
    }

}

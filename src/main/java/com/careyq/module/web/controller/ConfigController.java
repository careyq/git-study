package com.careyq.module.web.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.module.picture.service.PictureService;
import com.careyq.module.web.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author CareyQ
 * @since 2021/3/31 19:33
 */
@RestController
@RequestMapping("/web")
public class ConfigController {
    @Autowired
    private ConfigService configService;
    @Autowired
    private PictureService pictureService;

    @GetMapping("/getConfig")
    public ResponseDTO getConfig() {
        return configService.getConfig();
    }

    @GetMapping("/getLogo")
    public ResponseDTO getLogo() {
        return pictureService.getLogo();
    }
}

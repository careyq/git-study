package com.careyq.module.web.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.module.blog.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author CareyQ
 * @since 2021/4/6 13:34
 */
@RestController
@RequestMapping("/web/tag")
public class WebTagController {

    @Autowired
    private TagService tagService;

    @GetMapping("/getAll")
    public ResponseDTO getAllTag() {
        return ResponseDTO.success(tagService.getAllTag());
    }
}

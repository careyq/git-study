package com.careyq.module.web.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.module.web.service.CommentService;
import com.careyq.module.blog.vo.CommentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author CareyQ
 * @since 2021/4/3 21:04
 */
@RestController
@RequestMapping("/web")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @PostMapping("/addComment")
    public ResponseDTO addComment(@RequestBody CommentVO commentVO) {
        return commentService.addComment(commentVO);
    }

    @PostMapping("/getComment")
    public ResponseDTO getComment(@RequestBody CommentVO commentVO) {
        return ResponseDTO.success(commentService.getComments(commentVO));
    }
}

package com.careyq.module.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.careyq.common.service.impl.SuperServiceImpl;
import com.careyq.constant.CharacterConst;
import com.careyq.constant.SqlConst;
import com.careyq.holder.RequestHolder;
import com.careyq.module.blog.entity.Blog;
import com.careyq.module.blog.entity.Category;
import com.careyq.module.blog.entity.Tag;
import com.careyq.module.blog.mapper.BlogMapper;
import com.careyq.module.blog.service.CategoryService;
import com.careyq.module.blog.service.TagService;
import com.careyq.module.picture.entity.Picture;
import com.careyq.module.picture.service.PictureService;
import com.careyq.module.system.entity.SystemConfig;
import com.careyq.module.system.service.SystemConfigService;
import com.careyq.module.web.service.PostService;
import com.careyq.util.DateUtils;
import com.careyq.util.IpUtils;
import com.careyq.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author CareyQ
 * @since 2021/3/28 16:39
 */
@Service
public class PostServiceImpl extends SuperServiceImpl<BlogMapper, Blog> implements PostService {
    @Autowired
    private PostService postService;
    @Autowired
    private PictureService pictureService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private SystemConfigService systemConfigService;
    @Autowired
    private TagService tagService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public IPage<Blog> getPostList(Long currentPage, Long pageSize) {
        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.IS_DELETE, CharacterConst.ZERO);
        queryWrapper.eq(SqlConst.ORIGINAL, CharacterConst.ONE);
        queryWrapper.orderByDesc(SqlConst.CREATE_TIME);

        IPage<Blog> page = new Page<>();
        page.setCurrent(currentPage);
        page.setSize(pageSize);
        IPage<Blog> pageList = postService.page(page, queryWrapper);
        List<Blog> list = pageList.getRecords();

        if (list.isEmpty()) {
            return pageList;
        }

        list.forEach(post -> {
            post.setContent(setSummary(post));
            if (StringUtils.isNotEmpty(post.getImgUid())) {
                post.setImgUrl(setImgUrl(post));
            }
            post.setCategory(setCategory(post));
            post.setRelativeDate(DateUtils.getRelativeDate(post.getCreateTime()));
        });

        pageList.setRecords(list);

        return pageList;
    }

    @Override
    public Blog getPostByUid(String linkId) {

        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.IS_DELETE, CharacterConst.ZERO);
        queryWrapper.eq(SqlConst.LINK_ID, linkId);
        Blog post = postService.getOne(queryWrapper);

        if (post == null) {
            return null;
        }

        if (StringUtils.isNotEmpty(post.getImgUid())) {
            post.setImgUrl(setImgUrl(post));
        }

        if (StringUtils.isNotEmpty(post.getTagUid())) {
            post.setTags(setTags(post));
        }

        if (StringUtils.isNotEmpty(post.getCategoryUid())) {
            post.setCategory(setCategory(post));
        }

        return post;
    }

    @Override
    public List<Blog> getPostsByCategory(String categoryName) {
        Category category = categoryService.getCategoryByName(categoryName);
        if (category == null) {
            return null;
        }
        HttpServletRequest request = RequestHolder.getRequest();
        assert request != null;
        String ip = IpUtils.getIpAddr(request);

        String result = stringRedisTemplate.opsForValue().get("CATEGORY_CLICK:" + ip + "#" + category.getUid());

        if (StringUtils.isEmpty(result)) {
            Integer clickCount = category.getClickCount() + 1;
            category.setClickCount(clickCount);
            category.updateById();

            stringRedisTemplate.opsForValue().set("CATEGORY_CLICK:" + ip + "#" + category.getUid(), category.getClickCount().toString(), 24, TimeUnit.HOURS);
        }

        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.CATEGORY_UID, category.getUid());
        queryWrapper.eq(SqlConst.IS_DELETE, CharacterConst.ZERO);
        queryWrapper.eq(SqlConst.STATUS, CharacterConst.ONE);
        queryWrapper.orderByDesc(SqlConst.CREATE_TIME);

        return postService.list(queryWrapper);
    }

    @Override
    public List<Blog> getPostsByTag(String tagName) {
        Tag tag = tagService.getTagByName(tagName);
        if (tag == null) {
            return null;
        }

        HttpServletRequest request = RequestHolder.getRequest();
        assert request != null;
        String ip = IpUtils.getIpAddr(request);

        String result = stringRedisTemplate.opsForValue().get("TAG_CLICK:" + ip + "#" + tag.getUid());

        if (StringUtils.isEmpty(result)) {
            Integer clickCount = tag.getClickCount() + 1;
            tag.setClickCount(clickCount);
            tag.updateById();

            stringRedisTemplate.opsForValue().set("TAG_CLICK:" + ip + "#" + tag.getUid(), tag.getClickCount().toString(), 24, TimeUnit.HOURS);
        }

        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.IS_DELETE, CharacterConst.ZERO);
        queryWrapper.eq(SqlConst.STATUS, CharacterConst.ONE);
        queryWrapper.like(SqlConst.TAG_UID, tag.getUid());
        queryWrapper.orderByDesc(SqlConst.CREATE_TIME);

        return postService.list(queryWrapper);
    }

    private String setSummary(Blog blog) {
        String content = blog.getContent();
        if (StringUtils.isNotEmpty(content)) {
            String regEx = "!\\[.*\\)|#+\\s|\\*+\\s|\\*+|~+|`|>+\\s|-+\\s|\\+\\s|\\[|\\].*\\)";
            Pattern pattern = Pattern.compile(regEx);
            Matcher matcher = pattern.matcher(content);
            String regStr = matcher.replaceAll("").replace('\n', ' ').replaceAll(" +", " ").trim();
            if (regStr.length() > 110) {
                regStr = regStr.substring(0, 110).trim();
            }
            return regStr + "...";
        }
        return content + "...";
    }

    private String setImgUrl(Blog blog) {
        String imgUid = blog.getImgUid();
        Picture picture = pictureService.getById(imgUid);
        if (picture == null) {
            return "数据库中该图片不存在";
        }
        String url = picture.getUrl();

        SystemConfig systemConfig = systemConfigService.getSystemConfig();
        if (CharacterConst.ZERO.equals(systemConfig.getPicShow())) {
            return systemConfig.getLocalDomain() + url;
        } else {
            return systemConfig.getCosDomain() + url;
        }
    }

    private String setCategory(Blog blog) {
        String categoryUid = blog.getCategoryUid();
        Category category = categoryService.getById(categoryUid);
        return category.getCategoryName();
    }

    private List<String> setTags(Blog blog) {
        String[] split = blog.getTagUid().split(",");
        List<String> tagList = new ArrayList<>();
        for (String tagUid : split) {
            Tag tag = tagService.getById(tagUid);
            tagList.add(tag.getTagName());
        }
        return tagList;
    }
}

package com.careyq.module.web.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.SuperService;
import com.careyq.module.blog.entity.Comment;
import com.careyq.module.blog.vo.CommentVO;

/**
 * 评论表 服务类
 *
 * @author CareyQ
 * @since 2021/3/8 11:26
 */
public interface CommentService extends SuperService<Comment> {
    /**
     * 添加评论
     *
     * @param commentVO 评论 视图层
     * @return 结果
     */
    ResponseDTO addComment(CommentVO commentVO);

    /**
     * 获取评论
     * @param commentVO 评论视图层
     * @return 结果
     */
    IPage<Comment> getComments(CommentVO commentVO);
}

package com.careyq.module.web.service;

import com.careyq.common.domain.ResponseDTO;

/**
 * @author CareyQ
 * @since 2021/3/31 19:29
 */
public interface ConfigService {
    /**
     * 获取网站配置
     *
     * @return 结果
     */
    ResponseDTO getConfig();
}

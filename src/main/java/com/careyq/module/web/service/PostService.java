package com.careyq.module.web.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.SuperService;
import com.careyq.module.blog.entity.Blog;

import java.util.List;

/**
 * @author CareyQ
 * @since 2021/3/28 16:36
 */
public interface PostService extends SuperService<Blog> {
    /**
     * 获取文章列表
     *
     * @param currentPage 当前页
     * @param pageSize    页面数量
     * @return 文章列表
     */
    IPage<Blog> getPostList(Long currentPage, Long pageSize);

    /**
     * 根据 uid 获取文章
     *
     * @param linkId 文章 uid
     * @return 结果
     */
    Blog getPostByUid(String linkId);

    /**
     * 根据分类获取文章
     *
     * @param categoryName 分类名
     * @return 文章列表
     */
    List<Blog> getPostsByCategory(String categoryName);

    /**
     * 根据标签获取文章
     *
     * @param tagName 标签名
     * @return 文章列表
     */
    List<Blog> getPostsByTag(String tagName);
}

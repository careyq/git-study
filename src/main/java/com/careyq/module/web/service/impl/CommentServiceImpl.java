package com.careyq.module.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.impl.SuperServiceImpl;
import com.careyq.constant.CharacterConst;
import com.careyq.constant.MessageConst;
import com.careyq.constant.SqlConst;
import com.careyq.module.admin.service.AdminService;
import com.careyq.module.blog.entity.Comment;
import com.careyq.module.blog.mapper.CommentMapper;
import com.careyq.module.blog.vo.CommentVO;
import com.careyq.module.web.service.CommentService;
import com.careyq.util.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * @author CareyQ
 * @since 2021/4/3 21:01
 */
@Service
public class CommentServiceImpl extends SuperServiceImpl<CommentMapper, Comment> implements CommentService {

    @Autowired
    private CommentService commentService;
    @Autowired
    private AdminService adminService;

    private List<Comment> tempReply = new ArrayList<>();

    @Override
    public ResponseDTO addComment(CommentVO commentVO) {
        String nickName = commentVO.getNickName();
        String email = commentVO.getEmail();

        if (StringUtils.isEmpty(nickName) || StringUtils.isEmpty(email)) {
            return ResponseDTO.error(MessageConst.INSERT_FAIL);
        }

        Comment comment = new Comment();
        BeanUtils.copyProperties(commentVO, comment);
        comment.setNickName(nickName);
        comment.setEmail(email);
        comment.setIsAdmin(adminService.validate(nickName, email));

        if (comment.getIsAdmin()) {
            comment.setStatus(CharacterConst.ONE);
        } else {
            comment.setStatus(CharacterConst.ZERO);
        }

        if (StringUtils.isNotEmpty(commentVO.getUrl())) {
            comment.setUrl(commentVO.getUrl());
        }

        comment.setContent(commentVO.getContent());

        if (SqlConst.SOURCE_POST.equals(commentVO.getSource())) {
            comment.setBlogUid(commentVO.getBlogUid());
        }

        if (StringUtils.isNotEmpty(commentVO.getParentUid())) {
            comment.setParentUid(commentVO.getParentUid());
        }

        if (email.contains("@qq.com")) {
            comment.setAvatarUrl("https://q2.qlogo.cn/headimg_dl?dst_uin=" + email + "&spec=100");
        } else {
            String md5 = DigestUtils.md5DigestAsHex(email.getBytes(StandardCharsets.UTF_8));
            comment.setAvatarUrl("https://cdn.v2ex.com/gravatar/"+ md5 +"?s=96&d=mp&r=g");
        }

        comment.setSource(commentVO.getSource());
        comment.setCreateTime(new Date(System.currentTimeMillis()));
        comment.insert();
        return ResponseDTO.success(MessageConst.INSERT_SUCCESS);
    }

    @Override
    public IPage<Comment> getComments(CommentVO commentVO) {
        // 查询出所有父节点
        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.SOURCE, commentVO.getSource());
        if (StringUtils.isNotEmpty(commentVO.getBlogUid())) {
            queryWrapper.eq(SqlConst.BLOG_UID, commentVO.getBlogUid());
        }
        queryWrapper.isNull(SqlConst.PARENT_UID);
        queryWrapper.eq(SqlConst.STATUS, CharacterConst.ONE);
        queryWrapper.orderByDesc(SqlConst.CREATE_TIME);

        IPage<Comment> page = new Page<>();
        page.setCurrent(commentVO.getCurrentPage());
        page.setSize(commentVO.getPageSize());
        IPage<Comment> pageList = commentService.page(page, queryWrapper);
        List<Comment> list = pageList.getRecords();

        list.forEach(comment -> {
            String uid = comment.getUid();
            String nickName = comment.getNickName();
            QueryWrapper<Comment> childQueryWrapper = new QueryWrapper<>();
            childQueryWrapper.eq(SqlConst.STATUS, CharacterConst.ONE);
            childQueryWrapper.eq(SqlConst.PARENT_UID, uid);
            childQueryWrapper.orderByAsc(SqlConst.CREATE_TIME);
            List<Comment> childComments = commentService.list(childQueryWrapper);
            getChildren(childComments, nickName);
            comment.setReplyList(tempReply);
            tempReply = new ArrayList<>();
        });

        pageList.setRecords(list);
        return pageList;
    }

    private void getChildren(List<Comment> childComments, String parentNickName) {
        if (childComments.size() > 0) {
            childComments.forEach(childComment -> {
                childComment.setParentNickName(parentNickName);
                String nextParentNickName = childComment.getNickName();
//                childComment.setIsAdmin(adminService.validateEmail(childComment.getEmail()));
                tempReply.add(childComment);
                recursively(childComment.getUid(), nextParentNickName);
            });
        }
    }

    private void recursively(String childUid, String parentNickName) {
        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.STATUS, CharacterConst.ONE);
        queryWrapper.eq(SqlConst.PARENT_UID, childUid);
        queryWrapper.orderByAsc(SqlConst.CREATE_TIME);
        List<Comment> replyComments = commentService.list(queryWrapper);

        if (replyComments.size() > 0) {
            replyComments.forEach(item -> {
                String nickName = item.getNickName();
                String uid = item.getUid();
                item.setParentNickName(parentNickName);
//                item.setIsAdmin(adminService.validateEmail(item.getEmail()));
                tempReply.add(item);
                recursively(uid, nickName);
            });
        }
    }
}

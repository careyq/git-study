package com.careyq.module.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.module.system.entity.WebConfig;
import com.careyq.module.system.service.WebConfigService;
import com.careyq.module.web.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author CareyQ
 * @since 2021/3/31 19:30
 */
@Service
public class ConfigServiceImpl implements ConfigService {
    @Autowired
    private WebConfigService webConfigService;

    @Override
    public ResponseDTO getConfig() {
        QueryWrapper<WebConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.last("limit 1");
        WebConfig webConfig = webConfigService.getOne(queryWrapper);
        return ResponseDTO.success(webConfig);
    }
}

package com.careyq.module.menu.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.careyq.common.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 菜单表
 *
 * @author CareyQ
 * @since 2021/2/19 19:19
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_menu")
public class Menu extends BaseEntity<Menu> {
    /**
     * 菜单名称
     */
    private String menuName;
    /**
     * 父菜单 ID
     */
    private String parentUid;
    /**
     * 路由地址
     */
    private String path;
    /**
     * 组件路径
     */
    private String component;
    /**
     * 类型（D目录 M菜单 B按钮）
     */
    private String menuType;
    /**
     * 显示状态（0显示 1隐藏）
     */
    private String hidden;
    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 子菜单
     */
    @TableField(exist = false)
    private List<Menu> children = new ArrayList<>();

    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT_UPDATE, exist = false)
    private String createBy;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE, exist = false)
    private Date createTime;

    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT_UPDATE, exist = false)
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE, exist = false)
    private Date updateTime;
}

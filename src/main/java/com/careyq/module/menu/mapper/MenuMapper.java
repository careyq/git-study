package com.careyq.module.menu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.careyq.common.mapper.SuperMapper;
import com.careyq.module.menu.entity.Menu;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author CareyQ
 * @since 2021/2/19 19:33
 */
@Mapper
public interface MenuMapper extends SuperMapper<Menu> {

}

package com.careyq.module.menu.service;

import com.careyq.common.domain.vo.RouterVo;
import com.careyq.common.service.SuperService;
import com.careyq.module.menu.entity.Menu;
import com.careyq.module.system.entity.Role;

import java.util.List;

/**
 * 菜单业务层
 *
 * @author CareyQ
 * @since 2021/3/2 21:28
 */
public interface MenuService extends SuperService<Menu> {
    /**
     * 根据角色 uid 列表获取菜单列表
     *
     * @param roleList 角色 uid 列表
     * @return 菜单列表
     */
    List<Menu> getListByRoleList(List<Role> roleList);

    /**
     * 构建菜单结构，根据父节点 UID
     *
     * @param menus     菜单列表
     * @param parentUid 父节点 UID
     * @return 构建后的菜单列表
     */
    List<Menu> buildMenus(List<Menu> menus, String parentUid);

    /**
     * 构建前端路由所需要的菜单
     *
     * @param menus 菜单列表
     * @return 路由列表
     */
    List<RouterVo> buildRouters(List<Menu> menus);
}

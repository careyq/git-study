package com.careyq.module.menu.service.impl;

import com.careyq.common.domain.vo.MetaVo;
import com.careyq.common.domain.vo.RouterVo;
import com.careyq.common.service.impl.SuperServiceImpl;
import com.careyq.constant.CharacterConst;
import com.careyq.constant.MenuTypeConst;
import com.careyq.module.menu.entity.Menu;
import com.careyq.module.menu.mapper.MenuMapper;
import com.careyq.module.menu.service.MenuService;
import com.careyq.module.system.entity.Role;
import com.careyq.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author CareyQ
 * @since 2021/2/19 20:30
 */
@Service
public class MenuServiceImpl extends SuperServiceImpl<MenuMapper, Menu> implements MenuService {

    @Autowired
    private MenuService menuService;

    @Override
    public List<Menu> getListByRoleList(List<Role> roleList) {
        List<String> menuUisList = new ArrayList<>();

        roleList.forEach(item -> {
            String categoryUids = item.getCategoryUids();
            String[] menuUids = categoryUids.replace("[", "").replace("]", "").replace("\"", "").split(",");
            menuUisList.addAll(Arrays.asList(menuUids));
        });

        return menuService.listByIds(menuUisList);
    }

    @Override
    public List<Menu> buildMenus(List<Menu> menus, String parentUid) {
        List<Menu> menuList = new ArrayList<>();
        menus.forEach(item -> {
            if (item.getParentUid().equals(parentUid)) {
                recursionFn(menus, item);
                menuList.add(item);
            }
        });
        return menuList;
    }

    /**
     * 递归列表
     */
    private void recursionFn(List<Menu> list, Menu menu) {
        // 得到子节点列表
        List<Menu> childList = getChildList(list, menu);
        menu.setChildren(childList);
        for (Menu tChild : childList) {
            if (hasChild(list, tChild)) {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<Menu> getChildList(List<Menu> list, Menu menu) {
        List<Menu> menus = new ArrayList<>();

        list.forEach(item -> {
            if (item.getParentUid().equals(menu.getUid())) {
                menus.add(item);
            }
        });

        return menus;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<Menu> list, Menu menu) {
        return getChildList(list, menu).size() > 0;
    }

    /**
     * 构建前端标准结构的路由
     *
     * @param menus 菜单列表
     * @return 路由列表
     */
    @Override
    public List<RouterVo> buildRouters(List<Menu> menus) {

        List<RouterVo> routers = new LinkedList<>();

        menus.forEach(item -> {
            RouterVo routerVo = new RouterVo();
            routerVo.setName(getRouterName(item));
            routerVo.setHidden(CharacterConst.ONE.equals(item.getHidden()));
            routerVo.setPath(getRouterPath(item));
            routerVo.setComponent(getComponent(item));
            routerVo.setMeta(new MetaVo(item.getMenuName(), item.getIcon()));
            List<Menu> childrenList = item.getChildren();

            if (!childrenList.isEmpty() && MenuTypeConst.DIRECTORY.equals(item.getMenuType())) {
                routerVo.setChildren(buildRouters(childrenList));
            }

            routers.add(routerVo);
        });
        return routers;
    }

    /**
     * 是否为一级菜单 菜单
     *
     * @param menu 菜单
     * @return 结果
     */
    public boolean isFirstMenu(Menu menu) {
        return CharacterConst.ZERO.equals(menu.getParentUid()) && MenuTypeConst.MENU.equals(menu.getMenuType());
    }

    /**
     * 是否为一级菜单 目录
     *
     * @param menu 菜单
     * @return 结果
     */
    public boolean isDirectory(Menu menu) {
        return CharacterConst.ZERO.equals(menu.getParentUid()) && MenuTypeConst.DIRECTORY.equals(menu.getMenuType());
    }

    /**
     * 获取路由信息
     *
     * @param menu 菜单信息
     * @return 路由名称
     */
    public String getRouterName(Menu menu) {
        String routerName = StringUtils.capitalize(menu.getPath());
        if (isFirstMenu(menu)) {
            routerName = StringUtils.EMPTY;
        }
        return routerName;
    }

    /**
     * 获取路由地址
     *
     * @param menu 菜单信息
     * @return 路由地址
     */
    public String getRouterPath(Menu menu) {
        String routerPath = menu.getPath();
        if (isDirectory(menu)) {
            routerPath = "/" + menu.getPath();
        } else if (isFirstMenu(menu)) {
            routerPath = "/";
        }
        return routerPath;
    }

    /**
     * 获取组件
     *
     * @param menu 菜单信息
     * @return 组件信息
     */
    public String getComponent(Menu menu) {
        String component = MenuTypeConst.MAIN;
        if (StringUtils.isNotEmpty(menu.getComponent()) && !isFirstMenu(menu)) {
            component = menu.getComponent();
        }
        return component;
    }
}

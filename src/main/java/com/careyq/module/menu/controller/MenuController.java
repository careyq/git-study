package com.careyq.module.menu.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.module.admin.entity.Admin;
import com.careyq.module.login.domain.LoginUser;
import com.careyq.module.login.service.TokenService;
import com.careyq.module.menu.entity.Menu;

import com.careyq.module.menu.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author CareyQ
 * @since 2021/2/25 19:17
 */
@RestController
@RequestMapping("/auth")
public class MenuController {

    @Autowired
    private MenuService menuService;

    @Autowired
    private TokenService tokenService;

//    @GetMapping("/getMenu")
//    public ResponseDTO getMenu(HttpServletRequest request) {
//        LoginUser loginUser = tokenService.getLoginUser(request);
//        Admin admin = loginUser.getAdmin();
//        List<Menu> menus = menuService.selectMenuTreeByUserId(admin.getUid());
//        return ResponseDTO.success(menuService.buildMenus(menus));
//    }
}

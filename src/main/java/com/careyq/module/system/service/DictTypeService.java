package com.careyq.module.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.SuperService;
import com.careyq.module.system.entity.DictType;
import com.careyq.module.system.vo.DictTypeVO;

import java.util.List;

/**
 * 字典类型 服务类
 *
 * @author CareyQ
 * @since 2021/3/13 18:12
 */
public interface DictTypeService extends SuperService<DictType> {
    /**
     * 获取字典类型列表
     *
     * @param dictTypeVO 字典类型视图层
     * @return 列表
     */
    IPage<DictType> getPageList(DictTypeVO dictTypeVO);

    /**
     * 添加字典类型
     *
     * @param dictTypeVO 字典类型视图层
     * @return 操作结果消息
     */
    ResponseDTO addDictType(DictTypeVO dictTypeVO);

    /**
     * 修改字典类型
     *
     * @param dictTypeVO 字典类型视图层
     * @return 操作结果消息
     */
    ResponseDTO editDictType(DictTypeVO dictTypeVO);

    /**
     * 根据 uid 获取字典类型
     *
     * @param uid uid
     * @return 字典类型
     */
    DictType getDictTypeByUid(String uid);

    /**
     * 删除集合中的字典
     *
     * @param dictTypeVOList 集合
     * @return 操作结果消息
     */
    ResponseDTO deleteDictTypes(List<DictTypeVO> dictTypeVOList);
}

package com.careyq.module.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.impl.SuperServiceImpl;
import com.careyq.constant.BaseRedisConst;
import com.careyq.constant.CharacterConst;
import com.careyq.constant.MessageConst;
import com.careyq.exception.QueryException;
import com.careyq.module.system.entity.SystemConfig;
import com.careyq.module.system.entity.WebConfig;
import com.careyq.module.system.mapper.WebConfigMapper;
import com.careyq.module.system.service.SystemConfigService;
import com.careyq.module.system.service.WebConfigService;
import com.careyq.module.system.vo.WebConfigVO;
import com.careyq.third.RedisService;
import com.careyq.util.JsonUtils;
import com.careyq.util.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author CareyQ
 * @since 2021/3/31 18:39
 */
@Service
public class WebConfigServiceImpl extends SuperServiceImpl<WebConfigMapper, WebConfig> implements WebConfigService {

    @Autowired
    private WebConfigService webConfigService;
    @Autowired
    private RedisService redisService;

    @Override
    public ResponseDTO setConfig(WebConfigVO webConfigVO) {
        String uid = webConfigVO.getUid();
        if (StringUtils.isEmpty(uid)) {
            WebConfig webConfig = new WebConfig();
            BeanUtils.copyProperties(webConfigVO, webConfig);
            webConfig.insert();
            return ResponseDTO.success(MessageConst.INSERT_SUCCESS);
        }
        WebConfig webConfig = webConfigService.getById(uid);
        BeanUtils.copyProperties(webConfigVO, webConfig);

        webConfig.updateById();
        redisService.delete(BaseRedisConst.WEB_CONFIG);
        return ResponseDTO.success(MessageConst.UPDATE_SUCCESS);
    }

    @Override
    public ResponseDTO getConfig() {
        String webConfigJson = redisService.getEx(BaseRedisConst.WEB_CONFIG);
        if (StringUtils.isEmpty(webConfigJson)) {
            QueryWrapper<WebConfig> queryWrapper = new QueryWrapper<>();
            queryWrapper.last("limit 1");
            WebConfig webConfig = webConfigService.getOne(queryWrapper);

            if (webConfig == null) {
                return ResponseDTO.error(MessageConst.WEB_CONFIG_IS_NOT_EXIST);
            } else {
                redisService.setEx(BaseRedisConst.WEB_CONFIG, JsonUtils.objectToJson(webConfig), 24, TimeUnit.HOURS);
            }
            return ResponseDTO.success(webConfig);
        }

        WebConfig webConfig = JsonUtils.jsonToPojo(webConfigJson, WebConfig.class);
        if (webConfig == null) {
            throw new QueryException("系统配置转换错误，请检查系统配置，或者清空Redis后重试！");
        }
        return ResponseDTO.success(webConfig);
    }
}

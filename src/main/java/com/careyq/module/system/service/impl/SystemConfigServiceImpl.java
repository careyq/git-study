package com.careyq.module.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.impl.SuperServiceImpl;
import com.careyq.constant.BaseRedisConst;
import com.careyq.constant.CharacterConst;
import com.careyq.constant.MessageConst;
import com.careyq.constant.SqlConst;
import com.careyq.exception.QueryException;
import com.careyq.module.system.entity.SystemConfig;
import com.careyq.module.system.mapper.SystemConfigMapper;
import com.careyq.module.system.service.SystemConfigService;
import com.careyq.module.system.vo.SystemConfigVO;
import com.careyq.third.RedisService;
import com.careyq.util.JsonUtils;
import com.careyq.util.SecurityUtils;
import com.careyq.util.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author CareyQ
 * @since 2021/3/17 23:31
 */
@Service
public class SystemConfigServiceImpl extends SuperServiceImpl<SystemConfigMapper, SystemConfig> implements SystemConfigService {
    @Autowired
    private SystemConfigService systemConfigService;
    @Autowired
    private RedisService redisService;

    @Override
    public SystemConfig getSystemConfig() {
        String systemConfigJson = redisService.getEx(BaseRedisConst.SYSTEM_CONFIG);

        if (StringUtils.isEmpty(systemConfigJson)) {
            QueryWrapper<SystemConfig> queryWrapper = new QueryWrapper<>();
            queryWrapper.last("limit 1");
            SystemConfig systemConfig = systemConfigService.getOne(queryWrapper);
            if (systemConfig == null) {
                throw new QueryException(MessageConst.SYSTEM_CONFIG_IS_NOT_EXIST);
            } else {
                redisService.setEx(BaseRedisConst.SYSTEM_CONFIG, JsonUtils.objectToJson(systemConfig), 24, TimeUnit.HOURS);
            }
            return systemConfig;
        }
        SystemConfig systemConfig = JsonUtils.jsonToPojo(systemConfigJson, SystemConfig.class);
        if (systemConfig == null) {
            throw new QueryException("系统配置转换错误，请检查系统配置，或者清空Redis后重试！");
        }
        return systemConfig;
    }

    @Override
    public ResponseDTO setSystemConfig(SystemConfigVO systemConfigVO) {
        // 存储方式是否开启
        boolean uploadLocalOpen = CharacterConst.ONE.equals(systemConfigVO.getUploadLocal());
        boolean uploadCosOpen = CharacterConst.ONE.equals(systemConfigVO.getUploadCos());

        // 必须启用一种存储方式
        if (!uploadLocalOpen && !uploadCosOpen) {
            return ResponseDTO.error(MessageConst.FILE_MUST_BE_SELECT);
        }

        // 显示方式
        boolean isLocal = CharacterConst.ZERO.equals(systemConfigVO.getPicShow());
        boolean isCos = CharacterConst.ONE.equals(systemConfigVO.getPicShow());

        String localDomain = systemConfigVO.getLocalDomain();
        String localPath = systemConfigVO.getLocalPath();
        boolean localIsEmpty = StringUtils.isEmpty(localDomain) || StringUtils.isEmpty(localPath);

        // 启用本地存储必须配置本地存储信息
        if (uploadLocalOpen && localIsEmpty) {
            return ResponseDTO.error(MessageConst.MUST_BE_OPEN_LOCAL);
        }

        String cosSecretId = systemConfigVO.getCosSecretId();
        String cosSecretKey = systemConfigVO.getCosSecretKey();
        String cosBucket = systemConfigVO.getCosBucket();
        String cosRegion = systemConfigVO.getCosRegion();

        boolean cosIsEmpty = StringUtils.isEmpty(cosSecretId) ||
                StringUtils.isEmpty(cosSecretKey) ||
                StringUtils.isEmpty(cosBucket) ||
                StringUtils.isEmpty(cosRegion);

        if (uploadCosOpen && cosIsEmpty) {
            return ResponseDTO.error(MessageConst.MUST_BE_OPEN_COS);
        }

        if (StringUtils.isEmpty(systemConfigVO.getUid())) {
            SystemConfig systemConfig = new SystemConfig();
            BeanUtils.copyProperties(systemConfigVO, systemConfig);
            systemConfig.insert();

        } else {
            SystemConfig systemConfig = systemConfigService.getById(systemConfigVO.getUid());
            // 判断是否更新了图片显示方式
            /*if (!systemConfig.getPicShow().equals(systemConfigVO.getPicShow())) {

            }*/

            BeanUtils.copyProperties(systemConfigVO, systemConfig, SqlConst.UID);
            systemConfig.updateById();
        }
        redisService.delete(BaseRedisConst.SYSTEM_CONFIG);
        return ResponseDTO.success(MessageConst.UPDATE_SUCCESS);
    }

    @Override
    public ResponseDTO cleanRedis(String key) {
        if (StringUtils.isEmpty(key)) {
            return ResponseDTO.error(MessageConst.OPERATION_FAIL);
        }

        Set<String> keys;
        if (BaseRedisConst.ALL.equals(key)) {
            keys = redisService.keys(CharacterConst.STAR);
        } else if (BaseRedisConst.ROUTER.equals(key)){
            keys = redisService.keys(SecurityUtils.getUsername() + BaseRedisConst.ROUTER);
        } else {
            keys = redisService.keys(key + CharacterConst.STAR);
        }
        redisService.delete(keys);

        return ResponseDTO.success(MessageConst.OPERATION_SUCCESS);
    }
}

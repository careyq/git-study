package com.careyq.module.system.service;

import com.careyq.common.service.SuperService;
import com.careyq.module.admin.entity.Admin;
import com.careyq.module.system.entity.Role;

import java.util.Collection;

/**
 * 角色表 服务类
 *
 * @author CareyQ
 * @since 2021/3/14 14:40
 */
public interface RoleService extends SuperService<Role> {

}

package com.careyq.module.system.service;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.SuperService;
import com.careyq.module.system.entity.Todo;
import com.careyq.module.system.vo.TodoVO;

import java.util.List;

/**
 * @author CareyQ
 * @since 2021/3/26 13:14
 */
public interface TodoService extends SuperService<Todo> {

    /**
     * 添加 待办事项
     *
     * @param todoVO 待办事项 视图层
     * @return 操作结果消息
     */
    ResponseDTO addTodo(TodoVO todoVO);

    /**
     * 获取 待办事项
     *
     * @param status 待办事项的状态 0 进行中 1 已完成
     * @return 待办事项列表
     */
    List<Todo> getTodos(String status);

    /**
     * 修改 待办事项
     *
     * @param todoVO 待办事项 视图层
     * @return 操作结果消息
     */
    ResponseDTO editTodo(TodoVO todoVO);

    /**
     * 获取待办事项统计
     *
     * @param status 待办事项的状态 0 进行中 1 已完成
     * @return 统计
     */
    Integer getTodoCount(String status);

    /**
     * 删除 待办事项
     *
     * @param todoVO 待办事项 视图层
     * @return 操作结果消息
     */
    ResponseDTO deleteTodo(TodoVO todoVO);
}

package com.careyq.module.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.careyq.common.service.impl.SuperServiceImpl;
import com.careyq.constant.SqlConst;
import com.careyq.module.system.entity.DictData;
import com.careyq.module.system.entity.DictType;
import com.careyq.module.system.mapper.DictDataMapper;
import com.careyq.module.system.service.DictDataService;
import com.careyq.module.system.service.DictTypeService;
import com.careyq.module.system.vo.DictDataVO;
import com.careyq.module.system.vo.DictTypeVO;
import com.careyq.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author CareyQ
 * @since 2021/3/13 22:26
 */
@Service
public class DictDataServiceImpl extends SuperServiceImpl<DictDataMapper, DictData> implements DictDataService {

    @Autowired
    private DictDataService dictDataService;

    @Autowired
    private DictTypeService dictTypeService;

    @Override
    public IPage<DictData> getPageList(DictDataVO dictDataVO) {
        QueryWrapper<DictData> queryWrapper = new QueryWrapper<>();

        if (StringUtils.isNotEmpty(dictDataVO.getDictTypeUid())) {
            queryWrapper.eq(SqlConst.DICT_TYPE_UID, dictDataVO.getDictTypeUid());
        }

        if (StringUtils.isNotEmpty(dictDataVO.getDictLabel())) {
            queryWrapper.like(SqlConst.DICT_LABEL, dictDataVO.getDictLabel());
        }

        queryWrapper.orderByAsc(SqlConst.CREATE_TIME);

        Page<DictData> page = new Page<>();
        page.setCurrent(dictDataVO.getCurrentPage());
        page.setSize(dictDataVO.getPageSize());
        IPage<DictData> pageList = dictDataService.page(page, queryWrapper);
        List<DictData> dictDataList = pageList.getRecords();

        Set<String> dictTypeUidList = new HashSet<>();
        dictDataList.forEach(item -> {
            dictTypeUidList.add(item.getDictTypeUid());
        });

        Collection<DictType> dictTypeList = new ArrayList<>();
        if (dictTypeUidList.size() > 0) {
            dictTypeList = dictTypeService.listByIds(dictTypeUidList);
        }

        Map<String, DictType> dictTypeMap = new HashMap<>();
        dictTypeList.forEach(item -> {
            dictTypeMap.put(item.getUid(), item);
        });

        dictDataList.forEach(item -> {
            item.setDictType(dictTypeMap.get(item.getDictTypeUid()));
        });

        pageList.setRecords(dictDataList);
        return pageList;
    }
}

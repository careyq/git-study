package com.careyq.module.system.service.impl;

import com.careyq.common.service.impl.SuperServiceImpl;
import com.careyq.module.admin.entity.Admin;
import com.careyq.module.system.entity.Role;
import com.careyq.module.system.mapper.RoleMapper;
import com.careyq.module.system.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author CareyQ
 * @since 2021/3/14 14:41
 */
@Service
public class RoleServiceImpl extends SuperServiceImpl<RoleMapper, Role> implements RoleService {


}

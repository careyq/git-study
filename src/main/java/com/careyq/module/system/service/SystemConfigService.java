package com.careyq.module.system.service;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.SuperService;
import com.careyq.module.system.entity.SystemConfig;
import com.careyq.module.system.vo.SystemConfigVO;

/**
 * @author CareyQ
 * @since 2021/3/17 23:30
 */
public interface SystemConfigService extends SuperService<SystemConfig> {
    /**
     * 获取系统配置
     * @return 系统配置
     */
    SystemConfig getSystemConfig();

    /**
     * 设置系统配置
     * @param systemConfigVO 系统配置 视图层
     * @return 操作结果消息
     */
    ResponseDTO setSystemConfig(SystemConfigVO systemConfigVO);

    ResponseDTO cleanRedis(String key);
}

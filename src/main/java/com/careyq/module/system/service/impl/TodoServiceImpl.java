package com.careyq.module.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.impl.SuperServiceImpl;
import com.careyq.constant.CharacterConst;
import com.careyq.constant.MessageConst;
import com.careyq.constant.SqlConst;
import com.careyq.module.system.entity.Todo;
import com.careyq.module.system.mapper.TodoMapper;
import com.careyq.module.system.service.TodoService;
import com.careyq.module.system.vo.TodoVO;
import com.careyq.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author CareyQ
 * @since 2021/3/26 13:14
 */
@Service
public class TodoServiceImpl extends SuperServiceImpl<TodoMapper, Todo> implements TodoService {

    @Autowired
    private TodoService todoService;

    @Override
    public ResponseDTO addTodo(TodoVO todoVO) {
        Todo todo = new Todo();
        todo.setContent(todoVO.getContent());
        todo.setDone(todoVO.getDone());
        todo.insert();
        return ResponseDTO.success(MessageConst.INSERT_SUCCESS);
    }

    @Override
    public List<Todo> getTodos(String status) {
        QueryWrapper<Todo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.CREATE_BY, SecurityUtils.getUsername());
        queryWrapper.eq(SqlConst.DONE, status);
        if (CharacterConst.ZERO.equals(status)) {
            queryWrapper.orderByDesc(SqlConst.CREATE_TIME);
        }

        if (CharacterConst.ONE.equals(status)) {
            queryWrapper.orderByDesc(SqlConst.UPDATE_TIME);
        }

        return todoService.list(queryWrapper);
    }

    @Override
    public ResponseDTO editTodo(TodoVO todoVO) {
        String uid = todoVO.getUid();
        Todo todo = todoService.getById(uid);
        todo.setContent(todoVO.getContent());
        todo.setDone(todoVO.getDone());
        if (todo.updateById()) {
            return ResponseDTO.success(MessageConst.UPDATE_SUCCESS);
        }

        return ResponseDTO.error(MessageConst.UPDATE_FAIL);
    }

    @Override
    public Integer getTodoCount(String status) {
        QueryWrapper<Todo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.CREATE_BY, SecurityUtils.getUsername());
        queryWrapper.eq(SqlConst.DONE, status);

        return todoService.count(queryWrapper);
    }

    @Override
    public ResponseDTO deleteTodo(TodoVO todoVO) {
        Todo todo = todoService.getById(todoVO.getUid());
        if (todo.deleteById()) {
            return ResponseDTO.success(MessageConst.DELETE_SUCCESS);
        }
        return ResponseDTO.error(MessageConst.DELETE_FAIL);
    }
}

package com.careyq.module.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.impl.SuperServiceImpl;
import com.careyq.constant.MessageConst;
import com.careyq.constant.SqlConst;
import com.careyq.module.system.entity.DictType;
import com.careyq.module.system.mapper.DictTypeMapper;
import com.careyq.module.system.service.DictTypeService;
import com.careyq.module.system.vo.DictTypeVO;
import com.careyq.util.SecurityUtils;
import com.careyq.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 字典类型 服务实现类
 *
 * @author CareyQ
 * @since 2021/3/13 18:14
 */
@Service
public class DictTypeServiceImpl extends SuperServiceImpl<DictTypeMapper, DictType> implements DictTypeService {

    @Autowired
    private DictTypeService dictTypeService;
    @Autowired
    private DictTypeMapper dictTypeMapper;

    @Override
    public IPage<DictType> getPageList(DictTypeVO dictTypeVO) {
        QueryWrapper<DictType> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(dictTypeVO.getDictName())) {
            queryWrapper.like(SqlConst.DICT_NAME, dictTypeVO.getDictName().trim());
        }

        if (StringUtils.isNotEmpty(dictTypeVO.getDictType())) {
            queryWrapper.like(SqlConst.DICT_TYPE, dictTypeVO.getDictType().trim());
        }

        queryWrapper.orderByDesc(SqlConst.CREATE_TIME);

        Page<DictType> page = new Page<>();
        page.setCurrent(dictTypeVO.getCurrentPage());
        page.setSize(dictTypeVO.getPageSize());
        return dictTypeService.page(page, queryWrapper);
    }

    @Override
    public ResponseDTO addDictType(DictTypeVO dictTypeVO) {
        if (isExist(dictTypeVO.getDictName(), dictTypeVO.getDictType())) {
            return ResponseDTO.error(MessageConst.DATA_EXIST);
        }

        DictType dictType = new DictType();
        dictType = setDictType(dictType, dictTypeVO, false);
        boolean save = dictTypeService.save(dictType);

        if (save) {
            return ResponseDTO.success(MessageConst.INSERT_SUCCESS);
        }

        return ResponseDTO.error(MessageConst.INSERT_FAIL);
    }

    @Override
    public ResponseDTO editDictType(DictTypeVO dictTypeVO) {
        DictType dictType = dictTypeService.getById(dictTypeVO.getUid());
        dictType = setDictType(dictType, dictTypeVO, true);

        boolean save = dictType.updateById();

        if (save) {
            return ResponseDTO.success(MessageConst.UPDATE_SUCCESS);
        }

        return ResponseDTO.error(MessageConst.UPDATE_FAIL);
    }

    @Override
    public DictType getDictTypeByUid(String uid) {
        return dictTypeService.getById(uid);
    }

    @Override
    public ResponseDTO deleteDictTypes(List<DictTypeVO> dictTypeVOList) {
        List<String> uids = new ArrayList<>();
        dictTypeVOList.forEach(item -> {
            uids.add(item.getUid());
        });

        boolean flag = dictTypeService.removeByIds(uids);

        if (flag) {
            return ResponseDTO.success(MessageConst.DELETE_SUCCESS);
        }

        return ResponseDTO.error(MessageConst.DELETE_FAIL);
    }

    /**
     * 判断字典是否存在
     *
     * @param dictName 字典名
     * @param dictType 字典类型
     * @return true：存在 false：不存在
     */
    public boolean isExist(String dictName, String dictType) {
        QueryWrapper<DictType> queryWrapper = new QueryWrapper<>();
        Integer count = 0;

        queryWrapper.eq(SqlConst.DICT_NAME, dictName).last("limit 1");
        count = dictTypeMapper.selectCount(queryWrapper);

        if (count == 0) {
            queryWrapper.clear();
            queryWrapper.eq(SqlConst.DICT_TYPE, dictType).last("limit 1");
            count = dictTypeMapper.selectCount(queryWrapper);
        }
        return count >= 1;
    }

    public DictType setDictType(DictType dictType, DictTypeVO dictTypeVO, boolean update) {
        dictType.setCreateBy(SecurityUtils.getUsername());
        dictType.setDictName(dictTypeVO.getDictName());
        dictType.setDictType(dictTypeVO.getDictType());

        if (update) {
            dictType.setUpdateBy(SecurityUtils.getUsername());
            dictType.setUpdateTime(new Date());
        } else {
            dictType.setCreateBy(SecurityUtils.getUsername());
            dictType.setCreateTime(new Date());
        }

        dictType.setRemark(dictTypeVO.getRemark());
        return dictType;
    }
}

package com.careyq.module.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.careyq.common.service.SuperService;
import com.careyq.module.system.entity.DictData;
import com.careyq.module.system.vo.DictDataVO;

/**
 * @author CareyQ
 * @since 2021/3/13 22:21
 */
public interface DictDataService extends SuperService<DictData> {
    /**
     * 获取数据字典列表
     *
     * @param dictDataVO 字典数据视图层
     * @return 列表
     */
    IPage<DictData> getPageList(DictDataVO dictDataVO);
}

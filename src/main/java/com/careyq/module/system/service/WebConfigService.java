package com.careyq.module.system.service;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.SuperService;
import com.careyq.module.system.entity.WebConfig;
import com.careyq.module.system.vo.WebConfigVO;

/**
 * @author CareyQ
 * @since 2021/3/31 18:38
 */
public interface WebConfigService extends SuperService<WebConfig> {
    /**
     * 设置
     *
     * @param webConfigVO 视图
     * @return 结果
     */
    ResponseDTO setConfig(WebConfigVO webConfigVO);

    /**
     * 获取
     * @return 结果
     */
    ResponseDTO getConfig();
}

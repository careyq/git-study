package com.careyq.module.system.mapper;

import com.careyq.common.mapper.SuperMapper;
import com.careyq.module.system.entity.DictData;
import com.careyq.module.system.entity.DictType;
import org.apache.ibatis.annotations.Mapper;

/**
 * 字典数据 Mapper 接口
 *
 * @author CareyQ
 * @since 2021/3/13 18:15
 */
@Mapper
public interface DictDataMapper extends SuperMapper<DictData> {
}

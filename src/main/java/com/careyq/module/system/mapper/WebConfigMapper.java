package com.careyq.module.system.mapper;

import com.careyq.common.mapper.SuperMapper;
import com.careyq.module.system.entity.SystemConfig;
import com.careyq.module.system.entity.WebConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author CareyQ
 * @since 2021/3/17 23:29
 */
@Mapper
public interface WebConfigMapper extends SuperMapper<WebConfig> {
}

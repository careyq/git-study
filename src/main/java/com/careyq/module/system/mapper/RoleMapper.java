package com.careyq.module.system.mapper;

import com.careyq.common.mapper.SuperMapper;
import com.careyq.module.system.entity.Role;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author CareyQ
 * @since 2021/3/14 14:42
 */
@Mapper
public interface RoleMapper extends SuperMapper<Role> {
}

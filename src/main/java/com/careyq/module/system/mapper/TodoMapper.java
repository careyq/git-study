package com.careyq.module.system.mapper;

import com.careyq.common.mapper.SuperMapper;
import com.careyq.module.system.entity.Todo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author CareyQ
 * @since 2021/3/26 13:13
 */
@Mapper
public interface TodoMapper extends SuperMapper<Todo> {
}

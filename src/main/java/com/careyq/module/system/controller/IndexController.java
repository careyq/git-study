package com.careyq.module.system.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.constant.CommonConst;
import com.careyq.module.blog.service.*;
import com.careyq.module.web.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author CareyQ
 * @since 2021/3/24 14:17
 */
@RestController
@RequestMapping("/index")
public class IndexController {

    @Autowired
    private BlogService blogService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private TagService tagService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private LinkService linkService;

    @GetMapping("/init")
    public ResponseDTO init() {
        Map<String, Integer> map = new HashMap<>(5);
        map.put(CommonConst.BLOG_COUNT, blogService.getPostCount(null, null));
        map.put(CommonConst.CATEGORY_COUNT, categoryService.getCategoryCount());
        map.put(CommonConst.TAG_COUNT, tagService.getTagCount());
        map.put(CommonConst.COMMENT_COUNT, messageService.count());
        map.put(CommonConst.LINK_COUNT, linkService.count());
        return ResponseDTO.success(map);
    }

    @GetMapping("/getSystemCount")
    public ResponseDTO getSystemCount() {
        List<Map<String, Object>> countList = new ArrayList<>();
        countList.add(blogService.getBlogCountByMonth());
        countList.add(categoryService.getCategoryForPostCount());
        countList.add(tagService.getTagForPostCount());
        return ResponseDTO.success(countList);
    }
}

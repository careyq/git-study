package com.careyq.module.system.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.module.system.service.WebConfigService;
import com.careyq.module.system.vo.WebConfigVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author CareyQ
 * @since 2021/3/31 18:39
 */
@RestController
@RequestMapping("/system/web/config")
public class WebConfigController {
    @Autowired
    private WebConfigService webConfigService;

    @PostMapping("/set")
    public ResponseDTO setConfig(@RequestBody WebConfigVO webConfigVO) {
        return webConfigService.setConfig(webConfigVO);
    }

    @GetMapping("/get")
    public ResponseDTO getConfig() {
        return webConfigService.getConfig();
    }
}

package com.careyq.module.system.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.module.system.service.DictTypeService;
import com.careyq.module.system.vo.DictTypeVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 字典类型 controller
 *
 * @author CareyQ
 * @since 2021/3/13 18:24
 */
@RestController
@RequestMapping("/system/dict/type")
@Slf4j
public class DictTypeController {

    @Autowired
    private DictTypeService dictTypeService;

    @GetMapping("/list")
    public ResponseDTO list(DictTypeVO dictTypeVO) {
        log.info("获取字典类型列表");
        return ResponseDTO.success(dictTypeService.getPageList(dictTypeVO));
    }

    @PostMapping("/add")
    public ResponseDTO add(@RequestBody DictTypeVO dictTypeVO) {
        return dictTypeService.addDictType(dictTypeVO);
    }

    @PostMapping("/edit")
    public ResponseDTO edit(@RequestBody DictTypeVO dictTypeVO) {
        return dictTypeService.editDictType(dictTypeVO);
    }

    @GetMapping(value = "/{dictUid}")
    public ResponseDTO getInfo(@PathVariable String dictUid) {
        return ResponseDTO.success(dictTypeService.getDictTypeByUid(dictUid));
    }

    @PostMapping("/delete")
    public ResponseDTO delete(@RequestBody List<DictTypeVO> dictTypeVOList) {
        return dictTypeService.deleteDictTypes(dictTypeVOList);
    }
}

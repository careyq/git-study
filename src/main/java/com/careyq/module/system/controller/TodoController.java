package com.careyq.module.system.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.constant.CharacterConst;
import com.careyq.module.system.service.TodoService;
import com.careyq.module.system.vo.TodoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

/**
 * @author CareyQ
 * @since 2021/3/26 13:15
 */
@RestController
@RequestMapping("/system/todo")
public class TodoController {

    @Autowired
    private TodoService todoService;

    @PostMapping("/add")
    public ResponseDTO add(@RequestBody TodoVO todoVO) {
        return todoService.addTodo(todoVO);
    }

    @GetMapping("/getProcess")
    public ResponseDTO getProcessTodo() {
        return ResponseDTO.success(todoService.getTodos(CharacterConst.ZERO));
    }

    @GetMapping("/getCompleted")
    public ResponseDTO getCompletedTodo() {
        return ResponseDTO.success(todoService.getTodos(CharacterConst.ONE));
    }

    @PostMapping("/edit")
    public ResponseDTO editTodo(@RequestBody TodoVO todoVO) {
        return todoService.editTodo(todoVO);
    }

    @GetMapping("/getProcessCount")
    public ResponseDTO getProcessTodoCount() {
        return ResponseDTO.success(todoService.getTodoCount(CharacterConst.ZERO));
    }

    @GetMapping("/getCompletedCount")
    public ResponseDTO getCompletedTodoCount() {
        return ResponseDTO.success(todoService.getTodoCount(CharacterConst.ONE));
    }

    @PostMapping("/delete")
    public ResponseDTO deleteTodo(@RequestBody TodoVO todoVO) {
        return todoService.deleteTodo(todoVO);
    }
}

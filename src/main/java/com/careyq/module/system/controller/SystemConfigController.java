package com.careyq.module.system.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.module.system.service.SystemConfigService;
import com.careyq.module.system.vo.SystemConfigVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author CareyQ
 * @since 2021/3/18 0:03
 */
@RestController
@RequestMapping("/system/system/config")
@Slf4j
public class SystemConfigController {
    @Autowired
    private SystemConfigService systemConfigService;

    @GetMapping("/get")
    public ResponseDTO get() {
        return ResponseDTO.success(systemConfigService.getSystemConfig());
    }

    @PostMapping("/set")
    public ResponseDTO set(@RequestBody SystemConfigVO systemConfigVO) {
        return systemConfigService.setSystemConfig(systemConfigVO);
    }

    @PostMapping("/cleanRedis")
    public ResponseDTO cleanRedis(@RequestBody String key) {
        return systemConfigService.cleanRedis(key);
    }
}

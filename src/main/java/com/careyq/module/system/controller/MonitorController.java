package com.careyq.module.system.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.util.monitor.DynamicInfoUtils;
import com.careyq.util.monitor.StaticInfoUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author CareyQ
 * @since 2021/3/26 21:29
 */
@RestController
@RequestMapping("/system/monitor")
public class MonitorController {

    @GetMapping("/getStaticInfo")
    public ResponseDTO getSystemInfo() {
        StaticInfoUtils staticInfoUtils = new StaticInfoUtils();
        staticInfoUtils.getStaticInfo();
        return ResponseDTO.success(staticInfoUtils);
    }

    @GetMapping("/getDynamicInfo")
    public ResponseDTO getDynamicInfo() {
        DynamicInfoUtils dynamicInfoUtils = new DynamicInfoUtils();
        dynamicInfoUtils.getDynamicInfo();
        return ResponseDTO.success(dynamicInfoUtils);
    }
}

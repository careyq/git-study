package com.careyq.module.system.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.module.system.service.DictDataService;
import com.careyq.module.system.vo.DictDataVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author CareyQ
 * @since 2021/3/13 22:45
 */
@RestController
@RequestMapping("/system/dict/data")
@Slf4j
public class DictDataController {

    @Autowired
    private DictDataService dictDataService;

    @PostMapping("/list")
    public ResponseDTO getList(@RequestBody DictDataVO dictDataVO) {
        log.info("获取字典数据列表");
        return ResponseDTO.success(dictDataService.getPageList(dictDataVO));
    }
}

package com.careyq.module.system.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.careyq.common.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统配置表
 *
 * @author CareyQ
 * @since 2021/3/17 23:22
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_system_config")
public class SystemConfig extends BaseEntity<SystemConfig> {
    /**
     * 是否上传至本地 (0 否 1 是)
     */
    private String uploadLocal;
    /**
     * 是否上传至 COS (0 否 1 是)
     */
    private String uploadCos;
    /**
     * 显示方式 0 本地 1 COS
     */
    private String picShow;
    /**
     * 本地存储服务器地址
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String localDomain;
    /**
     * 本地存储服务器位置
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String localPath;
    /**
     * COS 密钥
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String cosSecretId;
    /**
     * COS 密钥
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String cosSecretKey;
    /**
     * COS 存储桶
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String cosBucket;
    /**
     * COS 存储地域
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String cosRegion;
    /**
     * COS 域名
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String cosDomain;
}

package com.careyq.module.system.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.careyq.common.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 角色表
 *
 * @author CareyQ
 * @since 2021/3/14 14:35
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_role")
public class Role extends BaseEntity<Role> {
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 角色介绍
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String summary;
    /**
     * 该角色下管辖的菜单 id
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String categoryUids;
}

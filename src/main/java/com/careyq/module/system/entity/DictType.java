package com.careyq.module.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.careyq.common.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 字典类型表
 *
 * @author CareyQ
 * @since 2021/3/13 18:03
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_dict_type")
public class DictType extends BaseEntity<DictType> {
    /**
     * 字典名称
     */
    private String dictName;
    /**
     * 字典类型
     */
    private String dictType;
    /**
     * 备注
     */
    private String remark;
}

package com.careyq.module.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.careyq.common.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 字典数据表
 *
 * @author CareyQ
 * @since 2021/3/13 20:59
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_dict_data")
public class DictData extends BaseEntity<DictData> {
    /**
     * 字典标签
     */
    private String dictLabel;
    /**
     * 字典键值
     */
    private String dictValue;
    /**
     * 字典排序
     */
    private String dictSort;
    /**
     * 字典类型 UID
     */
    private String dictTypeUid;
    /**
     * 是否默认
     */
    private String isDefault;
    /**
     * 备注
     */
    private String remark;
    /**
     * 字典数据所属的类型
     */
    @TableField(exist = false)
    private DictType dictType;
}

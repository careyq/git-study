package com.careyq.module.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.careyq.common.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author CareyQ
 * @since 2021/3/31 18:35
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_web_config")
public class WebConfig extends BaseEntity<WebConfig> {
    private String webTitle;
    private String webDesc;
    private String webKeyword;
    private String webBeiAn;
    private String homeImage;
    private String archiveImage;
    private String categoryImage;
    private String guestbookImage;
    private String linkImage;
    private String aboutImage;
    private String webDomain;
}

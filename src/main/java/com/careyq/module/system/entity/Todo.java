package com.careyq.module.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.careyq.common.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author CareyQ
 * @since 2021/3/26 13:10
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_todo")
public class Todo extends BaseEntity<Todo> {
    /**
     * 待办事项内容
     */
    private String content;
    /**
     * 是否完成 1 是 0 否
     */
    private String done;
}

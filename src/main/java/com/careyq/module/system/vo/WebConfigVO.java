package com.careyq.module.system.vo;

import com.careyq.common.domain.vo.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author CareyQ
 * @since 2021/3/17 23:32
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ToString
public class WebConfigVO extends BaseVO<WebConfigVO> {
    private String webTitle;
    private String webDesc;
    private String webKeyword;
    private String webBeiAn;
    private String homeImage;
    private String archiveImage;
    private String categoryImage;
    private String guestbookImage;
    private String linkImage;
    private String aboutImage;
    private String webDomain;
}

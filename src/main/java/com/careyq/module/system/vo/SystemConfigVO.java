package com.careyq.module.system.vo;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.careyq.common.domain.vo.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author CareyQ
 * @since 2021/3/17 23:32
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SystemConfigVO extends BaseVO<SystemConfigVO> {
    /**
     * 是否上传至本地 (0 否 1 是)
     */
    private String uploadLocal;
    /**
     * 是否上传至 COS (0 否 1 是)
     */
    private String uploadCos;
    /**
     * 显示方式 0 本地 1 COS
     */
    private String picShow;
    /**
     * 本地存储服务器地址
     */
    private String localDomain;
    /**
     * 本地存储服务器位置
     */
    private String localPath;
    /**
     * COS 密钥
     */
    private String cosSecretId;
    /**
     * COS 密钥
     */
    private String cosSecretKey;
    /**
     * COS 存储桶
     */
    private String cosBucket;
    /**
     * COS 存储地域
     */
    private String cosRegion;
    /**
     * COS 域名
     */
    private String cosDomain;
}

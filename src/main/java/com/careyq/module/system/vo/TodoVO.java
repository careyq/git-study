package com.careyq.module.system.vo;

import com.careyq.common.domain.vo.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author CareyQ
 * @since 2021/3/26 13:12
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TodoVO extends BaseVO<TodoVO> {
    /**
     * 待办事项内容
     */
    private String content;
    /**
     * 是否完成 1 是 0 否
     */
    private String done;
}

package com.careyq.module.system.vo;

import com.careyq.common.domain.vo.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author CareyQ
 * @since 2021/3/13 20:57
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class DictDataVO extends BaseVO<DictDataVO> {
    private String dictLabel;
    private String dictValue;
    private String dictTypeUid;
    private String dictSort;
    private String remark;
}

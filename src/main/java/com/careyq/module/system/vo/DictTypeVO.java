package com.careyq.module.system.vo;

import com.careyq.common.domain.vo.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * DictTypeVO
 *
 * @author CareyQ
 * @since 2021/3/13 18:09
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class DictTypeVO extends BaseVO<DictTypeVO> {
    private String dictName;
    private String dictType;
    private String remark;
}

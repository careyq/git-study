package com.careyq.module.login.service;

import com.careyq.exception.CustomException;
import com.careyq.module.admin.exception.AdminPasswordNotMatchException;
import com.careyq.module.login.domain.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 登录校验
 *
 * @author CareyQ
 * @since 2021/2/25 14:00
 */
@Service
@Slf4j
public class LoginService {
    @Autowired
    private TokenService tokenService;
    @Resource
    private AuthenticationManager authenticationManager;
    public String login(String username, String password) {
        Authentication authentication = null;
        try {
            // 调用 UserDetailsServiceImpl.loadUserByUsername 方法
            authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (Exception e) {
            if (e instanceof BadCredentialsException) {
                log.error("用户密码不正确或不符合规范");
                throw new AdminPasswordNotMatchException();
            } else {
                throw new CustomException(e.getMessage());
            }
        }
        assert authentication != null;
        LoginUser loginUser = (LoginUser) authentication.getPrincipal();
        return tokenService.generateToken(loginUser);
    }
}

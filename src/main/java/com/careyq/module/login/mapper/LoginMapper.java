package com.careyq.module.login.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.careyq.module.admin.entity.Admin;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author CareyQ
 * @since 2021/2/27 17:37
 */
@Mapper
public interface LoginMapper extends BaseMapper<Admin> {
}
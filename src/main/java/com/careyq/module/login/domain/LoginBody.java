package com.careyq.module.login.domain;

import lombok.Data;

/**
 * 登录对象
 *
 * @author CareyQ
 * @since 2021/2/25 14:03
 */
@Data
public class LoginBody {
    private String loginName;
    private String loginPwd;
}

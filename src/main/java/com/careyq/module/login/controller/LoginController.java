package com.careyq.module.login.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.domain.vo.RouterVo;
import com.careyq.constant.BaseRedisConst;
import com.careyq.constant.Constants;
import com.careyq.exception.QueryException;
import com.careyq.module.admin.entity.Admin;
import com.careyq.module.admin.service.AdminService;
import com.careyq.module.login.domain.LoginBody;
import com.careyq.module.login.domain.LoginUser;
import com.careyq.module.login.service.LoginService;
import com.careyq.module.login.service.TokenService;
import com.careyq.module.menu.entity.Menu;
import com.careyq.module.menu.service.MenuService;
import com.careyq.module.system.entity.Role;
import com.careyq.third.RedisService;
import com.careyq.util.JsonUtils;
import com.careyq.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 后台登录
 *
 * @author CareyQ
 * @since 2021/2/25 12:52
 */
@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private AdminService adminService;
    @Autowired
    private MenuService menuService;
    @Autowired
    private RedisService redisService;

    @PostMapping("/login")
    public ResponseDTO login(@RequestBody LoginBody loginFromDTO) {
        ResponseDTO response = ResponseDTO.success();

        String token = loginService.login(loginFromDTO.getLoginName(), loginFromDTO.getLoginPwd());

        response.put(Constants.TOKEN, token);
        return response;
    }

    @GetMapping("/getRouters")
    @SuppressWarnings("unchecked")
    public ResponseDTO getRouters(HttpServletRequest request) {
        LoginUser loginUser = tokenService.getLoginUser(request);
        Admin admin = loginUser.getAdmin();
        String routerRedis = admin.getUserName() + BaseRedisConst.ROUTER;

        String router = redisService.getEx(routerRedis);

        if (StringUtils.isEmpty(router)) {
            List<Role> roleList = adminService.getRoleList(admin);
            List<Menu> menuList = menuService.getListByRoleList(roleList);
            List<Menu> menuListBuild = menuService.buildMenus(menuList, "0");
            List<RouterVo> routerVos = menuService.buildRouters(menuListBuild);
            redisService.setEx(routerRedis, JsonUtils.objectToJson(routerVos), 24, TimeUnit.HOURS);
            return ResponseDTO.success(routerVos);
        }

        List<RouterVo> routerVos = JsonUtils.jsonToPojo(router, List.class);
        if (routerVos == null) {
            throw new QueryException("系统配置转换错误，请检查系统配置，或者清空Redis后重试！");
        }
        return ResponseDTO.success(routerVos);
    }
}

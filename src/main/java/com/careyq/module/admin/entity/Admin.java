package com.careyq.module.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.careyq.common.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Admin 表
 *
 * @author CareyQ
 * @since 2021/2/25 15:06
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_admin")
public class Admin extends BaseEntity<Admin> {
    /**
     * 用户名
     */
    private String userName;
    /**
     * 密码
     */
    private String passWord;
    /**
     * 角色 id
     */
    private String roleUid;

    private String avatarUrl;

    private String nickName;

    private String job;

    private String location;

    private String wechatUrl;

    private String email;

    private String qq;

    private String github;

    private String gitee;

    private String about;

    private String place;

    @JsonIgnore
    @JsonProperty
    public String getPassWord() {
        return passWord;
    }
}

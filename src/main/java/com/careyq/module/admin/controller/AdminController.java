package com.careyq.module.admin.controller;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.module.admin.service.AdminService;
import com.careyq.module.admin.vo.AdminPwdVO;
import com.careyq.module.admin.vo.AdminVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author CareyQ
 * @since 2021/4/5 13:15
 */
@RestController
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private AdminService adminService;

    @GetMapping("/get")
    public ResponseDTO getAdmin() {
        return ResponseDTO.success(adminService.getAdmin());
    }

    @PostMapping("/set")
    public ResponseDTO setAdmin(@RequestBody AdminVO adminVO) {
        return adminService.setAdmin(adminVO);
    }

    @PostMapping("/updatePwd")
    public ResponseDTO updatePwd(@RequestBody AdminPwdVO adminPwdVO) {
        return adminService.updatePwd(adminPwdVO.getOldPassword(), adminPwdVO.getNewPassword());
    }
}

package com.careyq.module.admin.service;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.SuperService;
import com.careyq.module.admin.entity.Admin;
import com.careyq.module.admin.vo.AdminVO;
import com.careyq.module.system.entity.Role;

import java.util.List;

/**
 * @author CareyQ
 * @since 2021/2/27 14:57
 */
public interface AdminService extends SuperService<Admin> {
    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象
     */
    Admin selectUserByUserName(String userName);

    /**
     * 根据用户获取角色
     *
     * @param admin 登录用户
     * @return 角色集合
     */
    List<Role> getRoleList(Admin admin);

    /**
     * 设置头像
     *
     * @param url 头像地址
     */
    void setAvatar(String url);

    /**
     * 设置微信
     *
     * @param url 微信图片地址
     */
    void setWechat(String url);

    Admin getAdmin();

    /**
     * 设置 admin
     *
     * @param adminVO 视图
     * @return 结果
     */
    ResponseDTO setAdmin(AdminVO adminVO);

    /**
     * 通过用户名、邮箱、个人网站验证是否是管理员
     *
     * @param nickName 用户名
     * @param email    邮箱
     * @return 结果
     */
    Boolean validate(String nickName, String email);

    /**
     * 获取管理员信息
     *
     * @return 管理员
     */
    Admin getInfo();

    ResponseDTO updatePwd(String oldPassword, String newPassword);
}

package com.careyq.module.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.common.service.impl.SuperServiceImpl;
import com.careyq.constant.MessageConst;
import com.careyq.constant.SqlConst;
import com.careyq.module.admin.entity.Admin;
import com.careyq.module.admin.mapper.AdminMapper;
import com.careyq.module.admin.service.AdminService;
import com.careyq.module.admin.vo.AdminVO;
import com.careyq.module.login.domain.LoginUser;
import com.careyq.module.login.service.TokenService;
import com.careyq.module.system.entity.Role;
import com.careyq.module.system.entity.SystemConfig;
import com.careyq.module.system.service.RoleService;
import com.careyq.module.system.service.SystemConfigService;
import com.careyq.third.RedisService;
import com.careyq.util.SecurityUtils;
import com.careyq.util.ServletUtils;
import com.careyq.util.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CareyQ
 * @since 2021/2/27 14:58
 */
@Service
public class AdminServiceImpl extends SuperServiceImpl<AdminMapper, Admin> implements AdminService {
    @Autowired
    private RedisService redisService;

    @Autowired
    private AdminMapper adminMapper;

    @Autowired
    private RoleService roleService;

    @Autowired
    private SystemConfigService systemConfigService;

    @Autowired
    private TokenService tokenService;

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象
     */
    @Override
    public Admin selectUserByUserName(String userName) {
        QueryWrapper<Admin> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.USER_NAME, userName);
        queryWrapper.last(SqlConst.LIMIT_ONE);
        return adminMapper.selectOne(queryWrapper);
    }

    @Override
    public List<Role> getRoleList(Admin admin) {
        List<String> roleUidList = new ArrayList<>();
        roleUidList.add(admin.getRoleUid());
        return roleService.listByIds(roleUidList);
    }

    @Override
    public void setAvatar(String url) {
        String username = SecurityUtils.getUsername();
        Admin admin = selectUserByUserName(username);
        admin.setAvatarUrl(url);
        admin.updateById();
    }

    @Override
    public void setWechat(String url) {
        String username = SecurityUtils.getUsername();
        Admin admin = selectUserByUserName(username);
        admin.setWechatUrl(url);
        admin.updateById();
    }

    @Override
    public Admin getAdmin() {
        String localDomain = systemConfigService.getSystemConfig().getLocalDomain();
        String username = SecurityUtils.getUsername();
        Admin admin = selectUserByUserName(username);

        if (StringUtils.isNotEmpty(admin.getAvatarUrl())) {
            admin.setAvatarUrl(localDomain + admin.getAvatarUrl());
        }

        if (StringUtils.isNotEmpty(admin.getWechatUrl())) {
            admin.setWechatUrl(localDomain + admin.getWechatUrl());
        }

        return admin;
    }

    @Override
    public ResponseDTO setAdmin(AdminVO adminVO) {
        String username = SecurityUtils.getUsername();
        Admin admin = selectUserByUserName(username);
        BeanUtils.copyProperties(adminVO, admin);
        admin.updateById();
        return ResponseDTO.success(MessageConst.UPDATE_SUCCESS);
    }

    @Override
    public Boolean validate(String nickName, String email) {
        QueryWrapper<Admin> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlConst.NICK_NAME, nickName)
                .eq(SqlConst.EMAIL, email);
        Integer count = adminMapper.selectCount(queryWrapper);
        return count > 0;
    }

    @Override
    public Admin getInfo() {
        QueryWrapper<Admin> queryWrapper = new QueryWrapper<>();
        queryWrapper.last("limit 1");
        queryWrapper.select(Admin.class, i ->
                !i.getColumn().equals(SqlConst.USER_NAME) &&
                        !i.getColumn().equals(SqlConst.PASS_WORD) &&
                        !i.getColumn().equals(SqlConst.CREATE_TIME) &&
                        !i.getColumn().equals(SqlConst.CREATE_BY) &&
                        !i.getColumn().equals(SqlConst.UPDATE_TIME) &&
                        !i.getColumn().equals(SqlConst.UPDATE_BY) &&
                        !i.getColumn().equals(SqlConst.ROLE_UID));
        Admin admin = adminMapper.selectOne(queryWrapper);
        SystemConfig systemConfig = systemConfigService.getSystemConfig();
        admin.setAvatarUrl(systemConfig.getLocalDomain() + admin.getAvatarUrl());
        admin.setWechatUrl(systemConfig.getLocalDomain() + admin.getWechatUrl());
        return admin;
    }

    @Override
    public ResponseDTO updatePwd(String oldPassword, String newPassword) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String username = loginUser.getUsername();
        String password = loginUser.getPassword();
        if (!SecurityUtils.matchesPassword(oldPassword, password)) {
            return ResponseDTO.error("修改密码失败，旧密码错误");
        }

        if (SecurityUtils.matchesPassword(newPassword, password)) {
            return ResponseDTO.error("新密码不能与旧密码相同");
        }

        Admin admin = loginUser.getAdmin();
        admin.setPassWord(SecurityUtils.encryptPassword(newPassword));
        if (admin.updateById()) {
            return ResponseDTO.success(MessageConst.UPDATE_SUCCESS);
        }
        return ResponseDTO.error(MessageConst.UPDATE_FAIL);
    }
}

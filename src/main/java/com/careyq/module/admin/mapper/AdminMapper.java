package com.careyq.module.admin.mapper;

import com.careyq.common.mapper.SuperMapper;
import com.careyq.module.admin.entity.Admin;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author CareyQ
 * @since 2021/2/27 16:50
 */
@Mapper
public interface AdminMapper extends SuperMapper<Admin> {
}

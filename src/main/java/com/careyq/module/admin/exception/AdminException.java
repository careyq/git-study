package com.careyq.module.admin.exception;

import com.careyq.exception.BaseException;

/**
 * 用户信息异常类
 *
 * @author CareyQ
 * @since 2021/3/1 20:38
 */
public class AdminException extends BaseException {

    private static final long serialVersionUID = 1L;

    public AdminException(String code, Object[] args) {
        super("admin", code, args, null);
    }
}

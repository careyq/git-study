package com.careyq.module.admin.exception;

/**
 * @author CareyQ
 * @since 2021/3/1 20:39
 */
public class AdminPasswordNotMatchException extends AdminException {
    private static final long serialVersionUID = 1L;

    public AdminPasswordNotMatchException() {
        super("admin.password.not.match", null);
    }
}

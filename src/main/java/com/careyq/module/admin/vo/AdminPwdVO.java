package com.careyq.module.admin.vo;

import lombok.Data;

/**
 * @author CareyQ
 * @since 2021/4/14 14:15
 */
@Data
public class AdminPwdVO {
    private String oldPassword;
    private String newPassword;
}

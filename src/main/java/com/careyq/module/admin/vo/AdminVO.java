package com.careyq.module.admin.vo;

import com.careyq.common.domain.vo.BaseVO;
import lombok.Data;

/**
 * @author CareyQ
 * @since 2021/4/5 12:53
 */
@Data
public class AdminVO {

    private String nickName;

    private String job;

    private String location;

    private String email;

    private String qq;

    private String github;

    private String gitee;

    private String about;

    private String place;
}

package com.careyq.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * mapper 父类
 *
 * @author CareyQ
 * @since 2021/3/7 10:59
 */
public interface SuperMapper<T> extends BaseMapper<T> {
}

package com.careyq.common.domain;

import com.careyq.constant.CommonConst;
import com.careyq.constant.HttpStatus;
import com.careyq.util.StringUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;

/**
 * 数据传输对象 返回类
 *
 * @author CareyQ
 * @since 2021/2/25 13:31
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ResponseDTO extends HashMap<String, Object> {

    private static final long serialVersionUID = 1L;

    /**
     * 状态码
     */
    private static final String CODE_TAG = "code";
    /**
     * 返回消息
     */
    private static final String MSG_TAG = "msg";
    /**
     * 返回数据
     */
    private static final String DATA_TAG = "data";

    public ResponseDTO() {
    }

    public ResponseDTO(int code, String msg) {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
    }

    public ResponseDTO(int code, String msg, Object data) {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
        if (StringUtils.isNotNull(data)) {
            super.put(DATA_TAG, data);
        }
    }

    /**
     * 返回成功消息
     *
     * @return 成功消息
     */
    public static ResponseDTO success() {
        return ResponseDTO.success("操作成功");
    }

    /**
     * 返回成功数据
     *
     * @return 成功消息
     */
    public static ResponseDTO success(Object data) {
        return ResponseDTO.success("操作成功", data);
    }

    /**
     * 返回成功消息
     *
     * @param msg 返回消息
     * @return 成功消息
     */
    public static ResponseDTO success(String msg) {
        return ResponseDTO.success(msg, null);
    }

    /**
     * 返回成功消息
     *
     * @param msg  返回消息
     * @param data 返回数据
     * @return 成功消息
     */
    public static ResponseDTO success(String msg, Object data) {
        return new ResponseDTO(HttpStatus.SUCCESS, msg, data);
    }

    /**
     * 返回错误消息
     *
     * @return 错误消息
     */
    public static ResponseDTO error() {
        return ResponseDTO.error("操作失败");
    }

    /**
     * 返回错误消息
     *
     * @param msg 返回消息
     * @return 错误消息
     */
    public static ResponseDTO error(String msg) {
        return ResponseDTO.error(msg, null);
    }

    /**
     * 返回错误消息
     *
     * @param msg  返回消息
     * @param data 返回数据
     * @return 错误消息
     */
    public static ResponseDTO error(String msg, Object data) {
        return new ResponseDTO(HttpStatus.ERROR, msg, data);
    }

    /**
     * 返回错误消息
     *
     * @param code 状态码
     * @param msg  返回消息
     * @return 错误消息
     */
    public static ResponseDTO error(int code, String msg) {
        return new ResponseDTO(code, msg, null);
    }
}

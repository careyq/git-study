package com.careyq.common.domain.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * BaseVo 表现层 基类对象
 *
 * @author CareyQ
 * @since 2021/3/7 10:25
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BaseVO<T> extends PageInfo<T> {
    /**
     * 唯一 UID
     */
    private String uid;
}

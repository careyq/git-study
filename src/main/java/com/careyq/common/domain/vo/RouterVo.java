package com.careyq.common.domain.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * 前端路由标准类
 *
 * @author CareyQ
 * @since 2021/3/2 22:22
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Data
public class RouterVo {
    /**
     * 路由名字
     */
    private String name;
    /**
     * 路由地址
     */
    private String path;
    /**
     * 是否隐藏
     */
    private boolean hidden;
    /**
     * 组件位置
     */
    private String component;
    /**
     * 路由其他元素
     */
    private MetaVo meta;

    /**
     * 子路由
     */
    private List<RouterVo> children;
}

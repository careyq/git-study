package com.careyq.common.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 前端路由中的信息标准类
 *
 * @author CareyQ
 * @since 2021/3/2 22:28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MetaVo {
    /**
     * 展示名字
     */
    private String title;
    /**
     * 展示图标
     */
    private String icon;
}

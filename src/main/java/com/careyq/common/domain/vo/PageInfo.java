package com.careyq.common.domain.vo;

import lombok.Data;

/**
 * PageVO 分页
 *
 * @author CareyQ
 * @since 2021/3/7 10:21
 */
@Data
public class PageInfo<T> {
    /**
     * 关键字
     */
    private String keyword;
    /**
     * 当前页
     */
    private Long currentPage;
    /**
     * 页大小
     */
    private Long pageSize;
}

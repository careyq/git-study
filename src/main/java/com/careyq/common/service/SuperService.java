package com.careyq.common.service;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * Service 父类
 *
 * @author CareyQ
 * @since 2021/3/7 10:39
 */
public interface SuperService<T> extends IService<T> {
}

package com.careyq.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.careyq.util.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Date;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * @author CareyQ
 * @since 2021/3/19 12:17
 */
@Component
@Slf4j
public class MyMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("进入插入自动填充...");
        this.strictInsertFill(metaObject, "createTime", Date.class, new Date());
        this.strictInsertFill(metaObject, "createBy", String.class, SecurityUtils.getUsername());
        this.strictInsertFill(metaObject, "updateTime", Date.class, new Date());
        this.strictInsertFill(metaObject, "updateBy", String.class, SecurityUtils.getUsername());
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("进入更新自动填充...");
        this.strictUpdateFill(metaObject, "updateTime", Date.class, new Date());
        if (SecurityUtils.getWebLoginUser() != null) {
            this.strictUpdateFill(metaObject, "updateBy", String.class, SecurityUtils.getUsername());
        }
    }

    @Override
    public MetaObjectHandler strictFillStrategy(MetaObject metaObject, String fieldName, Supplier<?> fieldVal) {
        Object obj = fieldVal.get();
        if (Objects.nonNull(obj)) {
            metaObject.setValue(fieldName, obj);
        }
        return this;
    }
}

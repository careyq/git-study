package com.careyq.exception;

import com.careyq.common.domain.ResponseDTO;
import com.careyq.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MultipartException;

/**
 * 全局异常处理
 *
 * @author CareyQ
 * @since 2021/3/1 21:36
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 基础异常
     */
    @ExceptionHandler(BaseException.class)
    public ResponseDTO baseException(BaseException e) {
        return ResponseDTO.error(e.getMessage());
    }

    /**
     * 自定义异常
     */
    public ResponseDTO customException(CustomException e) {
        if (StringUtils.isNull(e.getCode())) {
            return ResponseDTO.error(e.getMessage());
        }
        return ResponseDTO.error(e.getCode(), e.getMessage());
    }

    /**
     * 自定义验证异常
     */
    @ExceptionHandler(BindException.class)
    public ResponseDTO validatedBindException(BindException e) {
        log.error(e.getMessage(), e);
        String message = e.getAllErrors().get(0).getDefaultMessage();
        return ResponseDTO.error(message);
    }

    @ExceptionHandler(Exception.class)
    public ResponseDTO handleException(Exception e) {
        log.error(e.getMessage(), e);
        return ResponseDTO.error(e.getMessage());
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseDTO nullPointException(Exception e) {
        log.error(e.getMessage(), e);
        return ResponseDTO.error(e.getMessage());
    }

    @ExceptionHandler(MultipartException.class)
    public ResponseDTO fileException(Exception e) {
        log.error(e.getMessage(), e);
        return ResponseDTO.error("文件大小不超过 1MB");
    }
}

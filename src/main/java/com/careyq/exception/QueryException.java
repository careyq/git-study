package com.careyq.exception;

import com.careyq.constant.ErrorCodeConst;
import com.careyq.constant.MessageConst;
import com.careyq.constant.SqlConst;

import java.io.Serializable;

/**
 * 自定义查询操作相关的异常
 *
 * @author CareyQ
 * @since 2021/3/18 10:14
 */
public class QueryException extends RuntimeException implements Serializable {
    /**
     * 异常状态码
     */
    private String code;

    public QueryException() {
        super(MessageConst.QUERY_DEFAULT_ERROR);
        this.code = ErrorCodeConst.QUERY_DEFAULT_ERROR;
    }

    public QueryException(String message, Throwable cause) {
        super(message, cause);
        this.code = ErrorCodeConst.QUERY_DEFAULT_ERROR;
    }

    public QueryException(String message) {
        super(message);
        this.code = ErrorCodeConst.QUERY_DEFAULT_ERROR;
    }

    public QueryException(String code, String message) {
        super(message);
        this.code = code;
    }

    public QueryException(Throwable cause) {
        super(cause);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

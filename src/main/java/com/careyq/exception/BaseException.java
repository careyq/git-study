package com.careyq.exception;

import com.careyq.util.MessageUtils;
import com.careyq.util.StringUtils;

import java.util.Arrays;

/**
 * 基础异常
 *
 * @author CareyQ
 * @since 2021/3/1 20:28
 */
public class BaseException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * 所属模块
     */
    private final String module;
    /**
     * 错误码
     */
    private final String code;
    /**
     * 错误码对应的参数
     */
    private final Object[] args;
    /**
     * 错误信息
     */
    private final String defaultMessage;

    public BaseException(String defaultMessage) {
        this(null, null, null, defaultMessage);
    }

    public BaseException(String module, String code, Object[] args, String defaultMessage) {
        this.module = module;
        this.code = code;
        this.args = args;
        this.defaultMessage = defaultMessage;
    }

    @Override
    public String getMessage() {
        String message = null;
        if (!StringUtils.isEmpty(code)) {
            message = MessageUtils.message(code, args);
        }
        if (message == null) {
            message = defaultMessage;
        }
        return message;
    }

    public String getModule() {
        return module;
    }

    public String getCode() {
        return code;
    }

    public Object[] getArgs() {
        return args;
    }

    public String getDefaultMessage() {
        return defaultMessage;
    }
}

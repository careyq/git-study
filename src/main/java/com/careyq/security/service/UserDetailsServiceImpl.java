package com.careyq.security.service;

import com.careyq.module.admin.entity.Admin;
import com.careyq.module.admin.service.AdminService;
import com.careyq.module.login.domain.LoginUser;
import com.careyq.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * 用户验证处理
 *
 * @author CareyQ
 * @since 2021/2/27 21:40
 */
@Service
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private AdminService adminService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Admin admin = adminService.selectUserByUserName(username);
        if (StringUtils.isNull(admin)) {
            log.info("登录用户：{} 不存在", username);
            throw new UsernameNotFoundException("登录用户：" + username + "不存在");
        }

        return createLoginUser(admin);
    }

    public UserDetails createLoginUser(Admin admin) {
        return new LoginUser(admin);
    }
}

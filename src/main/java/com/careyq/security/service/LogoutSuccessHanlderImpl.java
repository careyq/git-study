package com.careyq.security.service;

import com.alibaba.fastjson.JSON;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.constant.HttpStatus;
import com.careyq.module.login.domain.LoginUser;
import com.careyq.module.login.service.TokenService;
import com.careyq.util.ServletUtils;
import com.careyq.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义退出处理类 返回成功
 *
 * @author CareyQ
 * @since 2021/3/1 23:04
 */
@Configuration
public class LogoutSuccessHanlderImpl implements LogoutSuccessHandler {

    @Autowired
    private TokenService tokenService;

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        LoginUser loginUser = tokenService.getLoginUser(request);
        if (StringUtils.isNotNull(loginUser)) {
            tokenService.delLoginUser(loginUser.getToken());
        }
        ServletUtils.renderString(response, JSON.toJSONString(ResponseDTO.error(HttpStatus.SUCCESS, "退出成功")));
    }
}

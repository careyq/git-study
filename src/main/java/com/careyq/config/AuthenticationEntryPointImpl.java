package com.careyq.config;

import com.alibaba.fastjson.JSON;
import com.careyq.common.domain.ResponseDTO;
import com.careyq.constant.HttpStatus;
import com.careyq.util.ServletUtils;
import com.careyq.util.StringUtils;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

/**
 * 认证失败处理类
 *
 * @author CareyQ
 * @since 2021/2/23 22:04
 */
@Component
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint, Serializable {

    private static final long serialVersionUID = -8970718410437077606L;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        int code = HttpStatus.UNAUTHORIZED;
        String msg = "请求访问：{ " + request.getRequestURI() + " }，认证失败，无法访问系统资源！";
        ServletUtils.renderString(response, JSON.toJSONString(ResponseDTO.error(code, msg)));
    }
}

package com.careyq.util.monitor;

import lombok.Data;
import lombok.ToString;

/**
 * 系统信息
 * @author CareyQ
 * @since 2021/3/26 19:09
 */
@Data
@ToString
public class Sys {
    /**
     * 设备制造商
     */
    private String computerManufacturer;
    /**
     * 设备系统模型
     */
    private String computerModel;
    /**
     * 设备名称
     */
    private String computerName;
    /**
     * 设备 IP
     */
    private String computerIp;
    /**
     * 设备系统
     */
    private String osName;
    /**
     * 设备架构
     */
    private String osArch;
}

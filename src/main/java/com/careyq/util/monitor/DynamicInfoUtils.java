package com.careyq.util.monitor;

import cn.hutool.core.date.DateUtil;
import com.careyq.util.ArithmeticUtils;
import com.careyq.util.DateUtils;
import lombok.Data;
import oshi.SystemInfo;
import oshi.hardware.CentralProcessor;
import oshi.hardware.GlobalMemory;
import oshi.hardware.HardwareAbstractionLayer;
import oshi.hardware.PhysicalMemory;

import java.lang.management.ManagementFactory;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * @author CareyQ
 * @since 2021/3/27 13:33
 */
@Data
public class DynamicInfoUtils {
    private Jvm jvm = new Jvm();
    private Cpu cpu = new Cpu();
    private Mem mem = new Mem();

    public void getDynamicInfo() {
        setJvmInfo();
        setCpuInfo();
        setMemInfo();
    }

    private void setJvmInfo() {
        long total = Runtime.getRuntime().totalMemory();
        long max = Runtime.getRuntime().maxMemory();
        long free = Runtime.getRuntime().freeMemory();
        Properties props = System.getProperties();

        jvm.setName(ManagementFactory.getRuntimeMXBean().getVmName());
        jvm.setTotal(ArithmeticUtils.div(total, (1024 * 1024), 2));
        jvm.setMax(ArithmeticUtils.div(max, (1024 * 1024), 2));
        jvm.setFree(ArithmeticUtils.div(free, (1024 * 1024), 2));
        jvm.setUsed(ArithmeticUtils.div(total - free, (1024 * 1024), 2));
        jvm.setUsage(ArithmeticUtils.mul(ArithmeticUtils.div(total - free, total, 4), 100));
        jvm.setStartTime(DateUtil.format(DateUtils.getServerStartDate(), DateUtils.YYYY_MM_DD_HH_MM_SS));
        jvm.setRunTime(DateUtils.getDatePoor(new Date(), DateUtils.getServerStartDate()));
        jvm.setVersion(props.getProperty("java.version"));
        jvm.setHome(props.getProperty("java.home"));
        jvm.setUserDir(props.getProperty("user.dir"));
    }

    private void setCpuInfo() {
        SystemInfo si = new SystemInfo();
        HardwareAbstractionLayer hal = si.getHardware();
        CentralProcessor processor = hal.getProcessor();
        cpu.setName(processor.getProcessorIdentifier().getName().trim() + " " + ArithmeticUtils.div(processor.getMaxFreq(), 1000000000, 1) + " GHz");
        cpu.setCoreNum(processor.getPhysicalProcessorCount());
        cpu.setThreadNum(processor.getLogicalProcessorCount());

        long[] prevTicks = processor.getSystemCpuLoadTicks();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long[] ticks = processor.getSystemCpuLoadTicks();
        double nice = ticks[CentralProcessor.TickType.NICE.getIndex()] - prevTicks[CentralProcessor.TickType.NICE.getIndex()];
        double irq = ticks[CentralProcessor.TickType.IRQ.getIndex()] - prevTicks[CentralProcessor.TickType.IRQ.getIndex()];
        double softIrq = ticks[CentralProcessor.TickType.SOFTIRQ.getIndex()] - prevTicks[CentralProcessor.TickType.SOFTIRQ.getIndex()];
        double steal = ticks[CentralProcessor.TickType.STEAL.getIndex()] - prevTicks[CentralProcessor.TickType.STEAL.getIndex()];
        double cSys = ticks[CentralProcessor.TickType.SYSTEM.getIndex()] - prevTicks[CentralProcessor.TickType.SYSTEM.getIndex()];
        double user = ticks[CentralProcessor.TickType.USER.getIndex()] - prevTicks[CentralProcessor.TickType.USER.getIndex()];
        double ioWait = ticks[CentralProcessor.TickType.IOWAIT.getIndex()] - prevTicks[CentralProcessor.TickType.IOWAIT.getIndex()];
        double idle = ticks[CentralProcessor.TickType.IDLE.getIndex()] - prevTicks[CentralProcessor.TickType.IDLE.getIndex()];
        double totalCpu = user + nice + cSys + idle + ioWait + irq + softIrq + steal;

        cpu.setTotalUsage(new DecimalFormat("#.##%").format(1.0 - (idle / totalCpu)));
        cpu.setSysUsage(new DecimalFormat("#.##%").format(cSys / totalCpu));
        cpu.setUserUsage(new DecimalFormat("#.##%").format(user / totalCpu));
        cpu.setFree(new DecimalFormat("#.##%").format(idle / totalCpu));
        cpu.setWait(new DecimalFormat("#.##%").format(ioWait / totalCpu));
    }

    private void setMemInfo() {
        SystemInfo si = new SystemInfo();
        HardwareAbstractionLayer hal = si.getHardware();
        GlobalMemory memory = hal.getMemory();
        List<PhysicalMemory> memoryList = memory.getPhysicalMemory();

        List<Memory> memories = new ArrayList<>();
        for (PhysicalMemory physicalMemory : memoryList) {
            Memory m = new Memory();
            m.setManufacturer(physicalMemory.getManufacturer());
            m.setType(physicalMemory.getMemoryType());
            memories.add(m);
        }
        double total = memory.getTotal();
        double available = memory.getAvailable();
        mem.setTotal(new DecimalFormat("#.## GB").format(total / (1024 * 1024 * 1024)));
        mem.setUsed(new DecimalFormat("#.## GB").format((total - available) / (1024 * 1024 * 1024)));
        mem.setFree(new DecimalFormat("#.## GB").format(available / (1024 * 1024 * 1024)));
        mem.setUsage(new DecimalFormat("#.##%").format((memory.getTotal() - memory.getAvailable()) * 1.0 / memory.getTotal()));
        mem.setMemory(memories);
    }
}

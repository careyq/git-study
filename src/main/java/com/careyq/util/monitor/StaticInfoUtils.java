package com.careyq.util.monitor;

import com.careyq.util.IpUtils;
import lombok.Data;
import oshi.SystemInfo;
import oshi.hardware.ComputerSystem;
import oshi.hardware.HardwareAbstractionLayer;

import java.util.Properties;

/**
 * @author CareyQ
 * @since 2021/3/26 19:06
 */
@Data
public class StaticInfoUtils {
    private Sys sys = new Sys();


    public void getStaticInfo() {
        setSysInfo();
    }

    private void setSysInfo() {
        SystemInfo systemInfo = new SystemInfo();
        HardwareAbstractionLayer hardware = systemInfo.getHardware();
        ComputerSystem system = hardware.getComputerSystem();
        Properties props = System.getProperties();
        sys.setComputerManufacturer(system.getManufacturer());
        sys.setComputerModel(system.getModel());
        sys.setComputerName(IpUtils.getHostName());
        sys.setComputerIp(IpUtils.getHostIp());
        sys.setOsName(String.valueOf(systemInfo.getOperatingSystem()));
        String arch = props.getProperty("os.arch");
        char[] chars = arch.toCharArray();
        chars[0] -= 32;
        sys.setOsArch(String.valueOf(chars));
    }
}

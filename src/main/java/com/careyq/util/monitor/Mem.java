package com.careyq.util.monitor;

import lombok.Data;

import java.util.List;

/**
 * @author CareyQ
 * @since 2021/3/26 19:09
 */
@Data
public class Mem {
    /**
     * 总内存量
     */
    private String total;
    /**
     * 已用内存量
     */
    private String used;
    /**
     * 剩余内存
     */
    private String free;
    /**
     * 使用率
     */
    private String usage;

    private List<Memory> memory;
}

package com.careyq.util.monitor;

import lombok.Data;

/**
 * @author CareyQ
 * @since 2021/3/27 17:06
 */
@Data
public class Memory {
    /**
     * 制造商
     */
    private String manufacturer;
    /**
     * 通道类型
     */
    private String type;
}

package com.careyq.util.monitor;

import lombok.Data;

/**
 * @author CareyQ
 * @since 2021/3/26 19:08
 */
@Data
public class Cpu {
    /**
     * CPU 名字
     */
    private String name;
    /**
     * 核心数
     */
    private int coreNum;
    /**
     * 线程数
     */
    private int threadNum;
    /**
     * 总使用率
     */
    private String totalUsage;
    /**
     * 系统使用率
     */
    private String sysUsage;
    /**
     * 用户使用率
     */
    private String userUsage;
    /**
     * 空闲
     */
    private String free;
    /**
     * 等待率
     */
    private String wait;
}

package com.careyq.util;

/**
 * 处理并记录日志文件
 *
 * @author CareyQ
 * @since 2021/3/1 0:28
 */
public class LogUtils {
    public static String getBlock(Object msg) {
        if (msg == null) {
            msg = "";
        }
        return "[" + msg.toString() + "]";
    }
}

package com.careyq.util;

import com.careyq.constant.CharacterConst;
/**
 * 文件工具类
 *
 * @author CareyQ
 * @since 2021/3/17 14:54
 */
public class FileUtils {

    public static String getExtension(String fileName) {
        String extension = "jpg";
        if (StringUtils.isNotEmpty(fileName) && fileName.contains(CharacterConst.POINT)) {
            extension = fileName.substring(fileName.lastIndexOf(CharacterConst.POINT) + 1).toLowerCase();
        }
        return extension;
    }
}

package com.careyq.util;

import java.util.UUID;

/**
 * 字符串工具类
 *
 * @author CareyQ
 * @since 2021/2/18 15:58
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils {

    private static final String NULL_STR = "";

    /**
     * 判断一个对象是否为空
     * @param object 对象
     * @return true：为空 false：非空
     */
    public static boolean isNull(Object object) {
        return object == null;
    }

    /**
     * 判断一个对象是否非空
     * @param object 对象
     * @return true：非空 false：为空
     */
    public static boolean isNotNull(Object object) {
        return !isNull(object);
    }

    /**
     * 判断字符串是否为空串
     * @param str 字符串
     * @return true：为空 false：非空
     */
    public static boolean isEmpty(String str) {
        return isNull(str) || NULL_STR.equals(str.trim());
    }

    /**
     * 判断对象数组是否为空
     * @param objects 要判断的对象数组要判断的对象数组
     * @return true：为空 false：非空
     */
    public static boolean isEmpty(Object[] objects) {
        return isNull(objects) || (objects.length == 0);
    }

    /**
     * 判断字符串是否非空串
     * @param str 字符串
     * @return true：非空 false：为空
     */
    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    public static boolean isNotEmptyAndNotNull(String str) {
        return isNotNull(str) && isNotEmpty(str);
    }

    public static String getUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 将数组转换成以逗号分隔的字符串
     *
     * @param needChange
     *            需要转换的数组
     * @return 以逗号分割的字符串
     */
    public static String join(String[] needChange) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < needChange.length; i++) {
            sb.append(needChange[i]);
            if ((i + 1) != needChange.length) {
                sb.append(",");
            }
        }
        return sb.toString();
    }
}

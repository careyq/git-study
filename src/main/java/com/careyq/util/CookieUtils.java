package com.careyq.util;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.Arrays;

/**
 * Cookie 工具类
 *
 * @author CareyQ
 * @since 2021/2/28 17:33
 */
public class CookieUtils {
    public static void setCookie(String cookieName, String cookieValue, int cookieMaxage) {
        setCookie(cookieName, cookieValue, cookieMaxage, false);
    }

    /**
     * 设置 Cookie，在指定时间内生效，进行编码
     * @param cookieName cookie 键
     * @param cookieValue cookie 值
     * @param cookieMaxage cookie 时效
     * @param isEncode cookie 是否编码
     */
    public static void setCookie(String cookieName, String cookieValue, int cookieMaxage, boolean isEncode) {
        doSetCookie(cookieName, cookieValue, cookieMaxage, isEncode);
    }

    private static void doSetCookie(String cookieName, String cookieValue, int cookieMaxage, boolean isEncode) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        assert attributes != null;
        HttpServletRequest request = attributes.getRequest();
        HttpServletResponse response = attributes.getResponse();

        try {
            if (cookieValue == null) {
                cookieValue = "";
            } else if (isEncode) {
                cookieValue = URLEncoder.encode(cookieValue, "utf-8");
            }

            Cookie cookie = new Cookie(cookieName, cookieValue);
            if (cookieMaxage > 0) {
                cookie.setMaxAge(cookieMaxage);
            }
            String domainName = getDomainName(request);
            if (!"localhost".equals(domainName)) {
                cookie.setDomain(domainName);
            }
            cookie.setDomain(domainName);
            cookie.setPath("/");
            assert response != null;
            response.addCookie(cookie);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 得到 cookie 的域名
     */
    private static String getDomainName(HttpServletRequest request) {
        String domainName = null;

        String serverName = request.getRequestURL().toString();
        if (StringUtils.isNotEmpty(serverName)) {
            serverName = serverName.toLowerCase().substring(7);
            final int end = serverName.indexOf("/");
            serverName = serverName.substring(0, end);
            final String[] domains = serverName.split("\\.");

            int len = domains.length;

            if (len > 3) {
                domainName = "." + domains[len - 3] + "." + domains[len - 2] + "." + domains[len - 1];
            } else if (len > 1) {
                domainName = "." + domains[len - 2] + "." + domains[len - 1];
            } else {
                domainName = serverName;
            }
        }

        assert domainName != null;
        if (domainName.indexOf(":") > 0) {
            String[] split = domainName.split("\\:");
            domainName = split[0];
        }

        return domainName;
    }
}

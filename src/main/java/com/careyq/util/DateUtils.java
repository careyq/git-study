package com.careyq.util;

import com.careyq.constant.DateConst;
import com.careyq.constant.NumberConst;

import java.lang.management.ManagementFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author CareyQ
 * @since 2021/2/27 22:50
 */
public class DateUtils {

    public static final String YYYY_MM = "yyyy-MM";
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    /**
     * 获得两个日期相差的秒数
     *
     * @param lastDate 新日期
     * @param date     旧日期
     * @return 相差秒数
     */
    public static int getSecondDiff(Date lastDate, Date date) {
        long second = 0L;
        try {
            second = (lastDate.getTime() - date.getTime()) / 1000;
        } catch (Exception e) {
            return 0;
        }
        return (int) second;
    }

    /**
     * 获取当前的年
     */
    public static Integer getYear() {
        return new GregorianCalendar(TimeZone.getDefault()).get(Calendar.YEAR);
    }

    /**
     * 获取当前的月
     */
    public static Integer getMonth() {
        return new GregorianCalendar(TimeZone.getDefault()).get(Calendar.MONTH) + 1;
    }

    /**
     * 获取当前的日
     */
    public static Integer getDay() {
        return new GregorianCalendar(TimeZone.getDefault()).get(Calendar.DAY_OF_MONTH);
    }

    /**
     * 获取距离当前 6 个月的月份
     *
     * @return 月份
     */
    public static String getHalfYearMonth() {
        SimpleDateFormat sdf = new SimpleDateFormat(YYYY_MM);
        Calendar instance = Calendar.getInstance();
        instance.setTime(new Date());
        instance.add(Calendar.MONTH, -5);
        return sdf.format(instance.getTime());
    }

    /**
     * 获取距离当前 6 个月的所有月份
     *
     * @return 月份列表
     */
    public static List<String> getHalfYearAllMonth() {
        SimpleDateFormat sdf = new SimpleDateFormat(YYYY_MM);
        List<String> monthList = new ArrayList<>();
        try {
            Date start = sdf.parse(getHalfYearMonth());
            Calendar startCalendar = Calendar.getInstance();
            startCalendar.setTime(new Date());
            startCalendar.add(Calendar.MONTH, 1);
            Date end = sdf.parse(sdf.format(startCalendar.getTime()));
            Calendar instance = Calendar.getInstance();
            instance.setTime(start);

            while (instance.getTime().before(end)) {
                String month = sdf.format(instance.getTime());
                instance.add(Calendar.MONTH, 1);
                monthList.add(month);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return monthList;
    }

    /**
     * 获取服务器启动时间
     */
    public static Date getServerStartDate() {
        long startTime = ManagementFactory.getRuntimeMXBean().getStartTime();
        return new Date(startTime);
    }

    /**
     * 计算两个时间差
     */
    public static String getDatePoor(Date endDate, Date nowDate) {
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        return day + " 天" + hour + " 小时" + min + " 分钟";
    }

    /**
     * 获取相对时间 几秒前、几分钟前、几小时前...
     *
     * @param date 比对的时间
     * @return 相对时间字符串
     */
    public static String getRelativeDate(Date date) {
        long delta = System.currentTimeMillis() - date.getTime();

        if (delta < DateConst.ONE_MINUTE) {
            long seconds = toSeconds(delta);
            return (seconds <= 0 ? 1 : seconds) + DateConst.SECOND_AGO;
        }
        if (delta < NumberConst.L45 * DateConst.ONE_MINUTE) {
            long minutes = toMinutes(delta);
            return (minutes <= 0 ? 1 : minutes) + DateConst.MINUTE_AGO;
        }
        if (delta < NumberConst.L24 * DateConst.ONE_HOUR) {
            long hours = toHours(delta);
            return (hours <= 0 ? 1 : hours) + DateConst.HOUR_AGO;
        }
        if (delta < NumberConst.L48 * DateConst.ONE_HOUR) {
            return DateConst.YESTERDAY;
        }
        if (delta < NumberConst.L30 * DateConst.ONE_DAY) {
            long days = toDays(delta);
            return (days <= 0 ? 1 : days) + DateConst.DAY_AGO;
        }
        if (delta < NumberConst.L48 * DateConst.ONE_WEEK) {
            long months = toMonths(delta);
            return (months <= 0 ? 1 : months) + DateConst.MONTH_AGO;
        } else {
            long years = toYears(delta);
            return (years <= 0 ? 1 : years) + DateConst.YEAR_AGO;
        }

    }

    private static long toSeconds(long date) {
        return date / 1000L;
    }

    private static long toMinutes(long date) {
        return toSeconds(date) / 60L;
    }

    private static long toHours(long date) {
        return toMinutes(date) / 60L;
    }

    private static long toDays(long date) {
        return toHours(date) / 24L;
    }

    private static long toMonths(long date) {
        return toDays(date) / 30L;
    }

    private static long toYears(long date) {
        return toMonths(date) / 365L;
    }
}

package com.careyq.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * json 工具类
 *
 * @author CareyQ
 * @since 2021/2/28 18:14
 */
public class JsonUtils {
    private static final ObjectMapper MAPPER = new ObjectMapper();

    public static <T> T jsonToPojo(String jsonDate, Class<T> beanType) {
        try {
            return MAPPER.readValue(jsonDate, beanType);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 把对象转换为json数据
     *
     * @param obj 对象
     * @return 结果
     */
    public static String objectToJson(Object obj) {
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
        try {
            return gson.toJson(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

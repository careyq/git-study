package com.careyq.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 精确算术运算
 *
 * @author CareyQ
 * @since 2021/3/26 23:20
 */
public class ArithmeticUtils {

    /**
     * 除法运算，scale 指定保留小数
     *
     * @param v1    被除数
     * @param v2    除数
     * @param scale 保留小数
     * @return 商
     */
    @SuppressWarnings("all")
    public static double div(double v1, double v2, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));

        if (b1.compareTo(BigDecimal.ZERO) == 0) {
            return BigDecimal.ZERO.doubleValue();
        }
        return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     * 乘法运算。
     *
     * @param v1 被乘数
     * @param v2 乘数
     * @return 积
     */
    public static double mul(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.multiply(b2).doubleValue();
    }
}
